webpackJsonp([1],{

/***/ 720:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FilterRangePageModule", function() { return FilterRangePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__filter_range__ = __webpack_require__(742);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var FilterRangePageModule = /** @class */ (function () {
    function FilterRangePageModule() {
    }
    FilterRangePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__filter_range__["a" /* FilterRangePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__filter_range__["a" /* FilterRangePage */]),
            ],
        })
    ], FilterRangePageModule);
    return FilterRangePageModule;
}());

//# sourceMappingURL=filter-range.module.js.map

/***/ }),

/***/ 742:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FilterRangePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var FilterRangePage = /** @class */ (function () {
    function FilterRangePage(navCtrl, navParams, viewController) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewController = viewController;
        this.priceRange = { lower: 20, upper: 500 };
        this.pageTitle = navParams.get('selectionType');
    }
    FilterRangePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad FilterRangePage');
    };
    FilterRangePage.prototype.reset = function () {
        this.priceRange = { lower: 20, upper: 500 };
    };
    FilterRangePage.prototype.closePriceRange = function () {
        this.viewController.dismiss();
    };
    FilterRangePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-filter-range',template:/*ion-inline-start:"C:\Users\User\Documents\myacccim\myACCCIM App\asos-user\src\pages\filter-range\filter-range.html"*/'<ion-header>\n    <ion-navbar>\n      <ion-title>\n        <p>{{pageTitle}}</p>\n      </ion-title>\n  \n      <ion-buttons right>  \n        <button padding ion-button icon-only outline (click)="reset()">\n          <p>RESET</p>\n        </button>\n      </ion-buttons>\n    </ion-navbar>\n  </ion-header>\n  \n  <ion-content padding>\n    <ion-range dualKnobs="true" pin="true" [(ngModel)]="priceRange" color="dark" min="20" max="500">\n      <p range-left>$20</p>\n      <p range-right>$500</p>\n    </ion-range>\n  </ion-content>\n  \n  <ion-footer padding>\n    <button ion-button full (click)="closePriceRange()">\n      <p>DONE</p>\n    </button>\n  </ion-footer>'/*ion-inline-end:"C:\Users\User\Documents\myacccim\myACCCIM App\asos-user\src\pages\filter-range\filter-range.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["z" /* ViewController */]])
    ], FilterRangePage);
    return FilterRangePage;
}());

//# sourceMappingURL=filter-range.js.map

/***/ })

});
//# sourceMappingURL=1.js.map