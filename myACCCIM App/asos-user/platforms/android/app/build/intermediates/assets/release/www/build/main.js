webpackJsonp([26],{

/***/ 137:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_home__ = __webpack_require__(76);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_api_service_api_service__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_storage__ = __webpack_require__(65);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var LoginPage = /** @class */ (function () {
    function LoginPage(nav, forgotCtrl, menu, toastCtrl, formBuilder, ApiService, alertCtrl, loadingCtrl, storage, view) {
        this.nav = nav;
        this.forgotCtrl = forgotCtrl;
        this.menu = menu;
        this.toastCtrl = toastCtrl;
        this.formBuilder = formBuilder;
        this.ApiService = ApiService;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.storage = storage;
        this.view = view;
        this.menu.swipeEnable(false);
        this.loginForm = this.formBuilder.group({
            'user': ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required])],
            'pass': ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required])]
        });
    }
    // closeLoginModal()
    // {
    //   this.view.dismiss();
    // }
    // login and go to home page
    LoginPage.prototype.login = function () {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: 'Please wait...',
            duration: 2000
        });
        loading.present();
        this.ApiService.userlogin(this.user, this.pass).subscribe(function (res) {
            var checkLogin = res.json();
            _this.result = checkLogin;
            var alert = _this.alertCtrl.create({
                title: 'Successfully Signed In',
                subTitle: 'Welcome To MyACCCIM!',
                buttons: ['Ok']
            });
            alert.present();
            _this.storage.set("userid", _this.result);
            _this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */]);
        }, function (err) {
            //console.log("Failed");
            var alert = _this.alertCtrl.create({
                title: 'Sign In Failed',
                subTitle: 'ID or password is incorrect',
                buttons: ['Ok']
            });
            alert.present();
        });
    };
    LoginPage.prototype.forgotPass = function () {
        var _this = this;
        var forgot = this.forgotCtrl.create({
            title: 'Forgot Password?',
            message: "Enter you email address to send a reset link password.",
            inputs: [
                {
                    name: 'username',
                    placeholder: 'username',
                    type: 'username'
                },
                {
                    name: 'email',
                    placeholder: 'Email',
                    type: 'email'
                },
                {
                    name: 'new password',
                    placeholder: 'new password',
                    type: 'password'
                },
            ],
            buttons: [
                {
                    text: 'Cancel',
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Send',
                    handler: function (data) {
                        console.log('Send clicked');
                        var toast = _this.toastCtrl.create({
                            message: 'Email was sended successfully',
                            duration: 3000,
                            position: 'top',
                            cssClass: 'dark-trans',
                            closeButtonText: 'OK',
                            showCloseButton: true
                        });
                        toast.present();
                    }
                }
            ]
        });
        forgot.present();
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-login',template:/*ion-inline-start:"C:\Users\domin\Documents\myacccim\myACCCIM App\asos-user\src\pages\login\login.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <ion-title>Sign In</ion-title>\n\n\n\n    </ion-navbar>\n\n  </ion-header>\n\n<!--\n\n  Generated template for the LoginPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<!-- -->\n\n<ion-content padding class="animated fadeIn login auth-page">\n\n  <div class="login-content">\n\n\n\n    <!-- Logo -->\n\n    <div padding-horizontal text-center class="animated fadeInDown">\n\n      <div class="logo">\n\n        <ion-img style="width: 160px; height: 80px;background:none;" src="../../assets/imgs/ACCIMlogo.png"></ion-img>\n\n      </div>\n\n      <h2 ion-text class="text-primary">\n\n        <strong>ACCCIM Committee</strong>\n\n      </h2>\n\n    </div>\n\n\n\n    <!-- Login form -->\n\n<div [formGroup]="loginForm"> \n\n    <div class="list-form">\n\n      <ion-item>\n\n        <ion-label floating>\n\n          <ion-icon name="person" item-start class="text-primary"></ion-icon>\n\n          ID \n\n        </ion-label>\n\n        <ion-input type="text" [(ngModel)]="user" name="user" formControlName="user" id="user"></ion-input>\n\n      </ion-item>\n\n\n\n      <ion-item>\n\n        <ion-label floating>\n\n          <ion-icon name="lock" item-start class="text-primary"></ion-icon>\n\n          Password\n\n        </ion-label>\n\n        <ion-input type="password" [(ngModel)]="pass" name="pass" formControlName="pass" id="pass"></ion-input>\n\n      </ion-item>\n\n    </div>\n\n</div>\n\n<!--\n\n    <p text-right ion-text color="secondary" tappable (click)="forgotPass()"><strong>Forgot Password?</strong></p>\n\n  -->\n\n    <div>\n\n\n\n    <br>\n\n      <button ion-button icon-start block color="dark" tappable [disabled]="!loginForm.valid" (click)="login()">\n\n        <ion-icon name="log-in"></ion-icon>\n\n        SIGN IN\n\n      </button>\n\n    \n\n     \n\n    </div>\n\n\n\n\n\n   \n\n  </div>\n\n</ion-content>\n\n\n\n'/*ion-inline-end:"C:\Users\domin\Documents\myacccim\myACCCIM App\asos-user\src\pages\login\login.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* MenuController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_4__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* ViewController */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 138:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SavedItemsPage; });
/* unused harmony export Item */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__product_colour_pop_over_product_colour_pop_over__ = __webpack_require__(176);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__product_size_pop_over_product_size_pop_over__ = __webpack_require__(177);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var SavedItemsPage = /** @class */ (function () {
    function SavedItemsPage(navCtrl, navParams, popoverCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.popoverCtrl = popoverCtrl;
        this.savedItems = [];
        this.itemsCount = 0;
        this.getExampleItems();
    }
    SavedItemsPage.prototype.getExampleItems = function () {
        // Discounted item
        var discountedItem = new Item();
        discountedItem.name = "Pull&Bear Denim Jacket In Black";
        discountedItem.price = 40;
        discountedItem.hasDiscount = true;
        discountedItem.discountedPrice = 30;
        discountedItem.picture = "https://res.cloudinary.com/cediim8/image/upload/v1523344614/product-1.jpg";
        this.savedItems.push(discountedItem);
        // Item where the size and colour have been selected
        var setSizeAndColourItem = new Item();
        setSizeAndColourItem.name = "ASOS Embroidered Mini Dress";
        setSizeAndColourItem.price = 109;
        setSizeAndColourItem.sizeAndColourSelected = true;
        setSizeAndColourItem.size = "M";
        setSizeAndColourItem.colour = "Black";
        setSizeAndColourItem.canAddToBag = true;
        setSizeAndColourItem.picture = "https://res.cloudinary.com/cediim8/image/upload/v1523415017/women-product-8.jpg";
        this.savedItems.push(setSizeAndColourItem);
        // Normal item without discount and size and colour not set yet
        var item = new Item();
        item.name = "Lee Sherpa Rider Denim Jacket";
        item.price = 218;
        item.hasDiscount = false;
        item.picture = "https://res.cloudinary.com/cediim8/image/upload/v1523344614/product-4.jpg";
        item.size = "M";
        item.colour = "Black";
        this.savedItems.push(item);
        this.itemsCount = this.savedItems.length;
    };
    SavedItemsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SavedItemsPage');
    };
    SavedItemsPage.prototype.editItem = function (item) {
        item.isInEditMode = !item.isInEditMode;
    };
    SavedItemsPage.prototype.colourPopOver = function (myEvent) {
        var popover = this.popoverCtrl.create(__WEBPACK_IMPORTED_MODULE_2__product_colour_pop_over_product_colour_pop_over__["a" /* ProductColourPopOverPage */]);
        popover.present({
            ev: myEvent
        });
    };
    SavedItemsPage.prototype.sizePopOver = function (myEvent) {
        var popover = this.popoverCtrl.create(__WEBPACK_IMPORTED_MODULE_3__product_size_pop_over_product_size_pop_over__["a" /* ProductSizePopOverPage */]);
        popover.present({
            ev: myEvent
        });
    };
    SavedItemsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-saved-items',template:/*ion-inline-start:"C:\Users\domin\Documents\myacccim\myACCCIM App\asos-user\src\pages\saved-items\saved-items.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <ion-buttons left>\n\n      <button ion-button menuToggle>\n\n        <ion-icon name="menu"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n\n\n    <ion-title>\n\n      <ion-row>\n\n        <p class="title">SAVED ITEMS</p>\n\n        <p class="items-count">({{ itemsCount }} items)</p>\n\n      </ion-row>\n\n    </ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n  <ion-list>\n\n    <ion-item no-lines class="saved-item" *ngFor="let item of savedItems">\n\n      <img item-start src="{{ item.picture }}">\n\n\n\n      <ion-col *ngIf="!item.isInEditMode">\n\n        <ion-item class="name-bin" no-lines>\n\n          <p item-start>{{ item.name }}</p>\n\n\n\n          <button item-end ion-button clear icon-only>\n\n            <ion-icon name="ios-trash-outline"></ion-icon>\n\n          </button>\n\n        </ion-item>\n\n\n\n        <ion-row *ngIf="!item.hasDiscount">\n\n          <p class="actual-price">${{ item.price }}</p>\n\n        </ion-row>\n\n\n\n        <ion-row *ngIf="item.hasDiscount">\n\n          <p class="discounted-price">${{ item.discountedPrice }}</p>\n\n          <p class="non-discounted-price">${{ item.price }}</p>\n\n        </ion-row>\n\n\n\n        <p class="size-colour" *ngIf="item.sizeAndColourSelected">{{ item.size }} / {{ item.colour }}</p>\n\n        <p class="select-size-colour" *ngIf="!item.sizeAndColourSelected">SELECT EDIT FOR SIZE AND COLOUR</p>\n\n\n\n        <ion-item no-lines>\n\n          <button class="edit-button" item-start outline ion-button (click)="editItem(item)">\n\n            <p>EDIT</p>\n\n          </button>\n\n\n\n          <button *ngIf="!item.canAddToBag" class="add-to-bag-button" outline ion-button disabled>\n\n            <p>ADD TO BAG</p>\n\n          </button>\n\n\n\n          <button *ngIf="item.canAddToBag" class="add-to-bag-button" outline ion-button>\n\n            <p>ADD TO BAG</p>\n\n          </button>\n\n        </ion-item>\n\n      </ion-col>\n\n\n\n      <ion-col *ngIf="item.isInEditMode">\n\n        <ion-item no-lines class="colour-size" (click)="colourPopOver($event)">\n\n          <p item-start>COLOUR</p>\n\n          <ion-icon item-end name="ios-arrow-down"></ion-icon>\n\n        </ion-item>\n\n\n\n        <ion-item no-lines class="colour-size" (click)="sizePopOver($event)">\n\n          <p item-start>SIZE</p>\n\n          <ion-icon item-end name="ios-arrow-down"></ion-icon>\n\n        </ion-item>\n\n\n\n        <ion-row>\n\n          <ion-col text-center (click)="editItem(item)">\n\n            <button class="cancel-button" ion-button>\n\n              <p>CANCEL</p>\n\n            </button>\n\n          </ion-col>\n\n\n\n          <ion-col text-center (click)="editItem(item)">\n\n            <button class="apply-button" ion-button>\n\n              <p>APPLY</p>\n\n            </button>\n\n          </ion-col>\n\n        </ion-row>\n\n      </ion-col>\n\n    </ion-item>\n\n  </ion-list>\n\n</ion-content>'/*ion-inline-end:"C:\Users\domin\Documents\myacccim\myACCCIM App\asos-user\src\pages\saved-items\saved-items.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* PopoverController */]])
    ], SavedItemsPage);
    return SavedItemsPage;
}());

var Item = /** @class */ (function () {
    function Item() {
    }
    return Item;
}());

//# sourceMappingURL=saved-items.js.map

/***/ }),

/***/ 160:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MenWomenCategoryPopOverPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var MenWomenCategoryPopOverPage = /** @class */ (function () {
    function MenWomenCategoryPopOverPage(navCtrl, navParams, viewController, events) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewController = viewController;
        this.events = events;
    }
    MenWomenCategoryPopOverPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad MenWomenCategoryPopOverPage');
    };
    MenWomenCategoryPopOverPage.prototype.selectAndClosePopover = function (selection) {
        this.events.publish('genderShoppingChanged', selection == 'Men');
        this.viewController.dismiss();
    };
    MenWomenCategoryPopOverPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-men-women-category-pop-over',template:/*ion-inline-start:"C:\Users\domin\Documents\myacccim\myACCCIM App\asos-user\src\pages\men-women-category-pop-over\men-women-category-pop-over.html"*/'<ion-list no-margin no-lines>\n\n  <ion-item>\n\n    <button ion-item no-lines (click)="selectAndClosePopover(\'Men\')">\n\n      Men\n\n    </button>\n\n  </ion-item>\n\n\n\n  <ion-item>\n\n    <button ion-item no-lines (click)="selectAndClosePopover(\'Women\')">\n\n      Women\n\n    </button>\n\n  </ion-item>\n\n</ion-list>'/*ion-inline-end:"C:\Users\domin\Documents\myacccim\myACCCIM App\asos-user\src\pages\men-women-category-pop-over\men-women-category-pop-over.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* ViewController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */]])
    ], MenWomenCategoryPopOverPage);
    return MenWomenCategoryPopOverPage;
}());

//# sourceMappingURL=men-women-category-pop-over.js.map

/***/ }),

/***/ 161:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProductDetailsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common_http__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_http__ = __webpack_require__(77);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ProductDetailsPage = /** @class */ (function () {
    function ProductDetailsPage(navCtrl, navParams, popoverCtrl, ApiService, httpClient, https, view) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.popoverCtrl = popoverCtrl;
        this.ApiService = ApiService;
        this.httpClient = httpClient;
        this.https = https;
        this.view = view;
        this.description = '';
        this.price = 0;
        this.photos = [];
        this.i = 0;
        this.isMenSelected = navParams.get('isMenSelected');
        this.EventID = navParams.get('data');
        /* if (this.isMenSelected) {
           this.initialiseManProductDetails();
         }
         else {
           this.initialiseWomanProductDetails();
         }
       }*/
    }
    ProductDetailsPage_1 = ProductDetailsPage;
    ProductDetailsPage.prototype.ionViewDidLoad = function () {
        this.loadSpecificEvent();
        this.loadEvent();
    };
    ProductDetailsPage.prototype.loadSpecificEvent = function () {
        var _this = this;
        this.ApiService.getSpecificEvent(this.EventID)
            .subscribe(function (res) {
            var specificEvent = res.json();
            _this.showSpecEvent = specificEvent;
        });
    };
    ProductDetailsPage.prototype.loadEvent = function () {
        var _this = this;
        this.ApiService.getEvent()
            .subscribe(function (res) {
            var getEvent = res.json();
            _this.showEvent = getEvent;
            //console.log(this.showEvent.MemberAvailability_tb.MemberType);
        });
    };
    ProductDetailsPage.prototype.gotoEvent = function (EventID) {
        var _this = this;
        this.ApiService.getSpecificEvent(EventID).subscribe(function (res) {
            var getSpecificEvent = res.json();
            _this.showSpecificEvent = getSpecificEvent;
            _this.navCtrl.push(ProductDetailsPage_1, {
                data: EventID
            });
        });
    };
    ProductDetailsPage.prototype.read2 = function () {
        if (!this.i) {
            document.getElementById("more").style.display = "inline";
            document.getElementById("dots").style.display = "none";
            document.getElementById("read").innerHTML = "Read Less";
            this.i = 1;
        }
        else {
            document.getElementById("more").style.display = "none";
            document.getElementById("dots").style.display = "inline";
            document.getElementById("read").innerHTML = "Read More";
            this.i = 0;
        }
    };
    ProductDetailsPage = ProductDetailsPage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-product-details',template:/*ion-inline-start:"C:\Users\domin\Documents\myacccim\myACCCIM App\asos-user\src\pages\product-details\product-details.html"*/'\n\n<ion-header no-border>\n\n  <ion-navbar>\n\n    <!--<ion-buttons right>\n\n      <button class="navbar-button" ion-button icon-only>\n\n        <ion-icon name="md-share"></ion-icon>\n\n      </button>\n\n    </ion-buttons>-->\n\n    <ion-title>Programme</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n\n\n  <div *ngFor="let getspecevent of showSpecEvent">\n\n\n\n    <div style="text-align:center">\n\n        <div *ngIf="getspecevent.EventImage">\n\n            <ion-col col-12>\n\n                <img src="http://admin.acccim-registration.org.my{{getspecevent.EventImage}}">\n\n        \n\n            </ion-col>\n\n        </div>\n\n    \n\n        <div *ngIf="!getspecevent.EventImage">\n\n    \n\n            <ion-col col-12>\n\n                <img src="../../assets/imgs/ACCIMlogo.png">\n\n        \n\n            </ion-col>\n\n        </div>\n\n    </div>\n\n    \n\n    \n\n  </div>\n\n      \n\n    \n\n\n\n  <ion-grid style="margin-top: 8px;" id="product-details" *ngFor="let getspecevent of showSpecEvent">\n\n    <ion-row>\n\n      <ion-col text-center>\n\n       \n\n          \n\n          <p text-center><b>{{getspecevent.EventTitle}}</b></p>\n\n      \n\n      </ion-col>\n\n    </ion-row>\n\n\n\n    <ion-row>\n\n      <ion-col col-12 *ngIf="getspecevent.OrganiserName">\n\n        <p class="product-price">by {{getspecevent.OrganiserName}}</p>\n\n      </ion-col>\n\n      <ion-col col-12 *ngIf="!getspecevent.OrganiserName">\n\n        <p class="product-price">by ACCCIM</p>\n\n      </ion-col>\n\n      <ion-col col-2>\n\n          <label><ion-icon name="calendar" style="font-size:30px; margin-right:20px;"></ion-icon></label>\n\n      </ion-col>\n\n      <ion-col col-10>\n\n       \n\n        <label class="product-price" *ngIf="getspecevent.StartDate">{{getspecevent.StartDate | date: "yyyy/MM/dd"}}</label>-\n\n        <label class="product-price" *ngIf="getspecevent.EndDate">{{getspecevent.EndDate | date: "yyyy/MM/dd"}}</label> <br/>\n\n        <label class="product-price" *ngIf="getspecevent.StartTime">{{getspecevent.StartTime}}</label> -\n\n        <label class="product-price" *ngIf="getspecevent.EndTime">{{getspecevent.EndTime}}</label>\n\n      </ion-col>\n\n      <ion-col col-2>\n\n          <label><ion-icon name="pin" style="font-size:30px; margin-right:20px;"></ion-icon></label>\n\n      </ion-col>\n\n      <ion-col col-10>\n\n          <label class="product-price" *ngIf="getspecevent.VenueName">{{getspecevent.VenueName}}</label>\n\n          <label class="product-price" *ngIf="!getspecevent.VenueName">-</label><br/>\n\n          <label class="product-external-links" *ngIf="getspecevent.Address">{{getspecevent.Address}}</label>\n\n          <label class="product-external-links" *ngIf="!getspecevent.Address"></label>\n\n      </ion-col>\n\n\n\n      <ion-col col-2>\n\n          <label><ion-icon name="md-film" style="font-size:30px; margin-right:20px;"></ion-icon></label>\n\n      </ion-col>\n\n      <ion-col col-10>\n\n          <label class="product-price" *ngIf="getspecevent.EventType">{{getspecevent.EventType}}</label>\n\n          <label class="product-price" *ngIf="!getspecevent.EventType"></label><br/>\n\n          <!--<label class="product-price" *ngIf="getspecevent.EventTopic">{{getspecevent.EventTopic}}</label>\n\n          <label class="product-price" *ngIf="!getspecevent.EventTopic"></label>-->\n\n      </ion-col>\n\n    </ion-row>\n\n\n\n    <ion-row>\n\n      <ion-col col-12>\n\n        <p class="product-price">About</p>\n\n      </ion-col>\n\n      <ion-col col-12>\n\n          <label class="product-external-links" *ngIf="getspecevent.Description">{{getspecevent.Description}}</label>\n\n          <label class="product-external-links" *ngIf="!getspecevent.Description"></label>\n\n      </ion-col>\n\n    </ion-row>\n\n\n\n     <ion-row>\n\n       <ion-col col-12>\n\n         <p class="product-price">Location</p>\n\n       </ion-col>\n\n       <ion-col col-12>\n\n         <label class="product-external-links">{{getspecevent.VenueName}}</label>\n\n       </ion-col>\n\n       <ion-col col-12>\n\n\n\n       </ion-col>\n\n     </ion-row>\n\n\n\n     <ion-row>\n\n       <ion-col col-12>\n\n         <p class="product-price">Available Only For</p>\n\n       </ion-col>\n\n       <ion-col col-12>\n\n          <div *ngFor="let gett of getspecevent.MemberAvailability_tb">\n\n              <label class="product-external-links">{{gett.MemberType}}</label>\n\n            \n\n          </div>\n\n       </ion-col>\n\n     </ion-row>\n\n     <ion-row>\n\n       \n\n       <ion-col col-12>\n\n         <p class="product-price">Organiser</p>\n\n       </ion-col>\n\n       <ion-col col-12>\n\n          <img src="../../assets/imgs/Logo 1.jpg" style="width:40%; height: 90%;">\n\n       </ion-col>\n\n       <ion-col col-12>\n\n         <p class="product-external-links">\n\n           The ACCCIM National Council, at its meeting held on 25th August 2011, \n\n           formally resolved to form the ACCCIM Young Entrepreneurs Working Group, \n\n           and at the 1st Meeting of ACCCIM 66th National Council, \n\n           decided to change the name to ACCCIM Young Entrepreneurs Committee (YEC).<!--<span id="dots">...</span>\n\n           <span id="more"> ACCCIM 66th National Council, \n\n           decided to change the name to ACCCIM Young Entrepreneurs Committee (YEC).\n\n          <br/>\n\n          <br/>\n\n           ACCCIM has 17 Constituent Chambers in 13 states and Federal Territory of Malaysia. \n\n           All the 17 Constituent Chambers have formed their respective YEC, \n\n           attracting participation by young entrepreneurs and professionals from all sectors. \n\n           These YECs have now a combined membership of more than 5,000 and \n\n           have become one of the key working committees of ACCCIM and its respective Constituent Chambers.</span>\n\n           </p>-->\n\n          <!--<span id="read" (click)="read2()" style="color:blue; background-color: white;">Read More</span>-->\n\n          \n\n        </ion-col>\n\n     </ion-row>\n\n      \n\n    \n\n\n\n    \n\n  </ion-grid>\n\n\n\n <ion-grid id="related-products">\n\n    <ion-item class="buy-look-row">\n\n      <p class="buy-the-look" item-start>Browse More Programme</p>\n\n    \n\n    </ion-item>\n\n\n\n    <ion-scroll scrollX="true" scroll-avatar>\n\n      <ion-col class="scroll-item" padding *ngFor="let getevent of showEvent">\n\n        <div (click)="gotoEvent(getevent.EventID);">\n\n\n\n        \n\n        <img class="product-image" *ngIf="getevent.EventImage" src="http://admin.acccim-registration.org.my{{getevent.EventImage}}">\n\n        <img class="product-image" *ngIf="!getevent.EventImage" src="../../assets/imgs/ACCIMlogo.png">\n\n\n\n        <ion-item no-lines>\n\n         <p class="discount-price" item-start>{{getevent.EventTitle}}</p>\n\n        </ion-item>\n\n\n\n        <p class="product-brand-description" *ngIf="getevent.VenueName">Venue: {{getevent.VenueName}}</p>\n\n        <p class="product-brand-description" *ngIf="!getevent.VenueName">Venue: N/A</p>\n\n      </div>\n\n      </ion-col>\n\n\n\n    </ion-scroll>\n\n  </ion-grid>\n\n</ion-content>'/*ion-inline-end:"C:\Users\domin\Documents\myacccim\myACCCIM App\asos-user\src\pages\product-details\product-details.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* PopoverController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_3__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_4__angular_http__["a" /* Http */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* ViewController */]])
    ], ProductDetailsPage);
    return ProductDetailsPage;
    var ProductDetailsPage_1;
}());

/*initialiseManProductDetails() {
   this.description = 'Lee Sherpa Rider Denim Jacket Mid Wash';
   this.price = 218;

   this.photos.push('https://res.cloudinary.com/cediim8/image/upload/v1523345058/product-details-1.jpg');
   this.photos.push('https://res.cloudinary.com/cediim8/image/upload/v1523345058/product-details-2.jpg');
   this.photos.push('https://res.cloudinary.com/cediim8/image/upload/v1523345058/product-details-3.jpg');
   this.photos.push('https://res.cloudinary.com/cediim8/image/upload/v1523345058/product-details-4.jpg');

   this.otherProducts.push(new Product('Pull&Bear Denim Jacket In Black', 'https://res.cloudinary.com/cediim8/image/upload/v1523344614/product-1.jpg', 40, 30));
   this.otherProducts.push(new Product('Liquor N Poker Oversized Denim Jacket Stonewas', 'https://res.cloudinary.com/cediim8/image/upload/v1523344614/product-2.jpg', 89, 79));
   this.otherProducts.push(new Product('Puma T7 Vintage Track Jacket In White 57498506', 'https://res.cloudinary.com/cediim8/image/upload/v1523344614/product-3.jpg', 110, 99));
   this.otherProducts.push(new Product('New Look Cotton Bomber Jacket In Burgundy', 'https://res.cloudinary.com/cediim8/image/upload/v1523344614/product-8.jpg', 50, 35));
 }
 
 initialiseWomanProductDetails() {
   this.description = 'ASOS Cotton Mini Shirt Dress';
   this.price = 40;

   this.photos.push('https://res.cloudinary.com/cediim8/image/upload/v1523417552/product-details-women-1.jpg');
   this.photos.push('https://res.cloudinary.com/cediim8/image/upload/v1523417552/product-details-2-women.jpg');
   this.photos.push('https://res.cloudinary.com/cediim8/image/upload/v1523417552/product-details-3-women.jpg');
   this.photos.push('https://res.cloudinary.com/cediim8/image/upload/v1523417552/product-details-4-women.jpg');

   this.otherProducts.push(new Product('Stradivarius Polka Dot Shirt Dress', 'https://res.cloudinary.com/cediim8/image/upload/v1523415018/women-product-2', 40, 30));
   this.otherProducts.push(new Product('ASOS Ultimate Rolled Sleeve T-Shirt Dress With Tab', 'https://res.cloudinary.com/cediim8/image/upload/v1523415018/women-product-3', 30, 25));
   this.otherProducts.push(new Product('Boohoo One Shoulder Floral Midi Dress', 'https://res.cloudinary.com/cediim8/image/upload/v1523415018/women-product-4', 40, 30));
   this.otherProducts.push(new Product('Boohoo Off Shoulder Lemon Print Dress', 'https://res.cloudinary.com/cediim8/image/upload/v1523415018/women-product-5', 44, 40));
 }

 ionViewDidLoad() {
   console.log('ionViewDidLoad ProductDetailsPage');
 }

 sizePopOver(myEvent) {
   let popover = this.popoverCtrl.create(ProductSizePopOverPage);

   popover.present({
     ev: myEvent
   });
 }

 likeUnlike() {
   this.isLiked = !this.isLiked;
 }
}

export class Product {
 constructor(name: string, picture: string, price: number, discountPrice: number,
   ) {
   this.name = name;
   this.picture = picture;
   this.price = price;
   this.discountPrice = discountPrice;
 }

 name: string;
 picture: string;
 price: number;
 discountPrice: number;

 
}*/ 
//# sourceMappingURL=product-details.js.map

/***/ }),

/***/ 162:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SpeakerDetail1Page; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__(40);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the SpeakerDetail1Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var SpeakerDetail1Page = /** @class */ (function () {
    function SpeakerDetail1Page(navCtrl, navParams, http) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.version = "chinese";
        this.automaticClose = false;
        this.showLevel1 = null;
        this.showLevel2 = null;
        this.cshowLevel1 = null;
        this.cshowLevel2 = null;
        this.getCategory();
    }
    SpeakerDetail1Page.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SpeakerDetail1Page');
    };
    SpeakerDetail1Page.prototype.getCategory = function () {
        var _this = this;
        this.http.get('assets/speakerdetail1.json')
            .subscribe(function (res) {
            _this.pages = res['english'];
            _this.pages2 = res['chinese'];
        });
    };
    SpeakerDetail1Page.prototype.toggleLevel1 = function (idx) {
        if (this.isLevel1Shown(idx)) {
            this.showLevel1 = null;
        }
        else {
            this.showLevel1 = idx;
        }
    };
    ;
    SpeakerDetail1Page.prototype.toggleLevel2 = function (idx) {
        if (this.isLevel2Shown(idx)) {
            this.showLevel1 = null;
            this.showLevel2 = null;
        }
        else {
            this.showLevel1 = idx;
            this.showLevel2 = idx;
        }
    };
    ;
    SpeakerDetail1Page.prototype.isLevel1Shown = function (idx) {
        return this.showLevel1 === idx;
    };
    ;
    SpeakerDetail1Page.prototype.isLevel2Shown = function (idx) {
        return this.showLevel2 === idx;
    };
    ;
    /*Chinese*/
    SpeakerDetail1Page.prototype.ctoggleLevel1 = function (idx) {
        if (this.cisLevel1Shown(idx)) {
            this.cshowLevel1 = null;
        }
        else {
            this.cshowLevel1 = idx;
        }
    };
    ;
    SpeakerDetail1Page.prototype.ctoggleLevel2 = function (idx) {
        if (this.cisLevel2Shown(idx)) {
            this.cshowLevel1 = null;
            this.cshowLevel2 = null;
        }
        else {
            this.cshowLevel1 = idx;
            this.cshowLevel2 = idx;
        }
    };
    ;
    SpeakerDetail1Page.prototype.cisLevel1Shown = function (idx) {
        return this.cshowLevel1 === idx;
    };
    ;
    SpeakerDetail1Page.prototype.cisLevel2Shown = function (idx) {
        return this.cshowLevel2 === idx;
    };
    ;
    SpeakerDetail1Page = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-speaker-detail1',template:/*ion-inline-start:"C:\Users\domin\Documents\myacccim\myACCCIM App\asos-user\src\pages\speaker-detail1\speaker-detail1.html"*/'<ion-header>\n\n    <ion-navbar>\n\n      <ion-buttons left>\n\n        <button ion-button menuToggle>\n\n          <ion-icon name="menu"></ion-icon>\n\n        </button>\n\n      </ion-buttons>\n\n  \n\n      <ion-title>Speakers</ion-title>\n\n    </ion-navbar>\n\n  </ion-header>\n\n  \n\n  <ion-content padding>\n\n    <div style="text-align:center;">\n\n        <img src="../../assets/imgs/TanSriDatukTerLeongYap.jpg">\n\n    </div>\n\n    \n\n    <ion-row>\n\n        <ion-col col-12>\n\n          <p text-center style="font-size:20px;">丹斯里拿督戴良业</p>\n\n          <p text-center style="font-size:20px;">Tan Sri Datuk Ter Leong Yap</p>\n\n        </ion-col>\n\n      </ion-row>\n\n    \n\n      <ion-row>\n\n          <ion-col col-12>\n\n            <p text-center>征阳集团 执行主席\n\n                Executive Chairman,Sunsuria Berhad</p>\n\n          </ion-col>\n\n        </ion-row>\n\n\n\n        <ion-row>\n\n            <ion-col col-12>\n\n              <p text-center>PSM PJN AMN</p>\n\n            </ion-col>\n\n          </ion-row>\n\n\n\n          <ion-list class="orders-list">\n\n              <ion-item>\n\n                <ion-icon item-start name="md-mail"></ion-icon>\n\n                <p>lyter@sunsuria.com</p>\n\n              </ion-item>\n\n          </ion-list>\n\n\n\n          <ion-segment [(ngModel)]="version">\n\n\n\n              <ion-segment-button value="chinese">\n\n                  <p class="item-title" style="color:#ED6D10;">Chinese</p>\n\n                </ion-segment-button>\n\n      \n\n            <ion-segment-button value="english">\n\n              <p class="item-title" style="color:#ED6D10;">English</p>\n\n      \n\n            </ion-segment-button>\n\n      \n\n            </ion-segment>\n\n\n\n\n\n   <div [ngSwitch]="version">\n\n          <div *ngSwitchCase="\'chinese\'">\n\n              <ion-list>\n\n                  <ion-item *ngFor="let p of pages2; let i=index" text-wrap (click)="ctoggleLevel1(\'idx\'+i)" [ngClass]="{active: cisLevel1Shown(\'idx\'+i)}">\n\n                    <h4>\n\n                      {{p.cfirstlayer}}\n\n                      <ion-icon color="success" item-right [name]="cisLevel1Shown(\'idx\'+i) ? \'arrow-dropdown\' : \'arrow-dropright\'"></ion-icon>\n\n                    </h4>\n\n                    <ion-list *ngIf="cisLevel1Shown(\'idx\'+i)">\n\n                      <ion-item *ngFor="let s of p.csubs; let i2=index" text-wrap (click)="ctoggleLevel2(\'idx\'+i+\'idx\'+i2)" [ngClass]="{active: cisLevel2Shown(\'idx\'+i+\'idx\'+i2)}">\n\n                       \n\n                        <h4 style="line-height: 2.0em;">\n\n                          {{s.csecondlayer}}\n\n                          <ion-icon color="success" item-right [name]="cisLevel2Shown(\'idx\'+i+\'idx\'+i2) ? \'arrow-dropdown\' : \'arrow-dropright\'"></ion-icon>\n\n                        </h4>\n\n                        <ion-list *ngIf="cisLevel2Shown(\'idx\'+i+\'idx\'+i2)">\n\n                          <ion-item *ngFor="let m of s.csecondsub" text-wrap>\n\n                           <p style="line-height: 2.0em; font-size: 20px;" *ngIf="m.cdate">{{m.cdate}}</p>\n\n                          <p style="line-height: 2.0em; font-size: 15px; font-weight:bold;" *ngIf="m.cname">{{m.cname}}</p>\n\n                          <p style="line-height: 2.0em;" *ngIf="m.cdescription1">- {{m.cdescription1}}</p>\n\n                          <p style="line-height: 2.0em;" *ngIf="m.cdescription2">- {{m.cdescription2}}</p>\n\n                          <p style="line-height: 2.0em;" *ngIf="m.cdescription3">- {{m.cdescription3}}</p>\n\n                          <p style="line-height: 2.0em;" *ngIf="m.cdescription4">- {{m.cdescription4}}</p>\n\n                          <p style="line-height: 2.0em;" *ngIf="m.cdescription5">- {{m.cdescription5}}</p>\n\n                          <p style="line-height: 2.0em;" *ngIf="m.cdescription6">- {{m.cdescription6}}</p>\n\n                          <p style="line-height: 2.0em;" *ngIf="m.cdescription7">- {{m.cdescription7}}</p>\n\n                          <p style="line-height: 2.0em;" *ngIf="m.cdescription8">- {{m.cdescription8}}</p>\n\n                          <p style="line-height: 2.0em;" *ngIf="m.cdescription9">- {{m.cdescription9}}</p>\n\n                          <p style="line-height: 2.0em;" *ngIf="m.cdescription10">- {{m.cdescription10}}</p>\n\n                          <p style="line-height: 2.0em;" *ngIf="m.cdescription11">- {{m.cdescription11}}</p>\n\n                          </ion-item>\n\n                        </ion-list>\n\n                      </ion-item>\n\n                    </ion-list>\n\n                  </ion-item>\n\n                </ion-list>\n\n          </div>\n\n      </div>\n\n\n\n      <div [ngSwitch]="version">\n\n          <div *ngSwitchCase="\'english\'">\n\n              <ion-list>\n\n                  <ion-item *ngFor="let p of pages; let i=index" text-wrap (click)="toggleLevel1(\'idx\'+i)" [ngClass]="{active: isLevel1Shown(\'idx\'+i)}">\n\n                    <h4>\n\n                      {{p.efirstlayer}}\n\n                      <ion-icon color="success" item-right [name]="isLevel1Shown(\'idx\'+i) ? \'arrow-dropdown\' : \'arrow-dropright\'"></ion-icon>\n\n                    </h4>\n\n                    <ion-list *ngIf="isLevel1Shown(\'idx\'+i)">\n\n                      <ion-item *ngFor="let s of p.esubs; let i2=index" text-wrap (click)="toggleLevel2(\'idx\'+i+\'idx\'+i2)" [ngClass]="{active: isLevel2Shown(\'idx\'+i+\'idx\'+i2)}">\n\n                       \n\n                        <h4 style="line-height: 2.0em;">\n\n                          {{s.esecondlayer}}\n\n                          <ion-icon color="success" item-right [name]="isLevel2Shown(\'idx\'+i+\'idx\'+i2) ? \'arrow-dropdown\' : \'arrow-dropright\'"></ion-icon>\n\n                        </h4>\n\n                        <ion-list *ngIf="isLevel2Shown(\'idx\'+i+\'idx\'+i2)">\n\n                          <ion-item *ngFor="let m of s.esecondsub" text-wrap>\n\n                          <p style="line-height: 2.0em; font-size: 20px;" *ngIf="m.edate">{{m.edate}}</p>\n\n                          <p style="line-height: 2.0em; font-size: 15px; font-weight:bold;" *ngIf="m.ename">{{m.ename}}</p>\n\n                          <p style="line-height: 2.0em;" *ngIf="m.epoint1">{{m.epoint1}}</p>\n\n                          <p style="line-height: 2.0em;" *ngIf="m.edescription1">- {{m.edescription1}}</p>\n\n                          <p style="line-height: 2.0em;" *ngIf="m.edescription2">- {{m.edescription2}}</p>\n\n                          <p style="line-height: 2.0em;" *ngIf="m.edescription3">- {{m.edescription3}}</p>\n\n                          <p style="line-height: 2.0em;" *ngIf="m.edescription4">- {{m.edescription4}}</p>\n\n                          <p style="line-height: 2.0em;" *ngIf="m.edescription5">- {{m.edescription5}}</p>\n\n                          <p style="line-height: 2.0em;" *ngIf="m.edescription6">- {{m.edescription6}}</p>\n\n                          <p style="line-height: 2.0em;" *ngIf="m.edescription7">- {{m.edescription7}}</p>\n\n                          <p style="line-height: 2.0em;" *ngIf="m.edescription8">- {{m.edescription8}}</p>\n\n                          <p style="line-height: 2.0em;" *ngIf="m.edescription9">- {{m.edescription9}}</p>\n\n                          <p style="line-height: 2.0em;" *ngIf="m.edescription10">- {{m.edescription10}}</p>\n\n                          <p style="line-height: 2.0em;" *ngIf="m.edescription11">- {{m.edescription11}}</p>\n\n                          \n\n                          </ion-item>\n\n                        </ion-list>\n\n                      </ion-item>\n\n                    </ion-list>\n\n                  </ion-item>\n\n                </ion-list>\n\n          </div>\n\n      </div>\n\n    <!-- <ion-list class="orders-list">\n\n      <ion-item>\n\n        <ion-icon item-start name="ios-basket-outline"></ion-icon>\n\n        <p>My orders</p>\n\n      </ion-item>\n\n  \n\n      <ion-item>\n\n        <ion-icon item-start name="ios-archive-outline"></ion-icon>\n\n        <p>My returns</p>\n\n      </ion-item>\n\n  \n\n      <ion-item>\n\n        <ion-icon item-start name="md-bicycle"></ion-icon>\n\n        <p>Premier Delivery</p>\n\n      </ion-item>\n\n    </ion-list>\n\n  \n\n    <ion-list>\n\n      <ion-item>\n\n        <ion-icon item-start name="ios-bookmarks-outline"></ion-icon>\n\n        <p>My details</p>\n\n      </ion-item>\n\n  \n\n      <ion-item>\n\n        <ion-icon item-start name="ios-home-outline"></ion-icon>\n\n        <p>Address Book</p>\n\n      </ion-item>\n\n  \n\n      <ion-item>\n\n        <ion-icon item-start name="ios-card"></ion-icon>\n\n        <p>Payment methods</p>\n\n      </ion-item>\n\n  \n\n      <ion-item>\n\n        <ion-icon item-start name="ios-text-outline"></ion-icon>\n\n        <p>Contact Preferences</p>\n\n      </ion-item>\n\n  \n\n      <ion-item>\n\n        <ion-icon item-start name="ios-contacts-outline"></ion-icon>\n\n        <p>Social accounts</p>\n\n      </ion-item>\n\n    </ion-list>\n\n  \n\n    <ion-list>\n\n      <ion-item>\n\n        <ion-icon item-start name="ios-game-controller-b-outline"></ion-icon>\n\n        <p>Gift card & vouchers</p>\n\n      </ion-item>\n\n    </ion-list>\n\n  \n\n    <ion-list>\n\n      <ion-item>\n\n        <ion-icon item-start name="ios-help-circle-outline"></ion-icon>\n\n        <p>Need help?</p>\n\n      </ion-item>\n\n  \n\n      <ion-item>\n\n        <ion-icon item-start name="ios-browsers-outline"></ion-icon>\n\n        <p>Where\'s my order?</p>\n\n      </ion-item>\n\n  \n\n      <ion-item>\n\n        <ion-icon item-start name="ios-browsers-outline"></ion-icon>\n\n        <p>How do I make a return?</p>\n\n      </ion-item>\n\n    </ion-list>\n\n  \n\n    <ion-item>\n\n      <ion-icon item-start name="md-log-out"></ion-icon>\n\n      <p>Sign out</p>\n\n    </ion-item> -->\n\n  </ion-content>'/*ion-inline-end:"C:\Users\domin\Documents\myacccim\myACCCIM App\asos-user\src\pages\speaker-detail1\speaker-detail1.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["a" /* HttpClient */]])
    ], SpeakerDetail1Page);
    return SpeakerDetail1Page;
}());

//# sourceMappingURL=speaker-detail1.js.map

/***/ }),

/***/ 163:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SpeakerDetail2Page; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the SpeakerDetail2Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var SpeakerDetail2Page = /** @class */ (function () {
    function SpeakerDetail2Page(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.version = "chinese";
    }
    SpeakerDetail2Page.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SpeakerDetail2Page');
    };
    SpeakerDetail2Page = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-speaker-detail2',template:/*ion-inline-start:"C:\Users\domin\Documents\myacccim\myACCCIM App\asos-user\src\pages\speaker-detail2\speaker-detail2.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <ion-buttons left>\n\n      <button ion-button menuToggle>\n\n        <ion-icon name="menu"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n\n\n    <ion-title>Speakers</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n  <div style="text-align:center;">\n\n      <img src="../../assets/imgs/Mr.ChinSooMau.jpg">\n\n  </div>\n\n \n\n\n\n <ion-list class="orders-list">\n\n\n\n  <ion-row>\n\n    <ion-col col-12>\n\n      <p text-center style="font-size:20px;">秦树茂先生</p>\n\n      <p text-center style="font-size:20px;">Mr Chin Soo Mau</p>\n\n    </ion-col>\n\n  </ion-row>\n\n\n\n  <ion-row>\n\n      <ion-col col-12>\n\n        <p text-center>马来西亚光伏产业协会2019/2020届会长</p>\n\n      </ion-col>\n\n    </ion-row>\n\n\n\n    <ion-row>\n\n        <ion-col col-12>\n\n          <p text-center>President of the Malaysian Photovoltaic Industry Association (MPIA) for the term 2019/2020.</p>\n\n        </ion-col>\n\n      </ion-row>\n\n    \n\n\n\n  \n\n\n\n    \n\n  </ion-list>\n\n\n\n  \n\n  <ion-segment [(ngModel)]="version">\n\n\n\n      <ion-segment-button value="chinese">\n\n          <p class="item-title" style="color:#ED6D10;">Chinese</p>\n\n        </ion-segment-button>\n\n\n\n    <ion-segment-button value="english">\n\n      <p class="item-title" style="color:#ED6D10;">English</p>\n\n\n\n    </ion-segment-button>\n\n\n\n  \n\n\n\n  </ion-segment>\n\n\n\n\n\n  <div [ngSwitch]="version">\n\n      <div *ngSwitchCase="\'chinese\'">\n\n\n\n          <ion-row>\n\n              <ion-col>\n\n                <p style="line-height: 2.0em;">秦树茂先生是马来西亚光伏产业协会2019/2020届 会长， 也是该协会2017/2018届会长及在2013-2016年担任副会长。 自2010年加入该协会， 他一直是活跃会员。 他从事太阳能光伏事业多年， 对光伏领域有渊博的知识和实际经验， 以致马来西亚政府在有关的政策规划上借助他的专长， 例如受委马来西亚太阳能路线图工作组2016-2030 成员、马来西亚可再生能源技术委员会委员、马来西亚能源储存系统项目资源人士等。 他持有英国电机工程高级国家文凭、并网和离网光伏系统设计师资格证书、可持续能源发展局(SEDA)注册服务提供商、国际可持续质量认证光伏系统设计资格证书等。 他目前是PEKAT 集团的集团董事经理， 该集团拥有Pekat Solar 私人有限公司、Pekat 工程私人有限公司及Pekat E&LP 私人有限公司。</p>\n\n              </ion-col>\n\n            </ion-row>\n\n        </div>\n\n        </div>\n\n\n\n\n\n        <div [ngSwitch]="version">\n\n            <div *ngSwitchCase="\'english\'">\n\n                <ion-row>\n\n                    <ion-col>\n\n                      <p style="line-height: 2.0em;">Mr Chin Soo Mau is the President of the Malaysian Photovoltaic Industry Association (MPIA) for the term 2019/2020. He was also the President for 2017/2018, the Vice-President from 2013-2016 and has been an active member since 2010. He has been involved in the solar PV business for many years and his knowledge and field experience have prompted the Malaysian Government to tap his expertise in policy-related planning such as: Member of the Working Group on Malaysia Solar Roadmap (MSR) 2016-2030, Member of the Technical Committee of Malaysian Standards on Renewable Energy (TC RE), Resource Person of the Energy Storage System (ESS) Project for Malaysia. Mr Chin holds a Higher National Diploma in Electrical Engineering, United Kingdom, Competent Certificates of both Grid-Connected and Off-Grid Photovoltaic Systems Designer, Sustainable Energy Development Authority (SEDA) registered Service Provider and International Sustainable Power Quality-accredited PV Systems Design Competency Certificates. He is the Group Managing Director of PEKAT Group of Companies that holds Pekat Solar Sdn Bhd, Pekat Engineering Sdn Bhd and Pekat E&LP Sdn Bhd.\n\n                       </p>\n\n                    </ion-col>\n\n                  </ion-row>\n\n              </div>\n\n              </div>\n\n\n\n \n\n  \n\n</ion-content>'/*ion-inline-end:"C:\Users\domin\Documents\myacccim\myACCCIM App\asos-user\src\pages\speaker-detail2\speaker-detail2.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */]])
    ], SpeakerDetail2Page);
    return SpeakerDetail2Page;
}());

//# sourceMappingURL=speaker-detail2.js.map

/***/ }),

/***/ 164:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SpeakerDetail3Page; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the SpeakerDetail3Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var SpeakerDetail3Page = /** @class */ (function () {
    function SpeakerDetail3Page(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.version = "chinese";
    }
    SpeakerDetail3Page.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SpeakerDetail3Page');
    };
    SpeakerDetail3Page = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-speaker-detail3',template:/*ion-inline-start:"C:\Users\domin\Documents\myacccim\myACCCIM App\asos-user\src\pages\speaker-detail3\speaker-detail3.html"*/'<ion-header>\n\n    <ion-navbar>\n\n      <ion-buttons left>\n\n        <button ion-button menuToggle>\n\n          <ion-icon name="menu"></ion-icon>\n\n        </button>\n\n      </ion-buttons>\n\n  \n\n      <ion-title>Speakers</ion-title>\n\n    </ion-navbar>\n\n  </ion-header>\n\n  \n\n  <ion-content padding>\n\n    <div style="text-align:center">\n\n        <img src="../../assets/imgs/Mr.CharlesChuang.png">\n\n    </div>\n\n         \n\n\n\n    <ion-row>\n\n        <ion-col col-12>\n\n          <p text-center style="font-size:20px;">庄嘉明先生</p>\n\n          <p text-center style="font-size:20px;">Mr. Charles Chuang</p>\n\n        </ion-col>\n\n      </ion-row>\n\n    \n\n      <ion-row>\n\n          <ion-col col-12>\n\n            <p text-center>创奕能源科技股份有限公司技术长</p>\n\n          </ion-col>\n\n        </ion-row>\n\n    \n\n        <ion-row>\n\n            <ion-col col-12>\n\n              <p text-center>Chief Technology Officer of Tron Energy Technology Corp</p>\n\n            </ion-col>\n\n          </ion-row>\n\n\n\n\n\n    <ion-segment [(ngModel)]="version">\n\n\n\n        <ion-segment-button value="chinese">\n\n            <p class="item-title" style="color:#ED6D10;">Chinese</p>\n\n          </ion-segment-button>\n\n\n\n      <ion-segment-button value="english">\n\n        <p class="item-title" style="color:#ED6D10;">English</p>\n\n\n\n      </ion-segment-button>\n\n\n\n    \n\n\n\n    </ion-segment>\n\n\n\n    <div [ngSwitch]="version">\n\n        <div *ngSwitchCase="\'chinese\'">\n\n            <ion-row>\n\n                <ion-col>\n\n                    <p style="line-height: 2.0em;">庄嘉明出生于台北，在青少年时移居美国。他在康乃尔大学以最优异的成绩完成电机工程学位，硕士则是在加州大学柏克莱分校取得。 他的第一份工作是在硅谷惠普，被封为惠普人机介面汇流排(HP Human Interface Loop) 之父。惠普人机介面汇流排在12年后进化演变成 USB业界标准。 在美国慧智 (Wyse Technology)他带领研发团队设计一系列最畅销的终端机及显示器。在美国Network Computing Devices他的X终端机产品连续领先业界达五年之久。在彩富电子，他发明了超微影像辨识技术，使阴极射像管调整可以无人化，并因此获取全球自动调整系统60%市占率。捷源绿能科技(Atieva, Inc.)为特斯拉原始电池团队所创，他以亚太区总经理职位建立中国总部，上海工厂，并设计动力电池系统给予公交巴士，并达成三千万公里，零事故记录。目前他在创奕能源担任技术长，致力于电动巴士， 无人巴士，及储能系统之开发与研究。创奕能源预计在2020年上市，这将是他职业生涯第四个IPO。 庄嘉明目前研究重点为高信赖度电池系统及无人车技术以切入5G智慧城市的应用。他已获得13个美国专利， 其中9项在新能源领域。他同时也发布过14篇技术文章。</p>\n\n                </ion-col>\n\n              </ion-row>\n\n        </div>\n\n    </div>\n\n   \n\n\n\n    <div [ngSwitch]="version">\n\n        <div *ngSwitchCase="\'english\'">\n\n            <ion-row>\n\n                <ion-col>\n\n                  <p style="line-height: 2.0em;">Charles was born in Taiwan, and moved to the United States while a teenager. He graduated Summa cum laude from Cornell University with a BS Electrical Engineering degree. He completed his MSEE degree at University of California at Berkeley. Charles worked in Silicon Valley for 16 years. At Hewlett Packard, he was nick-named “father of Human Interface Loop” for inventing USB like functions 12 years before the standard was published. At Wyse Technology he led a team that designed more than two dozen award winning computer terminals and displays. At Network Computing Devices, he led a team that held #1 position in the X-terminal market for 5 years. At DynaColor, he invented micro subpixel algorithm that enables CRT display to be automated and secured 60% world market share. Atieva, Inc. was founded by the initial battery team from Tesla Motors. Charles was Atieva’s VP and Asia Pacific general manager that set up China headquarter and factory in Shanghai and designed EV battery packs with over 30 Million kilometer safety track record in China. Presently, he is Chief Technology Officer at Tron Energy Technology Corporation (Tron-e) focusing on EV buses, autonomous buses, and energy storage systems. Tron-e is expected to have initial public offering (IPO) in 2020, the 4th IPO in Charles’ career. Charles current research interest is in ultra-reliable battery packs and vehicle innovations for Smart City applications. Charles has 13 US patents to this credit (9 in new energy field). He has also published 14 technical papers.</p>\n\n                </ion-col>\n\n              </ion-row>\n\n          </div>\n\n          </div>\n\n   \n\n  \n\n   \n\n  \n\n    <!-- <ion-list class="orders-list">\n\n      <ion-item>\n\n        <ion-icon item-start name="ios-basket-outline"></ion-icon>\n\n        <p>My orders</p>\n\n      </ion-item>\n\n  \n\n      <ion-item>\n\n        <ion-icon item-start name="ios-archive-outline"></ion-icon>\n\n        <p>My returns</p>\n\n      </ion-item>\n\n  \n\n      <ion-item>\n\n        <ion-icon item-start name="md-bicycle"></ion-icon>\n\n        <p>Premier Delivery</p>\n\n      </ion-item>\n\n    </ion-list>\n\n  \n\n    <ion-list>\n\n      <ion-item>\n\n        <ion-icon item-start name="ios-bookmarks-outline"></ion-icon>\n\n        <p>My details</p>\n\n      </ion-item>\n\n  \n\n      <ion-item>\n\n        <ion-icon item-start name="ios-home-outline"></ion-icon>\n\n        <p>Address Book</p>\n\n      </ion-item>\n\n  \n\n      <ion-item>\n\n        <ion-icon item-start name="ios-card"></ion-icon>\n\n        <p>Payment methods</p>\n\n      </ion-item>\n\n  \n\n      <ion-item>\n\n        <ion-icon item-start name="ios-text-outline"></ion-icon>\n\n        <p>Contact Preferences</p>\n\n      </ion-item>\n\n  \n\n      <ion-item>\n\n        <ion-icon item-start name="ios-contacts-outline"></ion-icon>\n\n        <p>Social accounts</p>\n\n      </ion-item>\n\n    </ion-list>\n\n  \n\n    <ion-list>\n\n      <ion-item>\n\n        <ion-icon item-start name="ios-game-controller-b-outline"></ion-icon>\n\n        <p>Gift card & vouchers</p>\n\n      </ion-item>\n\n    </ion-list>\n\n  \n\n    <ion-list>\n\n      <ion-item>\n\n        <ion-icon item-start name="ios-help-circle-outline"></ion-icon>\n\n        <p>Need help?</p>\n\n      </ion-item>\n\n  \n\n      <ion-item>\n\n        <ion-icon item-start name="ios-browsers-outline"></ion-icon>\n\n        <p>Where\'s my order?</p>\n\n      </ion-item>\n\n  \n\n      <ion-item>\n\n        <ion-icon item-start name="ios-browsers-outline"></ion-icon>\n\n        <p>How do I make a return?</p>\n\n      </ion-item>\n\n    </ion-list>\n\n  \n\n    <ion-item>\n\n      <ion-icon item-start name="md-log-out"></ion-icon>\n\n      <p>Sign out</p>\n\n    </ion-item> -->\n\n  </ion-content>'/*ion-inline-end:"C:\Users\domin\Documents\myacccim\myACCCIM App\asos-user\src\pages\speaker-detail3\speaker-detail3.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */]])
    ], SpeakerDetail3Page);
    return SpeakerDetail3Page;
}());

//# sourceMappingURL=speaker-detail3.js.map

/***/ }),

/***/ 165:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SpeakerDetail5Page; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__(40);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the SpeakerDetail5Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var SpeakerDetail5Page = /** @class */ (function () {
    function SpeakerDetail5Page(navCtrl, navParams, http) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.showLevel1 = null;
        this.showLevel2 = null;
        this.getCategory();
    }
    SpeakerDetail5Page.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SpeakerDetail5Page');
    };
    SpeakerDetail5Page.prototype.getCategory = function () {
        var _this = this;
        this.http.get('assets/speakerdetail5.json')
            .subscribe(function (res) {
            _this.pages = res['english'];
        });
    };
    SpeakerDetail5Page.prototype.toggleLevel1 = function (idx) {
        if (this.isLevel1Shown(idx)) {
            this.showLevel1 = null;
        }
        else {
            this.showLevel1 = idx;
        }
    };
    ;
    SpeakerDetail5Page.prototype.toggleLevel2 = function (idx) {
        if (this.isLevel2Shown(idx)) {
            this.showLevel1 = null;
            this.showLevel2 = null;
        }
        else {
            this.showLevel1 = idx;
            this.showLevel2 = idx;
        }
    };
    ;
    SpeakerDetail5Page.prototype.isLevel1Shown = function (idx) {
        return this.showLevel1 === idx;
    };
    ;
    SpeakerDetail5Page.prototype.isLevel2Shown = function (idx) {
        return this.showLevel2 === idx;
    };
    ;
    SpeakerDetail5Page = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-speaker-detail5',template:/*ion-inline-start:"C:\Users\domin\Documents\myacccim\myACCCIM App\asos-user\src\pages\speaker-detail5\speaker-detail5.html"*/'<ion-header>\n\n    <ion-navbar>\n\n      <ion-buttons left>\n\n        <button ion-button menuToggle>\n\n          <ion-icon name="menu"></ion-icon>\n\n        </button>\n\n      </ion-buttons>\n\n  \n\n      <ion-title>Speakers</ion-title>\n\n    </ion-navbar>\n\n  </ion-header>\n\n  \n\n  <ion-content padding>\n\n\n\n\n\n    <div style="text-align:center;">\n\n        <img src="../../assets/imgs/Mr.MichaelLeong.jpg">\n\n\n\n    </div>\n\n\n\n    <ion-row>\n\n        <ion-col col-12>\n\n          <p text-center  style="font-size:20px;">Michael Leong</p>\n\n        </ion-col>\n\n      </ion-row>\n\n    \n\n      <ion-row>\n\n          <ion-col col-12>\n\n            <p text-center>Cheif Executive Officer,TERA VA S/B</p>\n\n          </ion-col>\n\n        </ion-row>\n\n    \n\n        \n\n        <ion-row>\n\n            <ion-col col-12>\n\n              <p text-center>Grid Connected & Off Grid Solar PV System(FiT,NeM,SelCo & Off Grid)</p>\n\n            </ion-col>\n\n          </ion-row>\n\n\n\n          <ion-row>\n\n              <ion-col col-12>\n\n                <p text-center>Secretary of Malaysia Photovoltaic Industry Association(MPIA),FY19/20</p>\n\n              </ion-col>\n\n            </ion-row>\n\n        \n\n            \n\n                    <ion-list>\n\n                        <ion-item *ngFor="let p of pages; let i=index" text-wrap (click)="toggleLevel1(\'idx\'+i)" [ngClass]="{active: isLevel1Shown(\'idx\'+i)}">\n\n                          <h4>\n\n                            {{p.efirstlayer}}\n\n                            <ion-icon color="success" item-right [name]="isLevel1Shown(\'idx\'+i) ? \'arrow-dropdown\' : \'arrow-dropright\'"></ion-icon>\n\n                          </h4>\n\n                          <ion-list *ngIf="isLevel1Shown(\'idx\'+i)">\n\n                            <ion-item *ngFor="let s of p.esubs; let i2=index" text-wrap (click)="toggleLevel2(\'idx\'+i+\'idx\'+i2)" [ngClass]="{active: isLevel2Shown(\'idx\'+i+\'idx\'+i2)}">\n\n                             \n\n                              <h4 style="line-height: 2.0em;">\n\n                                {{s.esecondlayer}}\n\n                                <ion-icon color="success" item-right [name]="isLevel2Shown(\'idx\'+i+\'idx\'+i2) ? \'arrow-dropdown\' : \'arrow-dropright\'"></ion-icon>\n\n                              </h4>\n\n                              <ion-list *ngIf="isLevel2Shown(\'idx\'+i+\'idx\'+i2)">\n\n                                <ion-item *ngFor="let m of s.esecondsub" text-wrap>\n\n                                <p style="line-height: 2.0em; font-size:20px;" *ngIf="m.edescription6">{{m.edescription6}}</p>\n\n                                <p style="line-height: 2.0em;" *ngIf="m.edescription1">- {{m.edescription1}}</p>\n\n                                <p style="line-height: 2.0em;" *ngIf="m.edescription2">- {{m.edescription2}}</p>\n\n                                <p style="line-height: 2.0em;" *ngIf="m.edescription3">- {{m.edescription3}}</p>\n\n                                <p style="line-height: 2.0em;" *ngIf="m.edescription4">- {{m.edescription4}}</p>\n\n                                <p style="line-height: 2.0em;" *ngIf="m.edescription5">- {{m.edescription5}}</p>\n\n                                \n\n                                </ion-item>\n\n                              </ion-list>\n\n                            </ion-item>\n\n                          </ion-list>\n\n                        </ion-item>\n\n                      </ion-list>\n\n              \n\n   \n\n  \n\n   \n\n  </ion-content>'/*ion-inline-end:"C:\Users\domin\Documents\myacccim\myACCCIM App\asos-user\src\pages\speaker-detail5\speaker-detail5.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["a" /* HttpClient */]])
    ], SpeakerDetail5Page);
    return SpeakerDetail5Page;
}());

//# sourceMappingURL=speaker-detail5.js.map

/***/ }),

/***/ 166:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SpeakerDetail4Page; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__(40);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the SpeakerDetail4Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var SpeakerDetail4Page = /** @class */ (function () {
    function SpeakerDetail4Page(navCtrl, navParams, http) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.version = "chinese";
        this.showLevel1 = null;
        this.showLevel2 = null;
        this.cshowLevel1 = null;
        this.cshowLevel2 = null;
        this.getCategory();
    }
    SpeakerDetail4Page.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SpeakerDetail4Page');
    };
    SpeakerDetail4Page.prototype.getCategory = function () {
        var _this = this;
        this.http.get('assets/speakerdetail4.json')
            .subscribe(function (res) {
            _this.pages = res['english'];
            _this.pages2 = res['chinese'];
        });
    };
    SpeakerDetail4Page.prototype.toggleLevel1 = function (idx) {
        if (this.isLevel1Shown(idx)) {
            this.showLevel1 = null;
        }
        else {
            this.showLevel1 = idx;
        }
    };
    ;
    SpeakerDetail4Page.prototype.toggleLevel2 = function (idx) {
        if (this.isLevel2Shown(idx)) {
            this.showLevel1 = null;
            this.showLevel2 = null;
        }
        else {
            this.showLevel1 = idx;
            this.showLevel2 = idx;
        }
    };
    ;
    SpeakerDetail4Page.prototype.isLevel1Shown = function (idx) {
        return this.showLevel1 === idx;
    };
    ;
    SpeakerDetail4Page.prototype.isLevel2Shown = function (idx) {
        return this.showLevel2 === idx;
    };
    ;
    /*Chinese*/
    SpeakerDetail4Page.prototype.ctoggleLevel1 = function (idx) {
        if (this.cisLevel1Shown(idx)) {
            this.cshowLevel1 = null;
        }
        else {
            this.cshowLevel1 = idx;
        }
    };
    ;
    SpeakerDetail4Page.prototype.ctoggleLevel2 = function (idx) {
        if (this.cisLevel2Shown(idx)) {
            this.cshowLevel1 = null;
            this.cshowLevel2 = null;
        }
        else {
            this.cshowLevel1 = idx;
            this.cshowLevel2 = idx;
        }
    };
    ;
    SpeakerDetail4Page.prototype.cisLevel1Shown = function (idx) {
        return this.cshowLevel1 === idx;
    };
    ;
    SpeakerDetail4Page.prototype.cisLevel2Shown = function (idx) {
        return this.cshowLevel2 === idx;
    };
    ;
    SpeakerDetail4Page = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-speaker-detail4',template:/*ion-inline-start:"C:\Users\domin\Documents\myacccim\myACCCIM App\asos-user\src\pages\speaker-detail4\speaker-detail4.html"*/'<ion-header>\n\n    <ion-navbar>\n\n      <ion-buttons left>\n\n        <button ion-button menuToggle>\n\n          <ion-icon name="menu"></ion-icon>\n\n        </button>\n\n      </ion-buttons>\n\n  \n\n      <ion-title>Speakers</ion-title>\n\n    </ion-navbar>\n\n  </ion-header>\n\n  \n\n  <ion-content padding>\n\n    <div style="text-align:center">\n\n        <img src="../../assets/imgs/StefanieAwWanZhen.jpg">\n\n    </div>\n\n    \n\n    <ion-row>\n\n        <ion-col col-12>\n\n          <p text-center text-center style="font-size:20px;">胡琬真小姐</p>\n\n          <p text-center text-center style="font-size:20px;">Stefanie Aw Wan Zhen</p>\n\n        </ion-col>\n\n      </ion-row>\n\n    \n\n      <ion-list class="orders-list">\n\n          <ion-item>\n\n            <ion-icon item-start name="md-locate"></ion-icon>\n\n            <p>46 lorong senawang 2/2, <br/> kawasan perusahaan, senawang 70450, Sban.Malaysia</p>\n\n          </ion-item>\n\n      \n\n          <ion-item>\n\n            <ion-icon item-start name="md-call"></ion-icon>\n\n            <p>066792828</p>\n\n          </ion-item>\n\n      \n\n          \n\n              <ion-item>\n\n                <ion-icon item-start name="md-mail"></ion-icon>\n\n                <p>awstefanie@gmail.com</p>\n\n              </ion-item>\n\n          \n\n              <ion-item>\n\n                <ion-icon item-start name="md-navigate"></ion-icon>\n\n                <p>www.actengroup.com.my</p>\n\n              </ion-item>\n\n          \n\n        </ion-list>\n\n\n\n    <ion-segment [(ngModel)]="version">\n\n\n\n        <ion-segment-button value="chinese">\n\n            <p class="item-title" style="color:#ED6D10;">Chinese</p>\n\n          </ion-segment-button>\n\n\n\n      <ion-segment-button value="english">\n\n        <p class="item-title" style="color:#ED6D10;">English</p>\n\n\n\n      </ion-segment-button>\n\n\n\n      </ion-segment>\n\n\n\n      <div [ngSwitch]="version">\n\n          <div *ngSwitchCase="\'chinese\'">\n\n              <ion-list>\n\n                  <ion-item *ngFor="let p of pages2; let i=index" text-wrap (click)="ctoggleLevel1(\'idx\'+i)" [ngClass]="{active: cisLevel1Shown(\'idx\'+i)}">\n\n                    <h4>\n\n                      {{p.cfirstlayer}}\n\n                      <ion-icon color="success" item-right [name]="cisLevel1Shown(\'idx\'+i) ? \'arrow-dropdown\' : \'arrow-dropright\'"></ion-icon>\n\n                    </h4>\n\n                    <ion-list *ngIf="cisLevel1Shown(\'idx\'+i)">\n\n                      <ion-item *ngFor="let s of p.csubs; let i2=index" text-wrap (click)="ctoggleLevel2(\'idx\'+i+\'idx\'+i2)" [ngClass]="{active: cisLevel2Shown(\'idx\'+i+\'idx\'+i2)}">\n\n                       \n\n                        <h4 style="line-height: 2.0em;">\n\n                          {{s.csecondlayer}}\n\n                          <ion-icon color="success" item-right [name]="cisLevel2Shown(\'idx\'+i+\'idx\'+i2) ? \'arrow-dropdown\' : \'arrow-dropright\'"></ion-icon>\n\n                        </h4>\n\n                        <ion-list *ngIf="cisLevel2Shown(\'idx\'+i+\'idx\'+i2)">\n\n                          <ion-item *ngFor="let m of s.csecondsub" text-wrap>\n\n                           <p style="line-height: 2.0em; font-size: 20px;" *ngIf="m.cdescription5">{{m.cdescription5}}</p>\n\n                          <p style="line-height: 2.0em; font-size: 15px; font-weight:bold;" *ngIf="m.cname">{{m.cname}}</p>\n\n                          <p style="line-height: 2.0em;" *ngIf="m.cdescription1">- {{m.cdescription1}}</p>\n\n                          <p style="line-height: 2.0em;" *ngIf="m.cdescription2">- {{m.cdescription2}}</p>\n\n                          <p style="line-height: 2.0em;" *ngIf="m.cdescription3">- {{m.cdescription3}}</p>\n\n                          <p style="line-height: 2.0em;" *ngIf="m.cdescription4">- {{m.cdescription4}}</p>\n\n                          </ion-item>\n\n                        </ion-list>\n\n                      </ion-item>\n\n                    </ion-list>\n\n                  </ion-item>\n\n                </ion-list>\n\n          </div>\n\n      </div>\n\n\n\n      <div [ngSwitch]="version">\n\n          <div *ngSwitchCase="\'english\'">\n\n              <ion-list>\n\n                  <ion-item *ngFor="let p of pages; let i=index" text-wrap (click)="toggleLevel1(\'idx\'+i)" [ngClass]="{active: isLevel1Shown(\'idx\'+i)}">\n\n                    <h4>\n\n                      {{p.efirstlayer}}\n\n                      <ion-icon color="success" item-right [name]="isLevel1Shown(\'idx\'+i) ? \'arrow-dropdown\' : \'arrow-dropright\'"></ion-icon>\n\n                    </h4>\n\n                    <ion-list *ngIf="isLevel1Shown(\'idx\'+i)">\n\n                      <ion-item *ngFor="let s of p.esubs; let i2=index" text-wrap (click)="toggleLevel2(\'idx\'+i+\'idx\'+i2)" [ngClass]="{active: isLevel2Shown(\'idx\'+i+\'idx\'+i2)}">\n\n                       \n\n                        <h4 style="line-height: 2.0em;">\n\n                          {{s.esecondlayer}}\n\n                          <ion-icon color="success" item-right [name]="isLevel2Shown(\'idx\'+i+\'idx\'+i2) ? \'arrow-dropdown\' : \'arrow-dropright\'"></ion-icon>\n\n                        </h4>\n\n                        <ion-list *ngIf="isLevel2Shown(\'idx\'+i+\'idx\'+i2)">\n\n                          <ion-item *ngFor="let m of s.esecondsub" text-wrap>\n\n                          <p style="line-height: 2.0em; font-size: 20px;" *ngIf="m.edescription5">{{m.edescription5}}</p>\n\n                          <p style="line-height: 2.0em; font-size: 15px; font-weight:bold;" *ngIf="m.ename">{{m.ename}}</p>\n\n                          <p style="line-height: 2.0em;" *ngIf="m.edescription1">- {{m.edescription1}}</p>\n\n                          <p style="line-height: 2.0em;" *ngIf="m.edescription2">- {{m.edescription2}}</p>\n\n                          <p style="line-height: 2.0em;" *ngIf="m.edescription3">- {{m.edescription3}}</p>\n\n                          <p style="line-height: 2.0em;" *ngIf="m.edescription4">- {{m.edescription4}}</p>\n\n                          \n\n                          </ion-item>\n\n                        </ion-list>\n\n                      </ion-item>\n\n                    </ion-list>\n\n                  </ion-item>\n\n                </ion-list>\n\n          </div>\n\n      </div>\n\n  \n\n   \n\n  \n\n    <!-- <ion-list class="orders-list">\n\n      <ion-item>\n\n        <ion-icon item-start name="ios-basket-outline"></ion-icon>\n\n        <p>My orders</p>\n\n      </ion-item>\n\n  \n\n      <ion-item>\n\n        <ion-icon item-start name="ios-archive-outline"></ion-icon>\n\n        <p>My returns</p>\n\n      </ion-item>\n\n  \n\n      <ion-item>\n\n        <ion-icon item-start name="md-bicycle"></ion-icon>\n\n        <p>Premier Delivery</p>\n\n      </ion-item>\n\n    </ion-list>\n\n  \n\n    <ion-list>\n\n      <ion-item>\n\n        <ion-icon item-start name="ios-bookmarks-outline"></ion-icon>\n\n        <p>My details</p>\n\n      </ion-item>\n\n  \n\n      <ion-item>\n\n        <ion-icon item-start name="ios-home-outline"></ion-icon>\n\n        <p>Address Book</p>\n\n      </ion-item>\n\n  \n\n      <ion-item>\n\n        <ion-icon item-start name="ios-card"></ion-icon>\n\n        <p>Payment methods</p>\n\n      </ion-item>\n\n  \n\n      <ion-item>\n\n        <ion-icon item-start name="ios-text-outline"></ion-icon>\n\n        <p>Contact Preferences</p>\n\n      </ion-item>\n\n  \n\n      <ion-item>\n\n        <ion-icon item-start name="ios-contacts-outline"></ion-icon>\n\n        <p>Social accounts</p>\n\n      </ion-item>\n\n    </ion-list>\n\n  \n\n    <ion-list>\n\n      <ion-item>\n\n        <ion-icon item-start name="ios-game-controller-b-outline"></ion-icon>\n\n        <p>Gift card & vouchers</p>\n\n      </ion-item>\n\n    </ion-list>\n\n  \n\n    <ion-list>\n\n      <ion-item>\n\n        <ion-icon item-start name="ios-help-circle-outline"></ion-icon>\n\n        <p>Need help?</p>\n\n      </ion-item>\n\n  \n\n      <ion-item>\n\n        <ion-icon item-start name="ios-browsers-outline"></ion-icon>\n\n        <p>Where\'s my order?</p>\n\n      </ion-item>\n\n  \n\n      <ion-item>\n\n        <ion-icon item-start name="ios-browsers-outline"></ion-icon>\n\n        <p>How do I make a return?</p>\n\n      </ion-item>\n\n    </ion-list>\n\n  \n\n    <ion-item>\n\n      <ion-icon item-start name="md-log-out"></ion-icon>\n\n      <p>Sign out</p>\n\n    </ion-item> -->\n\n  </ion-content>'/*ion-inline-end:"C:\Users\domin\Documents\myacccim\myACCCIM App\asos-user\src\pages\speaker-detail4\speaker-detail4.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["a" /* HttpClient */]])
    ], SpeakerDetail4Page);
    return SpeakerDetail4Page;
}());

//# sourceMappingURL=speaker-detail4.js.map

/***/ }),

/***/ 167:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SpeakerDetail6Page; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the SpeakerDetail6Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var SpeakerDetail6Page = /** @class */ (function () {
    function SpeakerDetail6Page(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.version = "chinese";
    }
    SpeakerDetail6Page.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SpeakerDetail6Page');
    };
    SpeakerDetail6Page = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-speaker-detail6',template:/*ion-inline-start:"C:\Users\domin\Documents\myacccim\myACCCIM App\asos-user\src\pages\speaker-detail6\speaker-detail6.html"*/'<ion-header>\n\n    <ion-navbar>\n\n      <ion-buttons left>\n\n        <button ion-button menuToggle>\n\n          <ion-icon name="menu"></ion-icon>\n\n        </button>\n\n      </ion-buttons>\n\n  \n\n      <ion-title>Speakers</ion-title>\n\n    </ion-navbar>\n\n  </ion-header>\n\n  \n\n  <ion-content padding>\n\n    <div style="text-align:center">\n\n        <img src="../../assets/imgs/林国兴.jpg">\n\n    </div>\n\n    \n\n\n\n    <ion-row>\n\n        <ion-col col-12>\n\n          <p text-center style="font-size:20px;">林国兴先生</p>\n\n          <p text-center style="font-size:20px;">Mr Lim Kok Hing</p>\n\n        </ion-col>\n\n      </ion-row>\n\n    \n\n      <ion-row>\n\n          <ion-col col-12>\n\n            <p text-center>iPay88 创办人</p>\n\n          </ion-col>\n\n        </ion-row>\n\n    \n\n        <ion-row>\n\n            <ion-col col-12>\n\n              <p text-center>CO-FOUNDER OF iPay88</p>\n\n            </ion-col>\n\n          </ion-row>\n\n\n\n\n\n    <ion-segment [(ngModel)]="version">\n\n\n\n        <ion-segment-button value="chinese">\n\n            <p class="item-title" style="color:#ED6D10;">Chinese</p>\n\n          </ion-segment-button>\n\n  \n\n      <ion-segment-button value="english">\n\n        <p class="item-title" style="color:#ED6D10;">English</p>\n\n  \n\n      </ion-segment-button>\n\n  \n\n    \n\n  \n\n    </ion-segment>\n\n\n\n    <div [ngSwitch]="version">\n\n        <div *ngSwitchCase="\'chinese\'">\n\n\n\n            <ion-row>\n\n                <ion-col col-12>\n\n                  <p style="line-height: 2.0em;">他是一名工程师，于1989年毕业于马来亚大学工程系。在大学时，他在校园很活跃于各种活动筹办工作。毕业后，由于对商业及管理拥有浓厚的兴趣，在业余进修会计与金融科，并获得文凭。这对于他后来的创业，公司运营和策略制定，都有很大的帮助。 他曾经于跨国公司埃克森美孚Exxon Mobil就业，得以学习国际企业的管理方式。 在累积了几年的工作经验后，他决定创业！</p>\n\n                </ion-col>\n\n                <ion-col col-12>\n\n                  <p style="line-height: 2.0em;">1997年8月31日他第一天创业，也是国家庆祝40年独立。当时是亚洲金融风暴达到最坏的时刻，他毅然不惧风暴，迈步去开创事业。当时公司业务是成为移动电讯的商业伙伴，达到快速增长所带来的商机。</p>\n\n                </ion-col>\n\n                <ion-col col-12>\n\n                    <p style="line-height: 2.0em;">2006年，他和两位长久伙伴创立了iPay88。 他拥有20年创业和管理的经验。他的企业策划加上员工的配合，在高度竞争的环境中前进，带领着 iPay88 成为一个强大的公司。对与任何人，保持诚信乃是公司一贯的经营理念。</p>\n\n                </ion-col>\n\n                <ion-col col-12>\n\n                  <p>2016年，他被提名为 安永EY最佳企业家奖项的候选人之一。</p>\n\n                </ion-col>\n\n              </ion-row>\n\n\n\n              <ion-row>\n\n                <ion-col col-12>\n\n                  <p>关于 iPay88</p>\n\n\n\n                </ion-col>\n\n                <ion-col col-12>\n\n                  <p style="line-height: 2.0em;">iPay88 是东南亚领先的支付公司。自2006年以来，iPay88 已为多国超过2万个商家提供线上和线下支付服务。 iPay88 总部位于吉隆坡，在印尼，菲律宾，柬埔寨，泰国和新加坡设有办事处，目前在其办事处聘用了超过200 名专业人士。</p>\n\n                </ion-col>\n\n                <ion-col col-12>\n\n                  <p style="line-height: 2.0em;">iPay88 近年来快速增长，目前正计划把业务发展到其他发展中国家。 iPay88 也是马来西亚最大的线上支付公司。所服务客戶涵盖大型企业集团、公营事业、中小企业和SOHO族，例如云顶、星报、AIA, 森那美 ，DiGi，Old Town白咖啡，PappaRich, 等。 最近也加了多加电子钱包例如微信支付，支付宝，银联，Boost, Touch n Go, GrabPay 等等， 已达到给予商家非常便利的收款方式。iPay88 每月处理数以百万笔交易，年交易额超过马币 50 亿以上。</p>\n\n                </ion-col>\n\n              </ion-row>\n\n          </div>\n\n          </div>\n\n\n\n\n\n          <div [ngSwitch]="version">\n\n              <div *ngSwitchCase="\'english\'">\n\n      \n\n                  <ion-row>\n\n                      <ion-col col-12>\n\n                        <p style="line-height: 2.0em;">An engineer by training, he graduated with an engineering degree from University of Malaya in 1989. During his university days, he was active in various activities. Upon his graduation, his keen interest in business and management prompted him to take up a course and obtained a diploma in accounting and finance. The credential proves much useful when he later decided to start his own business and in managing and charting strategies for his company. He worked with multinational Exxon Mobil, gaining exposure to international business management systems and culture. Armed with several years of working experience, he later decided to embark on a business career！</p>\n\n                      </ion-col>\n\n                      <ion-col col-12>\n\n                        <p style="line-height: 2.0em;">He set up his business on 31 August 1997, the day when Malaysians celebrated the National Day, and at the height of the financial crisis sweeping through Asia . Undaunted by the crisis, he ventured into the business arena, becoming a partner of the mobile telecommunication industry. The company grew rapidly by taking advantage of the opportunities arising from the industry.</p>\n\n                      </ion-col>\n\n                      <ion-col col-12>\n\n                          <p style="line-height: 2.0em;">In 2006, he co-founded iPay88 with two long-time business partners. With over two decades of business and management expertise and experience coupled with his corporate planning and staff teamwork , he had led the company to grow from strength to strength in spite of keen competition, subscribing to the business philosophy of integrity to all.</p>\n\n                      </ion-col>\n\n                      <ion-col col-12>\n\n                        <p style="line-height: 2.0em;">In 2016, he was named one of the nominees for the EY Entrepreneur of the Year Award by Ernst and Young.\n\n                          </p>\n\n                      </ion-col>\n\n                    </ion-row>\n\n      \n\n                    <ion-row>\n\n                      <ion-col col-12>\n\n                        <p>About iPay88</p>\n\n      \n\n                      </ion-col>\n\n                      <ion-col col-12>\n\n                        <p style="line-height: 2.0em;">iPay88 is the leading payment company in Southeast Asia, providing online and offline payment services to over 20,000 merchants in many countries since its inception in 2006. Based in Kuala Lumpur, iPay88 has set up offices in Indonesia, Philippines, Cambodia, Thailand and Singapore. Its headquarters employ over 200 professionals.</p>\n\n                      </ion-col>\n\n                      <ion-col col-12>\n\n                        <p style="line-height: 2.0em;">Buoyed by its rapid growth in recent years, the Company is planning to expand to other developing countries. Being the biggest online payments service provider in Malaysia, iPay88 services cover conglomerates, utility companies, SMEs and the SOHO community. It clientele includes Gentings Group, the Star, AIA, Sime Darby ，DiGi，Old Town White Coffee， PappaRich and many more. Recently, it has added e-wallets such as WeChat Pay, Alipay, Union Pay, Boost, Touch n Go, and GrabPay to its list, providing convenient payment services to merchants. iPay88 processes millions of transactions worth over RM5.0 billion every month.</p>\n\n                      </ion-col>\n\n                    </ion-row>\n\n                </div>\n\n                </div>\n\n\n\n  \n\n  \n\n    <!-- <ion-list class="orders-list">\n\n      <ion-item>\n\n        <ion-icon item-start name="ios-basket-outline"></ion-icon>\n\n        <p>My orders</p>\n\n      </ion-item>\n\n  \n\n      <ion-item>\n\n        <ion-icon item-start name="ios-archive-outline"></ion-icon>\n\n        <p>My returns</p>\n\n      </ion-item>\n\n  \n\n      <ion-item>\n\n        <ion-icon item-start name="md-bicycle"></ion-icon>\n\n        <p>Premier Delivery</p>\n\n      </ion-item>\n\n    </ion-list>\n\n  \n\n    <ion-list>\n\n      <ion-item>\n\n        <ion-icon item-start name="ios-bookmarks-outline"></ion-icon>\n\n        <p>My details</p>\n\n      </ion-item>\n\n  \n\n      <ion-item>\n\n        <ion-icon item-start name="ios-home-outline"></ion-icon>\n\n        <p>Address Book</p>\n\n      </ion-item>\n\n  \n\n      <ion-item>\n\n        <ion-icon item-start name="ios-card"></ion-icon>\n\n        <p>Payment methods</p>\n\n      </ion-item>\n\n  \n\n      <ion-item>\n\n        <ion-icon item-start name="ios-text-outline"></ion-icon>\n\n        <p>Contact Preferences</p>\n\n      </ion-item>\n\n  \n\n      <ion-item>\n\n        <ion-icon item-start name="ios-contacts-outline"></ion-icon>\n\n        <p>Social accounts</p>\n\n      </ion-item>\n\n    </ion-list>\n\n  \n\n    <ion-list>\n\n      <ion-item>\n\n        <ion-icon item-start name="ios-game-controller-b-outline"></ion-icon>\n\n        <p>Gift card & vouchers</p>\n\n      </ion-item>\n\n    </ion-list>\n\n  \n\n    <ion-list>\n\n      <ion-item>\n\n        <ion-icon item-start name="ios-help-circle-outline"></ion-icon>\n\n        <p>Need help?</p>\n\n      </ion-item>\n\n  \n\n      <ion-item>\n\n        <ion-icon item-start name="ios-browsers-outline"></ion-icon>\n\n        <p>Where\'s my order?</p>\n\n      </ion-item>\n\n  \n\n      <ion-item>\n\n        <ion-icon item-start name="ios-browsers-outline"></ion-icon>\n\n        <p>How do I make a return?</p>\n\n      </ion-item>\n\n    </ion-list>\n\n  \n\n    <ion-item>\n\n      <ion-icon item-start name="md-log-out"></ion-icon>\n\n      <p>Sign out</p>\n\n    </ion-item> -->\n\n  </ion-content>'/*ion-inline-end:"C:\Users\domin\Documents\myacccim\myACCCIM App\asos-user\src\pages\speaker-detail6\speaker-detail6.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */]])
    ], SpeakerDetail6Page);
    return SpeakerDetail6Page;
}());

//# sourceMappingURL=speaker-detail6.js.map

/***/ }),

/***/ 168:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SpeakerDetail10Page; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the SpeakerDetail10Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var SpeakerDetail10Page = /** @class */ (function () {
    function SpeakerDetail10Page(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.version = "chinese";
    }
    SpeakerDetail10Page.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SpeakerDetail10Page');
    };
    SpeakerDetail10Page = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-speaker-detail10',template:/*ion-inline-start:"C:\Users\domin\Documents\myacccim\myACCCIM App\asos-user\src\pages\speaker-detail10\speaker-detail10.html"*/'<ion-header>\n\n    <ion-navbar>\n\n      <ion-buttons left>\n\n        <button ion-button menuToggle>\n\n          <ion-icon name="menu"></ion-icon>\n\n        </button>\n\n      </ion-buttons>\n\n  \n\n      <ion-title>Speakers</ion-title>\n\n    </ion-navbar>\n\n  </ion-header>\n\n  \n\n  <ion-content padding>\n\n\n\n    <div style="text-align:center">\n\n        <img src="../../assets/imgs/Mr.MichaelChaiWoonChew.png">\n\n  \n\n    </div>\n\n    \n\n    <ion-row>\n\n        <ion-col col-12>\n\n          <p text-center style="font-size:20px;">蔡文洲律师</p>\n\n          <p text-center style="font-size:20px;">MICHAEL Chai Woon Chew</p>\n\n        </ion-col>\n\n      </ion-row>\n\n    \n\n      <ion-row>\n\n          <ion-col col-12>\n\n            <p text-center>Michael Chai & Co律师事务所 执行合伙人 Managing Partner of Michael Chai & Co.</p>\n\n          </ion-col>\n\n        </ion-row>\n\n  \n\n    <ion-list class="orders-list">\n\n      <ion-item>\n\n        <ion-icon item-start name="md-locate"></ion-icon>\n\n        <p>Level 5 Wisma Hong Leong <br/>\n\n\n\n            18 Jalan Perak,50450 Kuala Lumpur,Malaysia</p>\n\n      </ion-item>\n\n  \n\n      <ion-item>\n\n        <ion-icon item-start name="md-call"></ion-icon>\n\n        <p>+ 603-2166 8662</p>\n\n      </ion-item>\n\n  \n\n      <ion-item>\n\n        <ion-icon item-start name="md-print"></ion-icon>\n\n        <p>+ 603-2166 8661</p>\n\n      </ion-item>\n\n    </ion-list>\n\n  \n\n    <ion-list>\n\n      <ion-item>\n\n        <ion-icon item-start name="md-mail"></ion-icon>\n\n        <p>mchai@mchai.com.my</p>\n\n      </ion-item>\n\n  \n\n      <ion-item>\n\n        <ion-icon item-start name="md-navigate"></ion-icon>\n\n        <p>www.mchai.com.my</p>\n\n      </ion-item>\n\n  </ion-list>\n\n     \n\n     \n\n\n\n    <ion-segment [(ngModel)]="version">\n\n\n\n        <ion-segment-button value="chinese">\n\n            <p class="item-title" style="color:#ED6D10;">Chinese</p>\n\n          </ion-segment-button>\n\n  \n\n      <ion-segment-button value="english">\n\n        <p class="item-title" style="color:#ED6D10;">English</p>\n\n  \n\n      </ion-segment-button>\n\n  \n\n    \n\n  \n\n    </ion-segment>\n\n\n\n    <div [ngSwitch]="version">\n\n        <div *ngSwitchCase="\'chinese\'">\n\n\n\n          <ion-row>\n\n            <ion-col col-12>\n\n              <p style="line-height: 2.0em;">蔡文洲律师是Michael Chai & Co律师事务所 (前称Michael Chai Ken)的执行合伙人， 主要执业领域为知识产权法，提供有关专利、工业设计、商标、版权、商业机密、技术转移、许可权及网络法等方面的意见。 他留学英国，持有伯明翰大学法律学位和萨里大学的化学学位。 他是英格兰林肯法学院的合格律师并获得马来西亚律师资格，于1991 年开始执业，并于1995年成为新加坡最高法院律师。 他是一位马来西亚的注册专利、商标及工业设计代理。蔡先生在知识产权上的专长涵盖控告、提供法律意见、许可权、咨询及诉讼等领域。他也是马来西亚海事学院董事。 蔡先生是多个国际专业组织的成员，并积极参与活动，包括国际保护知识产权协会 （马来西亚属会)、马来西亚许可贸易工作者协会、马来西亚知识产权协会、亚洲专利代理人协会、东盟知识产权协会、国际商标协会、马来西亚海事法协会、国际许可贸易工作者协会、马来西亚国际商会、 日本商标协会、马来西亚品牌协会、泛太平洋伙伴关系协定及马来西亚中华总商会（中总）企业顾问。他也是中国全国侨联的马来西亚法律顾问及广州海外交流协会理事</p>\n\n            </ion-col>\n\n          </ion-row>\n\n          </div>\n\n          </div>\n\n\n\n          <div [ngSwitch]="version">\n\n              <div *ngSwitchCase="\'english\'">\n\n\n\n                <ion-row>\n\n                  <ion-col col-12>\n\n                    <p style="line-height: 2.0em;">Michael Chai is the managing partner of Michael Chai & Co. (formerly known as Michael Chai Ken). His main practice area is intellectual property law in which he advises on patents, industrial designs, trademarks, copyright, trade secrets, technology transfer, licensing and cyber law. He studied in UK where he holds a law degree from the University of Bukingham and degree in chemistry from the University of Surrey. He is qualified as a Barrister-at-law from Lincoln\'s Inn, England and called to the Bar in Malaysia before starting practice in 1991. He was admitted as an Advocate and Solicitor 1995 of the Supreme Court of Singapore. He is a registered patent, trade mark and industrial design agent in Malaysia. Michael\'s intellectual property expertise covers prosecution, opinions, licensing, counselling and litigation.</p>\n\n                  </ion-col>\n\n\n\n                  <ion-col col-12>\n\n                    <p style="line-height: 2.0em;">He is also holding a directorship in Maritime Institute of Malaysia (MIMA). Michael Chai is a member and active in several international professional organisations including, Association Internationale pour la Protection de la Propriété Industrielle (AIPPI) (Malaysian Chapter), Licensing Executives Society Malaysia (LESM), Malaysian Intellectual Property Association (MIPA), Asian Patent Attorneys Association (APAA), Asean Intellectual Property Association (AIPA), International Trade Mark Association (INTA), Malaysian Maritime Law Association (MMLA), Licensing Executives Society International (LES International), International Chambers of Commerce Malaysia (ICC Malaysia), The Japan Trademark Association (JTA), The Branding Association of Malaysia (BAM), Corporate Advisor to Trans-Pacific Partnership Agreement (TPPA) and The Associated Chinese Chambers of Commerce and Industry of Malaysia (ACCCIM). He is also a legal advisor for Malaysia of the All-China Federation of Returned Overseas Legal Advisory Committee and member of Guangzhou Overseas Exchange Association Council.</p>\n\n                  </ion-col>\n\n                </ion-row>\n\n      </div>\n\n      </div>\n\n  \n\n   \n\n  </ion-content>'/*ion-inline-end:"C:\Users\domin\Documents\myacccim\myACCCIM App\asos-user\src\pages\speaker-detail10\speaker-detail10.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */]])
    ], SpeakerDetail10Page);
    return SpeakerDetail10Page;
}());

//# sourceMappingURL=speaker-detail10.js.map

/***/ }),

/***/ 169:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SpeakerDetail9Page; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the SpeakerDetail9Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var SpeakerDetail9Page = /** @class */ (function () {
    function SpeakerDetail9Page(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.version = "chinese";
    }
    SpeakerDetail9Page.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SpeakerDetail9Page');
    };
    SpeakerDetail9Page = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-speaker-detail9',template:/*ion-inline-start:"C:\Users\domin\Documents\myacccim\myACCCIM App\asos-user\src\pages\speaker-detail9\speaker-detail9.html"*/'<ion-header>\n\n    <ion-navbar>\n\n      <ion-buttons left>\n\n        <button ion-button menuToggle>\n\n          <ion-icon name="menu"></ion-icon>\n\n        </button>\n\n      </ion-buttons>\n\n  \n\n      <ion-title>Speakers</ion-title>\n\n    </ion-navbar>\n\n  </ion-header>\n\n  \n\n  <ion-content padding>\n\n\n\n    <div style="text-align:center">\n\n        <img src="../../assets/imgs/Mr.ChrisLeong.jpg">\n\n    </div>\n\n    \n\n\n\n    <ion-row>\n\n        <ion-col col-12>\n\n          <p text-center style="font-size:20px;">梁耀聪先生</p>\n\n          <p text-center style="font-size:20px;">Chris Leong</p>\n\n        </ion-col>\n\n      </ion-row>\n\n    \n\n      <ion-row>\n\n          <ion-col col-12>\n\n            <p text-center>策略总监 Chief Strategy Officer (CSO)</p>\n\n          </ion-col>\n\n        </ion-row>\n\n  \n\n        <ion-row>\n\n            <ion-col col-12>\n\n              <p text-center>工商管理硕士（思克莱德大学）、伊斯兰认证、电子及电脑理科学士\n\n                  MBA (University of Strathclyde), Certified Islamic, Bsc Electronic and Computing</p>\n\n            </ion-col>\n\n          </ion-row>\n\n    \n\n    <ion-segment [(ngModel)]="version">\n\n\n\n        <ion-segment-button value="chinese">\n\n            <p class="item-title" style="color:#ED6D10;">Chinese</p>\n\n          </ion-segment-button>\n\n  \n\n      <ion-segment-button value="english">\n\n        <p class="item-title" style="color:#ED6D10;">English</p>\n\n  \n\n      </ion-segment-button>\n\n  \n\n    \n\n  \n\n    </ion-segment>\n\n\n\n    <div [ngSwitch]="version">\n\n        <div *ngSwitchCase="\'chinese\'">\n\n            <ion-row>\n\n                <ion-col col-12>\n\n                  <p style="line-height: 2.0em;">梁耀聪先生是Soft Space 现任策略总监，负责管理公司区域伙伴的利益相关者关系和提供流动支付市场的策略监督， 确保符合公司的增长目标。 他在开发可扩展企业解决方案方面有超过13 年的经验。他的职业背景包括提供意见给初创企业，加速它们的发展。 他是促使企业者与天使投资者搭配的天使投资组织---Virtuous Investment Circle (VIC)的创办人之一和著作 “Like Me, Follow Me—Twitter and Facebook For Business “ 的作者。</p>\n\n                </ion-col>\n\n              </ion-row>\n\n          </div>\n\n          </div>\n\n\n\n\n\n          <div [ngSwitch]="version">\n\n              <div *ngSwitchCase="\'english\'">\n\n                  <ion-row>\n\n                      <ion-col col-12>\n\n                        <p style="line-height: 2.0em;">Chris Leong is the current Chief Strategy Officer at Soft Space. Chris manages stakeholder relations of our regional partners and provides strategic oversight of the mobile payments market to ensure alignment to the company’s growth targets. Chris has over 13 years of experience in developing scalable enterprise solutions. His career background includes providing advise to startups as part of an acceleration programme. He is a founding member of Virtuous Investment Circle (VIC), an angel investment group that match make entrepreneurs with angel investors and author of the book “Like Me, Follow Me – Twitter and Facebook for Business”.</p>\n\n                      </ion-col>\n\n                    </ion-row>\n\n                </div>\n\n                </div>\n\n   \n\n  \n\n    <!-- <ion-list class="orders-list">\n\n      <ion-item>\n\n        <ion-icon item-start name="ios-basket-outline"></ion-icon>\n\n        <p>My orders</p>\n\n      </ion-item>\n\n  \n\n      <ion-item>\n\n        <ion-icon item-start name="ios-archive-outline"></ion-icon>\n\n        <p>My returns</p>\n\n      </ion-item>\n\n  \n\n      <ion-item>\n\n        <ion-icon item-start name="md-bicycle"></ion-icon>\n\n        <p>Premier Delivery</p>\n\n      </ion-item>\n\n    </ion-list>\n\n  \n\n    <ion-list>\n\n      <ion-item>\n\n        <ion-icon item-start name="ios-bookmarks-outline"></ion-icon>\n\n        <p>My details</p>\n\n      </ion-item>\n\n  \n\n      <ion-item>\n\n        <ion-icon item-start name="ios-home-outline"></ion-icon>\n\n        <p>Address Book</p>\n\n      </ion-item>\n\n  \n\n      <ion-item>\n\n        <ion-icon item-start name="ios-card"></ion-icon>\n\n        <p>Payment methods</p>\n\n      </ion-item>\n\n  \n\n      <ion-item>\n\n        <ion-icon item-start name="ios-text-outline"></ion-icon>\n\n        <p>Contact Preferences</p>\n\n      </ion-item>\n\n  \n\n      <ion-item>\n\n        <ion-icon item-start name="ios-contacts-outline"></ion-icon>\n\n        <p>Social accounts</p>\n\n      </ion-item>\n\n    </ion-list>\n\n  \n\n    <ion-list>\n\n      <ion-item>\n\n        <ion-icon item-start name="ios-game-controller-b-outline"></ion-icon>\n\n        <p>Gift card & vouchers</p>\n\n      </ion-item>\n\n    </ion-list>\n\n  \n\n    <ion-list>\n\n      <ion-item>\n\n        <ion-icon item-start name="ios-help-circle-outline"></ion-icon>\n\n        <p>Need help?</p>\n\n      </ion-item>\n\n  \n\n      <ion-item>\n\n        <ion-icon item-start name="ios-browsers-outline"></ion-icon>\n\n        <p>Where\'s my order?</p>\n\n      </ion-item>\n\n  \n\n      <ion-item>\n\n        <ion-icon item-start name="ios-browsers-outline"></ion-icon>\n\n        <p>How do I make a return?</p>\n\n      </ion-item>\n\n    </ion-list>\n\n  \n\n    <ion-item>\n\n      <ion-icon item-start name="md-log-out"></ion-icon>\n\n      <p>Sign out</p>\n\n    </ion-item> -->\n\n  </ion-content>'/*ion-inline-end:"C:\Users\domin\Documents\myacccim\myACCCIM App\asos-user\src\pages\speaker-detail9\speaker-detail9.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */]])
    ], SpeakerDetail9Page);
    return SpeakerDetail9Page;
}());

//# sourceMappingURL=speaker-detail9.js.map

/***/ }),

/***/ 170:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SpeakerDetail8Page; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__(40);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the SpeakerDetail8Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var SpeakerDetail8Page = /** @class */ (function () {
    function SpeakerDetail8Page(navCtrl, navParams, http) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.version = "chinese";
        this.showLevel1 = null;
        this.showLevel2 = null;
        this.cshowLevel1 = null;
        this.cshowLevel2 = null;
        this.getCategory();
    }
    SpeakerDetail8Page.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SpeakerDetail8Page');
    };
    SpeakerDetail8Page.prototype.getCategory = function () {
        var _this = this;
        this.http.get('assets/speakerdetail8.json')
            .subscribe(function (res) {
            _this.pages = res['english'];
            _this.pages2 = res['chinese'];
        });
    };
    SpeakerDetail8Page.prototype.toggleLevel1 = function (idx) {
        if (this.isLevel1Shown(idx)) {
            this.showLevel1 = null;
        }
        else {
            this.showLevel1 = idx;
        }
    };
    ;
    SpeakerDetail8Page.prototype.toggleLevel2 = function (idx) {
        if (this.isLevel2Shown(idx)) {
            this.showLevel1 = null;
            this.showLevel2 = null;
        }
        else {
            this.showLevel1 = idx;
            this.showLevel2 = idx;
        }
    };
    ;
    SpeakerDetail8Page.prototype.isLevel1Shown = function (idx) {
        return this.showLevel1 === idx;
    };
    ;
    SpeakerDetail8Page.prototype.isLevel2Shown = function (idx) {
        return this.showLevel2 === idx;
    };
    ;
    /*Chinese*/
    SpeakerDetail8Page.prototype.ctoggleLevel1 = function (idx) {
        if (this.cisLevel1Shown(idx)) {
            this.cshowLevel1 = null;
        }
        else {
            this.cshowLevel1 = idx;
        }
    };
    ;
    SpeakerDetail8Page.prototype.ctoggleLevel2 = function (idx) {
        if (this.cisLevel2Shown(idx)) {
            this.cshowLevel1 = null;
            this.cshowLevel2 = null;
        }
        else {
            this.cshowLevel1 = idx;
            this.cshowLevel2 = idx;
        }
    };
    ;
    SpeakerDetail8Page.prototype.cisLevel1Shown = function (idx) {
        return this.cshowLevel1 === idx;
    };
    ;
    SpeakerDetail8Page.prototype.cisLevel2Shown = function (idx) {
        return this.cshowLevel2 === idx;
    };
    ;
    SpeakerDetail8Page = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-speaker-detail8',template:/*ion-inline-start:"C:\Users\domin\Documents\myacccim\myACCCIM App\asos-user\src\pages\speaker-detail8\speaker-detail8.html"*/'<ion-header>\n\n    <ion-navbar>\n\n      <ion-buttons left>\n\n        <button ion-button menuToggle>\n\n          <ion-icon name="menu"></ion-icon>\n\n        </button>\n\n      </ion-buttons>\n\n  \n\n      <ion-title>Speakers</ion-title>\n\n    </ion-navbar>\n\n  </ion-header>\n\n  \n\n  <ion-content padding>\n\n\n\n<div style="text-align:center;">\n\n    <img src="../../assets/imgs/Mr.IskandarEzzahuddin.jpg">\n\n  \n\n</div>\n\n    \n\n\n\n<ion-row>\n\n    <ion-col col-12>\n\n      <p text-center>Mr. Iskandar Ezzahuddin</p>\n\n    </ion-col>\n\n  </ion-row>\n\n\n\n  <ion-row>\n\n      <ion-col col-12>\n\n        <p text-center>执行长 Chief Executive Officer, Setel</p>\n\n      </ion-col>\n\n    </ion-row>\n\n\n\n   \n\n\n\n<ion-segment [(ngModel)]="version">\n\n\n\n    <ion-segment-button value="chinese">\n\n        <p class="item-title" style="color:#ED6D10;">Chinese</p>\n\n      </ion-segment-button>\n\n\n\n  <ion-segment-button value="english">\n\n    <p class="item-title" style="color:#ED6D10;">English</p>\n\n\n\n  </ion-segment-button>\n\n\n\n\n\n\n\n</ion-segment>\n\n\n\n<div [ngSwitch]="version">\n\n    <div *ngSwitchCase="\'chinese\'">\n\n\n\n      <ion-row>\n\n        <ion-col col-12>\n\n          <p style="line-height: 2.0em;">国家总监、总经理及企业家。 在2 个不同市场开发、推动及迅速助长2 个网上初创企业. 职业生涯初期在跨国公司如 Protecr and Gamble、Nestle等担任多年消费者营销和数字业绩营销职务后， 他转向金融业发展， 在花旗银行和大华银行担任产品管理职务。 对网上初创企业世界的热诚.\n\n              目前担任国油贸易有限公司独资拥有金融技术给 Setel Ventures 私人有限公司执行长 \n\n              </p>\n\n        </ion-col>\n\n      </ion-row>\n\n\n\n      <ion-list>\n\n          <ion-item *ngFor="let p of pages2; let i=index" text-wrap (click)="ctoggleLevel1(\'idx\'+i)" [ngClass]="{active: cisLevel1Shown(\'idx\'+i)}">\n\n            <h4>\n\n              {{p.cfirstlayer}}\n\n              <ion-icon color="success" item-right [name]="cisLevel1Shown(\'idx\'+i) ? \'arrow-dropdown\' : \'arrow-dropright\'"></ion-icon>\n\n            </h4>\n\n            <ion-list *ngIf="cisLevel1Shown(\'idx\'+i)">\n\n              <ion-item *ngFor="let s of p.csubs; let i2=index" text-wrap (click)="ctoggleLevel2(\'idx\'+i+\'idx\'+i2)" [ngClass]="{active: cisLevel2Shown(\'idx\'+i+\'idx\'+i2)}">\n\n               \n\n                <h4 style="line-height: 2.0em;">\n\n                  {{s.csecondlayer}}\n\n                  <ion-icon color="success" item-right [name]="cisLevel2Shown(\'idx\'+i+\'idx\'+i2) ? \'arrow-dropdown\' : \'arrow-dropright\'"></ion-icon>\n\n                </h4>\n\n                <ion-list *ngIf="cisLevel2Shown(\'idx\'+i+\'idx\'+i2)">\n\n                  <ion-item *ngFor="let m of s.csecondsub" text-wrap>\n\n                  <p style="font-size:20px; line-height: 2.0em;" *ngIf="m.cdate">{{m.cdate}}</p>\n\n                   <p style="font-size:15px; line-height: 2.0em; font-weight:bold;" *ngIf="m.cname">{{m.cname}}</p>\n\n                   <p style="line-height: 2.0em;" *ngIf="m.cdescription1">- {{m.cdescription1}}</p>\n\n                   <p style="line-height: 2.0em;" *ngIf="m.cdescription2">- {{m.cdescription2}}</p>\n\n                  </ion-item>\n\n                </ion-list>\n\n              </ion-item>\n\n            </ion-list>\n\n          </ion-item>\n\n        </ion-list>\n\n\n\n     \n\n      </div>\n\n</div>\n\n\n\n<div [ngSwitch]="version">\n\n    <div *ngSwitchCase="\'english\'">\n\n\n\n        <ion-row>\n\n            <ion-col col-12>\n\n              <p style="line-height: 2.0em;">Country Director, General Manager and Entrepreneur. Developed, launched and quickly grew 2 online startups in 2 different verticals. After spending formative years in Consumer Marketing and Digital Performance Marketing in Multinational companies like Procter and Gamble, and Nestle,transitioned to the financial industry with Product Management roles with Citibank and UOB Bank. Passion for the online startup world. Currently holds Chief Executive Officer (CEO) for Setel Ventures Sdn Bhd, a wholly owned Fintech Company of PETRONAS Dagangan Berhad.</p>\n\n            </ion-col>\n\n          </ion-row>\n\n\n\n\n\n          <ion-list>\n\n              <ion-item *ngFor="let p of pages; let i=index" text-wrap (click)="toggleLevel1(\'idx\'+i)" [ngClass]="{active: isLevel1Shown(\'idx\'+i)}">\n\n                <h4>\n\n                  {{p.efirstlayer}}\n\n                  <ion-icon color="success" item-right [name]="isLevel1Shown(\'idx\'+i) ? \'arrow-dropdown\' : \'arrow-dropright\'"></ion-icon>\n\n                </h4>\n\n                <ion-list *ngIf="isLevel1Shown(\'idx\'+i)">\n\n                  <ion-item *ngFor="let s of p.esubs; let i2=index" text-wrap (click)="toggleLevel2(\'idx\'+i+\'idx\'+i2)" [ngClass]="{active: isLevel2Shown(\'idx\'+i+\'idx\'+i2)}">\n\n                   \n\n                    <h4 style="line-height: 2.0em;">\n\n                      {{s.esecondlayer}}\n\n                      <ion-icon color="success" item-right [name]="isLevel2Shown(\'idx\'+i+\'idx\'+i2) ? \'arrow-dropdown\' : \'arrow-dropright\'"></ion-icon>\n\n                    </h4>\n\n                    <ion-list *ngIf="isLevel2Shown(\'idx\'+i+\'idx\'+i2)">\n\n                      <ion-item *ngFor="let m of s.esecondsub" text-wrap>\n\n                          <p style="font-size:20px; line-height: 2.0em;" *ngIf="m.edate">{{m.edate}}</p>\n\n                          <p style="font-size:15px; line-height: 2.0em; font-weight:bold;" *ngIf="m.ename">{{m.ename}}</p>\n\n                          <p style="line-height: 2.0em;" *ngIf="m.edescription1">- {{m.edescription1}}</p>\n\n                          <p style="line-height: 2.0em;" *ngIf="m.edescription2">- {{m.edescription2}}</p>\n\n                      </ion-item>\n\n                    </ion-list>\n\n                  </ion-item>\n\n                </ion-list>\n\n              </ion-item>\n\n            </ion-list>\n\n      </div>\n\n\n\n\n\n      </div>\n\n  \n\n  \n\n    <!-- <ion-list class="orders-list">\n\n      <ion-item>\n\n        <ion-icon item-start name="ios-basket-outline"></ion-icon>\n\n        <p>My orders</p>\n\n      </ion-item>\n\n  \n\n      <ion-item>\n\n        <ion-icon item-start name="ios-archive-outline"></ion-icon>\n\n        <p>My returns</p>\n\n      </ion-item>\n\n  \n\n      <ion-item>\n\n        <ion-icon item-start name="md-bicycle"></ion-icon>\n\n        <p>Premier Delivery</p>\n\n      </ion-item>\n\n    </ion-list>\n\n  \n\n    <ion-list>\n\n      <ion-item>\n\n        <ion-icon item-start name="ios-bookmarks-outline"></ion-icon>\n\n        <p>My details</p>\n\n      </ion-item>\n\n  \n\n      <ion-item>\n\n        <ion-icon item-start name="ios-home-outline"></ion-icon>\n\n        <p>Address Book</p>\n\n      </ion-item>\n\n  \n\n      <ion-item>\n\n        <ion-icon item-start name="ios-card"></ion-icon>\n\n        <p>Payment methods</p>\n\n      </ion-item>\n\n  \n\n      <ion-item>\n\n        <ion-icon item-start name="ios-text-outline"></ion-icon>\n\n        <p>Contact Preferences</p>\n\n      </ion-item>\n\n  \n\n      <ion-item>\n\n        <ion-icon item-start name="ios-contacts-outline"></ion-icon>\n\n        <p>Social accounts</p>\n\n      </ion-item>\n\n    </ion-list>\n\n  \n\n    <ion-list>\n\n      <ion-item>\n\n        <ion-icon item-start name="ios-game-controller-b-outline"></ion-icon>\n\n        <p>Gift card & vouchers</p>\n\n      </ion-item>\n\n    </ion-list>\n\n  \n\n    <ion-list>\n\n      <ion-item>\n\n        <ion-icon item-start name="ios-help-circle-outline"></ion-icon>\n\n        <p>Need help?</p>\n\n      </ion-item>\n\n  \n\n      <ion-item>\n\n        <ion-icon item-start name="ios-browsers-outline"></ion-icon>\n\n        <p>Where\'s my order?</p>\n\n      </ion-item>\n\n  \n\n      <ion-item>\n\n        <ion-icon item-start name="ios-browsers-outline"></ion-icon>\n\n        <p>How do I make a return?</p>\n\n      </ion-item>\n\n    </ion-list>\n\n  \n\n    <ion-item>\n\n      <ion-icon item-start name="md-log-out"></ion-icon>\n\n      <p>Sign out</p>\n\n    </ion-item> -->\n\n  </ion-content>'/*ion-inline-end:"C:\Users\domin\Documents\myacccim\myACCCIM App\asos-user\src\pages\speaker-detail8\speaker-detail8.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["a" /* HttpClient */]])
    ], SpeakerDetail8Page);
    return SpeakerDetail8Page;
}());

//# sourceMappingURL=speaker-detail8.js.map

/***/ }),

/***/ 171:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SpeakerDetail7Page; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the SpeakerDetail7Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var SpeakerDetail7Page = /** @class */ (function () {
    function SpeakerDetail7Page(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    SpeakerDetail7Page.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SpeakerDetail7Page');
    };
    SpeakerDetail7Page = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-speaker-detail7',template:/*ion-inline-start:"C:\Users\domin\Documents\myacccim\myACCCIM App\asos-user\src\pages\speaker-detail7\speaker-detail7.html"*/'<ion-header>\n\n    <ion-navbar>\n\n      <ion-buttons left>\n\n        <button ion-button menuToggle>\n\n          <ion-icon name="menu"></ion-icon>\n\n        </button>\n\n      </ion-buttons>\n\n  \n\n      <ion-title>Speakers</ion-title>\n\n    </ion-navbar>\n\n  </ion-header>\n\n  \n\n  <ion-content padding>\n\n\n\n    <div style="text-align:center">\n\n        <img src="../../assets/imgs/Mr.HansonToh.jpg">\n\n    </div>\n\n\n\n\n\n    <ion-row>\n\n        <ion-col col-12>\n\n          <p text-center style="font-size:20px;">Hanson Toh</p>\n\n        </ion-col>\n\n      </ion-row>\n\n    \n\n      <ion-row>\n\n          <ion-col col-12>\n\n            <p text-center>Director of Strategy & Special Projects at Boost (Axiata Digital eCode, within the Axiata Group)</p>\n\n          </ion-col>\n\n        </ion-row>\n\n    \n\n       \n\n\n\n    <ion-row>\n\n      <ion-col col-12>\n\n        <p style="line-height: 2.0em;">Hanson Toh is currently the Director of Strategy & Special Projects at Boost (Axiata Digital eCode, within the Axiata Group) and has been in leadership roles within Axiata, focusing on incubation and acceleration of Internet/web/tech startup projects within the Axiata Group since he joined in 2015. Prior to Axiata, Hanson was the former Regional Head of Product Management & Marketing Analytics at Jobstreet.com, where he played a leadership role in the subsequent acquisition by SEEK.com. Hanson was the founder of ServiceClicks.com.my which was an online marketplace for consumer-centric services back in 2012 and which was pre-seed funded by Cradle\'s 150k fund. Hanson was Google\'s Country Consultant for 5 years (2006-2011) and was responsible for many aspects of Google\'s SEA and Malaysian strategy and operations in areas such as corporate strategy, product, sales, marketing, corp comm and other functional areas, leading to the setup of Google\'s Singaporean entity in 2007 and Malaysian entity in 2011. In addition to the above, he had spent 8 prior years in management consulting, telco strategy and operations as well as tech sales across South East Asia.</p>\n\n      </ion-col>\n\n\n\n      <ion-col col-12>\n\n        <p style="line-height: 2.0em;">He is a Honeywell Millennium Challenge Award winner, awarded for global excellence in sales and global account-management practices. In his spare time (when there\'s any left!), Hanson works with various startups in a mentor/advisor capacity; he is a 3rd-season Coach & Grow Programme (CGP) Coach since 2014 and as an angel investor, has also invested in one CGP startup as well as other startups in the Malaysian ecosystem. He is also a current and former Mentor to the Young Corporate Malaysian YCM Community. He has two young boys (aged 11 and 7) and enjoys tennis, Spotify, traveling and spending time with the family. Hanson has an MBA from the Said Business School, University of Oxford and a Bachelor\'s in Mech. Engineering from the University of Warwick. He is also a certified Design Thinking trainer from Stanford\'s d.School and Hasso-Plattner Institute (the pioneer batch from Genovasi).</p>\n\n      </ion-col>\n\n    </ion-row>\n\n   \n\n  \n\n    \n\n  \n\n    <!-- <ion-list class="orders-list">\n\n      <ion-item>\n\n        <ion-icon item-start name="ios-basket-outline"></ion-icon>\n\n        <p>My orders</p>\n\n      </ion-item>\n\n  \n\n      <ion-item>\n\n        <ion-icon item-start name="ios-archive-outline"></ion-icon>\n\n        <p>My returns</p>\n\n      </ion-item>\n\n  \n\n      <ion-item>\n\n        <ion-icon item-start name="md-bicycle"></ion-icon>\n\n        <p>Premier Delivery</p>\n\n      </ion-item>\n\n    </ion-list>\n\n  \n\n    <ion-list>\n\n      <ion-item>\n\n        <ion-icon item-start name="ios-bookmarks-outline"></ion-icon>\n\n        <p>My details</p>\n\n      </ion-item>\n\n  \n\n      <ion-item>\n\n        <ion-icon item-start name="ios-home-outline"></ion-icon>\n\n        <p>Address Book</p>\n\n      </ion-item>\n\n  \n\n      <ion-item>\n\n        <ion-icon item-start name="ios-card"></ion-icon>\n\n        <p>Payment methods</p>\n\n      </ion-item>\n\n  \n\n      <ion-item>\n\n        <ion-icon item-start name="ios-text-outline"></ion-icon>\n\n        <p>Contact Preferences</p>\n\n      </ion-item>\n\n  \n\n      <ion-item>\n\n        <ion-icon item-start name="ios-contacts-outline"></ion-icon>\n\n        <p>Social accounts</p>\n\n      </ion-item>\n\n    </ion-list>\n\n  \n\n    <ion-list>\n\n      <ion-item>\n\n        <ion-icon item-start name="ios-game-controller-b-outline"></ion-icon>\n\n        <p>Gift card & vouchers</p>\n\n      </ion-item>\n\n    </ion-list>\n\n  \n\n    <ion-list>\n\n      <ion-item>\n\n        <ion-icon item-start name="ios-help-circle-outline"></ion-icon>\n\n        <p>Need help?</p>\n\n      </ion-item>\n\n  \n\n      <ion-item>\n\n        <ion-icon item-start name="ios-browsers-outline"></ion-icon>\n\n        <p>Where\'s my order?</p>\n\n      </ion-item>\n\n  \n\n      <ion-item>\n\n        <ion-icon item-start name="ios-browsers-outline"></ion-icon>\n\n        <p>How do I make a return?</p>\n\n      </ion-item>\n\n    </ion-list>\n\n  \n\n    <ion-item>\n\n      <ion-icon item-start name="md-log-out"></ion-icon>\n\n      <p>Sign out</p>\n\n    </ion-item> -->\n\n  </ion-content>'/*ion-inline-end:"C:\Users\domin\Documents\myacccim\myACCCIM App\asos-user\src\pages\speaker-detail7\speaker-detail7.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */]])
    ], SpeakerDetail7Page);
    return SpeakerDetail7Page;
}());

//# sourceMappingURL=speaker-detail7.js.map

/***/ }),

/***/ 172:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PopupModalPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_in_app_browser_ngx__ = __webpack_require__(123);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the PopupModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var PopupModalPage = /** @class */ (function () {
    function PopupModalPage(navCtrl, navParams, view, ApiService, iab, modal) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.view = view;
        this.ApiService = ApiService;
        this.iab = iab;
        this.modal = modal;
        this.id = navParams.get('data');
        this.DiamondSponsor();
    }
    PopupModalPage.prototype.DiamondSponsor = function () {
        var _this = this;
        this.ApiService.getSpecificAds(this.id).subscribe(function (res) {
            var getDiamondSponsor = res.json();
            _this.Diamond = getDiamondSponsor;
        });
    };
    PopupModalPage.prototype.OpenLink = function (link) {
        this.iab.create(link, '_blank');
    };
    PopupModalPage.prototype.OpenLink2 = function (link) {
        this.iab.create(link, '_blank');
    };
    PopupModalPage.prototype.OpenLink3 = function (link) {
        this.iab.create(link, '_blank');
    };
    PopupModalPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-popup-modal',template:/*ion-inline-start:"C:\Users\domin\Documents\myacccim\myACCCIM App\asos-user\src\pages\popup-modal\popup-modal.html"*/'<ion-header>\n\n  <ion-navbar>\n\n      <title>Advertisement</title>\n\n  </ion-navbar>\n\n       \n\n  </ion-header>\n\n  \n\n  <ion-content fullscreen class="bg-style">\n\n      <br> \n\n\n\n      <ion-card *ngFor="let item of Diamond">\n\n          \n\n            <img src="https://admin.acccim-registration.org.my{{item.FullListing}}">\n\n            \n\n            <ion-item style="text-align:center;">\n\n              <button ion-button (click)="OpenLink(item.DiamondHomePageAdsWebURL)" (click)="OpenLink2(item.WebPopUpAdsURL)"\n\n              (click)="OpenLink3(item.PGSSHomePageAdsWebURL)">\n\n                Click to view company website\n\n              </button>\n\n            </ion-item>\n\n            \n\n         \n\n          </ion-card>\n\n          \n\n\n\n                \n\n  </ion-content>'/*ion-inline-end:"C:\Users\domin\Documents\myacccim\myACCCIM App\asos-user\src\pages\popup-modal\popup-modal.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_in_app_browser_ngx__["a" /* InAppBrowser */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ModalController */]])
    ], PopupModalPage);
    return PopupModalPage;
}());

//# sourceMappingURL=popup-modal.js.map

/***/ }),

/***/ 173:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AccountPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AccountPage = /** @class */ (function () {
    function AccountPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    AccountPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AccountPage');
    };
    AccountPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-account',template:/*ion-inline-start:"C:\Users\domin\Documents\myacccim\myACCCIM App\asos-user\src\pages\account\account.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <ion-buttons left>\n\n      <button ion-button menuToggle>\n\n        <ion-icon name="menu"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n\n\n    <ion-title>Account</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n  <img src="https://res.cloudinary.com/cediim8/image/upload/v1523427281/cover-photo.jpg">\n\n\n\n  <div text-center class="circle">\n\n    <p class="circle-text">JD</p>\n\n  </div>\n\n\n\n  <ion-list class="orders-list">\n\n    <ion-item>\n\n      <ion-icon item-start name="ios-basket-outline"></ion-icon>\n\n      <p>My orders</p>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <ion-icon item-start name="ios-archive-outline"></ion-icon>\n\n      <p>My returns</p>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <ion-icon item-start name="md-bicycle"></ion-icon>\n\n      <p>Premier Delivery</p>\n\n    </ion-item>\n\n  </ion-list>\n\n\n\n  <ion-list>\n\n    <ion-item>\n\n      <ion-icon item-start name="ios-bookmarks-outline"></ion-icon>\n\n      <p>My details</p>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <ion-icon item-start name="ios-home-outline"></ion-icon>\n\n      <p>Address Book</p>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <ion-icon item-start name="ios-card"></ion-icon>\n\n      <p>Payment methods</p>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <ion-icon item-start name="ios-text-outline"></ion-icon>\n\n      <p>Contact Preferences</p>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <ion-icon item-start name="ios-contacts-outline"></ion-icon>\n\n      <p>Social accounts</p>\n\n    </ion-item>\n\n  </ion-list>\n\n\n\n  <ion-list>\n\n    <ion-item>\n\n      <ion-icon item-start name="ios-game-controller-b-outline"></ion-icon>\n\n      <p>Gift card & vouchers</p>\n\n    </ion-item>\n\n  </ion-list>\n\n\n\n  <ion-list>\n\n    <ion-item>\n\n      <ion-icon item-start name="ios-help-circle-outline"></ion-icon>\n\n      <p>Need help?</p>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <ion-icon item-start name="ios-browsers-outline"></ion-icon>\n\n      <p>Where\'s my order?</p>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <ion-icon item-start name="ios-browsers-outline"></ion-icon>\n\n      <p>How do I make a return?</p>\n\n    </ion-item>\n\n  </ion-list>\n\n\n\n  <ion-item>\n\n    <ion-icon item-start name="md-log-out"></ion-icon>\n\n    <p>Sign out</p>\n\n  </ion-item>\n\n</ion-content>'/*ion-inline-end:"C:\Users\domin\Documents\myacccim\myACCCIM App\asos-user\src\pages\account\account.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */]])
    ], AccountPage);
    return AccountPage;
}());

//# sourceMappingURL=account.js.map

/***/ }),

/***/ 174:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppSettingsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AppSettingsPage = /** @class */ (function () {
    function AppSettingsPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    AppSettingsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AppSettingsPage');
    };
    AppSettingsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-app-settings',template:/*ion-inline-start:"C:\Users\domin\Documents\myacccim\myACCCIM App\asos-user\src\pages\app-settings\app-settings.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <ion-buttons left>\n\n      <button ion-button menuToggle>\n\n        <ion-icon name="menu"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n\n\n    <ion-title>APP SETTINGS</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n  <ion-list>\n\n    <ion-list-header>\n\n      <p>Store setup</p>\n\n    </ion-list-header>\n\n\n\n    <ion-item>\n\n      <p class="item-title">Country</p>\n\n      <p>Australia</p>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <p class="item-title">Change currency</p>\n\n      <p>$ AUD</p>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <p class="item-title">Change language</p>\n\n      <p>Australian English</p>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <p class="item-title">Change sizes</p>\n\n      <p>Australian English</p>\n\n    </ion-item>\n\n  </ion-list>\n\n\n\n  <ion-list>\n\n    <ion-list-header>\n\n      <p>Other</p>\n\n    </ion-list-header>\n\n\n\n    <ion-item>\n\n      <p class="item-title">Clear search history</p>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <p class="item-title">Help & Contact</p>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <p class="item-title">Report an issue</p>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <p class="item-title">Rate the app</p>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <p class="item-title">Notifications</p>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <p class="item-title">Clear search history</p>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <p class="item-title">Vibrate when you add to bag</p>\n\n      <ion-toggle item-end checked="false"></ion-toggle>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <p class="item-title">Terms and conditions</p>\n\n    </ion-item>\n\n  </ion-list>\n\n\n\n  <ion-list>\n\n    <ion-list-header>\n\n      <p>Other</p>\n\n    </ion-list-header>\n\n\n\n    <ion-item>\n\n      <p class="item-title">Build version</p>\n\n      <p>1.0.0 (100)</p>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <p class="item-title">Build time</p>\n\n      <p>2018-01-01 12:00:00</p>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <p class="item-title">Open-source licenses</p>\n\n      <p>License details for used open-source software</p>\n\n    </ion-item>\n\n  </ion-list>\n\n'/*ion-inline-end:"C:\Users\domin\Documents\myacccim\myACCCIM App\asos-user\src\pages\app-settings\app-settings.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */]])
    ], AppSettingsPage);
    return AppSettingsPage;
}());

//# sourceMappingURL=app-settings.js.map

/***/ }),

/***/ 175:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HelpFaqPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var HelpFaqPage = /** @class */ (function () {
    function HelpFaqPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    HelpFaqPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad HelpFaqPage');
    };
    HelpFaqPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-help-faq',template:/*ion-inline-start:"C:\Users\domin\Documents\myacccim\myACCCIM App\asos-user\src\pages\help-faq\help-faq.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <ion-buttons left>\n\n      <button ion-button menuToggle>\n\n        <ion-icon name="menu"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n\n\n    <ion-title>Help & FAQs</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\domin\Documents\myacccim\myACCCIM App\asos-user\src\pages\help-faq\help-faq.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */]])
    ], HelpFaqPage);
    return HelpFaqPage;
}());

//# sourceMappingURL=help-faq.js.map

/***/ }),

/***/ 176:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProductColourPopOverPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ProductColourPopOverPage = /** @class */ (function () {
    function ProductColourPopOverPage(navCtrl, navParams, viewController) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewController = viewController;
    }
    ProductColourPopOverPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ProductColourPopOverPage');
    };
    ProductColourPopOverPage.prototype.closePopover = function () {
        this.viewController.dismiss();
    };
    ProductColourPopOverPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-product-colour-pop-over',template:/*ion-inline-start:"C:\Users\domin\Documents\myacccim\myACCCIM App\asos-user\src\pages\product-colour-pop-over\product-colour-pop-over.html"*/'<ion-list no-margin no-lines>\n\n  <ion-item>\n\n    <button ion-item no-lines (click)="closePopover()">\n\n      BLACK\n\n    </button>\n\n  </ion-item>\n\n\n\n  <ion-item>\n\n    <button ion-item no-lines (click)="closePopover()">\n\n      BLUE\n\n    </button>\n\n  </ion-item>\n\n\n\n  <ion-item>\n\n    <button ion-item no-lines (click)="closePopover()">\n\n      RED\n\n    </button>\n\n  </ion-item>\n\n\n\n  <ion-item>\n\n    <button ion-item no-lines (click)="closePopover()">\n\n      GREEN\n\n    </button>\n\n  </ion-item>\n\n\n\n  <ion-item>\n\n    <button ion-item no-lines (click)="closePopover()">\n\n      YELLOW\n\n    </button>\n\n  </ion-item>\n\n</ion-list>'/*ion-inline-end:"C:\Users\domin\Documents\myacccim\myACCCIM App\asos-user\src\pages\product-colour-pop-over\product-colour-pop-over.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* ViewController */]])
    ], ProductColourPopOverPage);
    return ProductColourPopOverPage;
}());

//# sourceMappingURL=product-colour-pop-over.js.map

/***/ }),

/***/ 177:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProductSizePopOverPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ProductSizePopOverPage = /** @class */ (function () {
    function ProductSizePopOverPage(navCtrl, navParams, viewController) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewController = viewController;
    }
    ProductSizePopOverPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ProductSizePopOverPage');
    };
    ProductSizePopOverPage.prototype.closePopover = function () {
        this.viewController.dismiss();
    };
    ProductSizePopOverPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-product-size-pop-over',template:/*ion-inline-start:"C:\Users\domin\Documents\myacccim\myACCCIM App\asos-user\src\pages\product-size-pop-over\product-size-pop-over.html"*/'<ion-list no-margin no-lines>\n\n  <ion-item>\n\n    <button ion-item no-lines (click)="closePopover()">\n\n      XS\n\n    </button>\n\n  </ion-item>\n\n\n\n  <ion-item>\n\n    <button ion-item no-lines (click)="closePopover()">\n\n      S\n\n    </button>\n\n  </ion-item>\n\n\n\n  <ion-item>\n\n    <button ion-item no-lines (click)="closePopover()">\n\n      M\n\n    </button>\n\n  </ion-item>\n\n\n\n  <ion-item>\n\n    <button class="out-of-stock" ion-item no-lines (click)="closePopover()">\n\n      L - Out of stock\n\n    </button>\n\n  </ion-item>\n\n\n\n  <ion-item>\n\n    <button class="out-of-stock" ion-item no-lines (click)="closePopover()">\n\n      XL - Out of stock\n\n    </button>\n\n  </ion-item>\n\n</ion-list>'/*ion-inline-end:"C:\Users\domin\Documents\myacccim\myACCCIM App\asos-user\src\pages\product-size-pop-over\product-size-pop-over.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* ViewController */]])
    ], ProductSizePopOverPage);
    return ProductSizePopOverPage;
}());

//# sourceMappingURL=product-size-pop-over.js.map

/***/ }),

/***/ 188:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 188;

/***/ }),

/***/ 232:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/about/about.module": [
		704,
		25
	],
	"../pages/account/account.module": [
		705,
		24
	],
	"../pages/app-settings/app-settings.module": [
		706,
		23
	],
	"../pages/category/category.module": [
		729,
		3
	],
	"../pages/change-password/change-password.module": [
		707,
		22
	],
	"../pages/filter-multi-selection/filter-multi-selection.module": [
		708,
		2
	],
	"../pages/filter-range/filter-range.module": [
		709,
		1
	],
	"../pages/filter/filter.module": [
		710,
		21
	],
	"../pages/help-faq/help-faq.module": [
		711,
		20
	],
	"../pages/men-women-category-pop-over/men-women-category-pop-over.module": [
		712,
		19
	],
	"../pages/popup-modal/popup-modal.module": [
		713,
		18
	],
	"../pages/product-colour-pop-over/product-colour-pop-over.module": [
		714,
		17
	],
	"../pages/product-details/product-details.module": [
		715,
		16
	],
	"../pages/product-size-pop-over/product-size-pop-over.module": [
		716,
		15
	],
	"../pages/products/products.module": [
		718,
		0
	],
	"../pages/recommended-pop-over/recommended-pop-over.module": [
		717,
		14
	],
	"../pages/saved-items/saved-items.module": [
		328
	],
	"../pages/speaker-detail1/speaker-detail1.module": [
		719,
		12
	],
	"../pages/speaker-detail10/speaker-detail10.module": [
		720,
		13
	],
	"../pages/speaker-detail2/speaker-detail2.module": [
		721,
		11
	],
	"../pages/speaker-detail3/speaker-detail3.module": [
		722,
		10
	],
	"../pages/speaker-detail4/speaker-detail4.module": [
		723,
		9
	],
	"../pages/speaker-detail5/speaker-detail5.module": [
		724,
		8
	],
	"../pages/speaker-detail6/speaker-detail6.module": [
		725,
		7
	],
	"../pages/speaker-detail7/speaker-detail7.module": [
		726,
		6
	],
	"../pages/speaker-detail8/speaker-detail8.module": [
		727,
		5
	],
	"../pages/speaker-detail9/speaker-detail9.module": [
		728,
		4
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 232;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 328:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SavedItemsPageModule", function() { return SavedItemsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__saved_items__ = __webpack_require__(138);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var SavedItemsPageModule = /** @class */ (function () {
    function SavedItemsPageModule() {
    }
    SavedItemsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__saved_items__["a" /* SavedItemsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__saved_items__["a" /* SavedItemsPage */]),
            ],
        })
    ], SavedItemsPageModule);
    return SavedItemsPageModule;
}());

//# sourceMappingURL=saved-items.module.js.map

/***/ }),

/***/ 370:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FilterPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var FilterPage = /** @class */ (function () {
    function FilterPage(navCtrl, navParams, viewController) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewController = viewController;
    }
    FilterPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad FilterPage');
    };
    FilterPage.prototype.closeFilter = function () {
        this.viewController.dismiss();
    };
    FilterPage.prototype.goToMultiSelection = function (selectionType) {
        this.navCtrl.push('FilterMultiSelectionPage', { selectionType: selectionType });
    };
    FilterPage.prototype.goToRangeSelection = function (selectionType) {
        this.navCtrl.push('FilterRangePage', { selectionType: selectionType });
    };
    FilterPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-filter',template:/*ion-inline-start:"C:\Users\domin\Documents\myacccim\myACCCIM App\asos-user\src\pages\filter\filter.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <ion-title>FILTER</ion-title>\n\n\n\n    <ion-buttons right>\n\n      <button class="navbar-button" ion-button icon-only (click)="closeFilter()">\n\n        <ion-icon name="md-close-circle"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n  <ion-item (click)="goToMultiSelection(\'Product Type\')">\n\n    <p class="filter-type" item-start>Product Type</p>\n\n    <p item-end>All</p>\n\n  </ion-item>\n\n\n\n  <ion-item (click)="goToMultiSelection(\'Range\')">\n\n    <p class="filter-type" item-start>Range</p>\n\n    <p item-end>All</p>\n\n  </ion-item>\n\n\n\n  <ion-item (click)="goToMultiSelection(\'Brand\')">\n\n    <p class="filter-type" item-start>Brand</p>\n\n    <p item-end>All</p>\n\n  </ion-item>\n\n\n\n  <ion-item (click)="goToMultiSelection(\'Multipacks\')">\n\n    <p class="filter-type" item-start>Multipacks</p>\n\n    <p item-end>All</p>\n\n  </ion-item>\n\n\n\n  <ion-item (click)="goToMultiSelection(\'Size\')">\n\n    <p class="filter-type" item-start>Size</p>\n\n    <p item-end>All</p>\n\n  </ion-item>\n\n\n\n  <ion-item (click)="goToRangeSelection(\'Price Range\')">\n\n    <p class="filter-type" item-start>Price Range</p>\n\n    <p item-end>$20 - $500</p>\n\n  </ion-item>\n\n\n\n  <ion-item (click)="goToMultiSelection(\'Colour\')">\n\n    <p class="filter-type" item-start>Colour</p>\n\n    <p item-end>All</p>\n\n  </ion-item>\n\n</ion-content>\n\n\n\n<ion-footer padding>\n\n  <button ion-button full (click)="closeFilter()">\n\n    <p>DONE</p>\n\n  </button>\n\n</ion-footer>'/*ion-inline-end:"C:\Users\domin\Documents\myacccim\myACCCIM App\asos-user\src\pages\filter\filter.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* ViewController */]])
    ], FilterPage);
    return FilterPage;
}());

//# sourceMappingURL=filter.js.map

/***/ }),

/***/ 371:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RecommendedPopOverPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var RecommendedPopOverPage = /** @class */ (function () {
    function RecommendedPopOverPage(navCtrl, navParams, viewController) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewController = viewController;
    }
    RecommendedPopOverPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad RecommendedPopOverPage');
    };
    RecommendedPopOverPage.prototype.selectAndClosePopover = function () {
        this.viewController.dismiss();
    };
    RecommendedPopOverPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-recommended-pop-over',template:/*ion-inline-start:"C:\Users\domin\Documents\myacccim\myACCCIM App\asos-user\src\pages\recommended-pop-over\recommended-pop-over.html"*/'<ion-list no-margin no-lines>\n\n  <ion-item>\n\n    <button ion-item no-lines (click)="selectAndClosePopover()">\n\n      Recommended\n\n    </button>\n\n  </ion-item>\n\n\n\n  <ion-item>\n\n    <button ion-item no-lines (click)="selectAndClosePopover()">\n\n      What\'s New\n\n    </button>\n\n  </ion-item>\n\n\n\n  <ion-item>\n\n    <button ion-item no-lines (click)="selectAndClosePopover()">\n\n      Price - Low to High\n\n    </button>\n\n  </ion-item>\n\n\n\n  <ion-item>\n\n    <button ion-item no-lines (click)="selectAndClosePopover()">\n\n      Price - High to Low\n\n    </button>\n\n  </ion-item>\n\n</ion-list>'/*ion-inline-end:"C:\Users\domin\Documents\myacccim\myACCCIM App\asos-user\src\pages\recommended-pop-over\recommended-pop-over.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* ViewController */]])
    ], RecommendedPopOverPage);
    return RecommendedPopOverPage;
}());

//# sourceMappingURL=recommended-pop-over.js.map

/***/ }),

/***/ 372:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(373);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(377);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 377:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_status_bar__ = __webpack_require__(368);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_splash_screen__ = __webpack_require__(369);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_component__ = __webpack_require__(703);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_home_home__ = __webpack_require__(76);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_men_women_category_pop_over_men_women_category_pop_over__ = __webpack_require__(160);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_about_about__ = __webpack_require__(90);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_saved_items_saved_items__ = __webpack_require__(138);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_account_account__ = __webpack_require__(173);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_app_settings_app_settings__ = __webpack_require__(174);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_help_faq_help_faq__ = __webpack_require__(175);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_saved_items_saved_items_module__ = __webpack_require__(328);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_recommended_pop_over_recommended_pop_over__ = __webpack_require__(371);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_filter_filter__ = __webpack_require__(370);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_product_size_pop_over_product_size_pop_over__ = __webpack_require__(177);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_product_colour_pop_over_product_colour_pop_over__ = __webpack_require__(176);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__providers_api_service_api_service__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__angular_common_http__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__angular_http__ = __webpack_require__(77);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__ionic_native_in_app_browser_ngx__ = __webpack_require__(123);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__pages_product_details_product_details__ = __webpack_require__(161);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__pages_login_login__ = __webpack_require__(137);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__pages_change_password_change_password__ = __webpack_require__(91);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__ionic_storage__ = __webpack_require__(65);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__pages_speaker_detail1_speaker_detail1__ = __webpack_require__(162);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__pages_speaker_detail2_speaker_detail2__ = __webpack_require__(163);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__pages_speaker_detail3_speaker_detail3__ = __webpack_require__(164);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__pages_speaker_detail4_speaker_detail4__ = __webpack_require__(166);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__pages_speaker_detail5_speaker_detail5__ = __webpack_require__(165);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__pages_speaker_detail6_speaker_detail6__ = __webpack_require__(167);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__pages_speaker_detail7_speaker_detail7__ = __webpack_require__(171);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__pages_speaker_detail8_speaker_detail8__ = __webpack_require__(170);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__pages_speaker_detail9_speaker_detail9__ = __webpack_require__(169);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__pages_speaker_detail10_speaker_detail10__ = __webpack_require__(168);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__pages_popup_modal_popup_modal__ = __webpack_require__(172);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





































var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_6__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_men_women_category_pop_over_men_women_category_pop_over__["a" /* MenWomenCategoryPopOverPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_recommended_pop_over_recommended_pop_over__["a" /* RecommendedPopOverPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_product_size_pop_over_product_size_pop_over__["a" /* ProductSizePopOverPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_product_colour_pop_over_product_colour_pop_over__["a" /* ProductColourPopOverPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_account_account__["a" /* AccountPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_app_settings_app_settings__["a" /* AppSettingsPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_help_faq_help_faq__["a" /* HelpFaqPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_filter_filter__["a" /* FilterPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_about_about__["a" /* AboutPage */],
                __WEBPACK_IMPORTED_MODULE_23__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_22__pages_product_details_product_details__["a" /* ProductDetailsPage */],
                __WEBPACK_IMPORTED_MODULE_24__pages_change_password_change_password__["a" /* ChangePasswordPage */],
                __WEBPACK_IMPORTED_MODULE_26__pages_speaker_detail1_speaker_detail1__["a" /* SpeakerDetail1Page */],
                __WEBPACK_IMPORTED_MODULE_27__pages_speaker_detail2_speaker_detail2__["a" /* SpeakerDetail2Page */],
                __WEBPACK_IMPORTED_MODULE_28__pages_speaker_detail3_speaker_detail3__["a" /* SpeakerDetail3Page */],
                __WEBPACK_IMPORTED_MODULE_29__pages_speaker_detail4_speaker_detail4__["a" /* SpeakerDetail4Page */],
                __WEBPACK_IMPORTED_MODULE_30__pages_speaker_detail5_speaker_detail5__["a" /* SpeakerDetail5Page */],
                __WEBPACK_IMPORTED_MODULE_31__pages_speaker_detail6_speaker_detail6__["a" /* SpeakerDetail6Page */],
                __WEBPACK_IMPORTED_MODULE_32__pages_speaker_detail7_speaker_detail7__["a" /* SpeakerDetail7Page */],
                __WEBPACK_IMPORTED_MODULE_33__pages_speaker_detail8_speaker_detail8__["a" /* SpeakerDetail8Page */],
                __WEBPACK_IMPORTED_MODULE_34__pages_speaker_detail9_speaker_detail9__["a" /* SpeakerDetail9Page */],
                __WEBPACK_IMPORTED_MODULE_35__pages_speaker_detail10_speaker_detail10__["a" /* SpeakerDetail10Page */],
                __WEBPACK_IMPORTED_MODULE_36__pages_popup_modal_popup_modal__["a" /* PopupModalPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/about/about.module#AboutPageModule', name: 'AboutPage', segment: 'about', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/account/account.module#AccountPageModule', name: 'AccountPage', segment: 'account', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/app-settings/app-settings.module#AppSettingsPageModule', name: 'AppSettingsPage', segment: 'app-settings', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/change-password/change-password.module#ChangePasswordPageModule', name: 'ChangePasswordPage', segment: 'change-password', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/filter-multi-selection/filter-multi-selection.module#FilterMultiSelectionPageModule', name: 'FilterMultiSelectionPage', segment: 'filter-multi-selection', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/filter-range/filter-range.module#FilterRangePageModule', name: 'FilterRangePage', segment: 'filter-range', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/filter/filter.module#FilterPageModule', name: 'FilterPage', segment: 'filter', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/help-faq/help-faq.module#HelpFaqPageModule', name: 'HelpFaqPage', segment: 'help-faq', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/men-women-category-pop-over/men-women-category-pop-over.module#MenWomenCategoryPopOverPageModule', name: 'MenWomenCategoryPopOverPage', segment: 'men-women-category-pop-over', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/popup-modal/popup-modal.module#PopupModalPageModule', name: 'PopupModalPage', segment: 'popup-modal', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/product-colour-pop-over/product-colour-pop-over.module#ProductColourPopOverPageModule', name: 'ProductColourPopOverPage', segment: 'product-colour-pop-over', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/product-details/product-details.module#ProductDetailsPageModule', name: 'ProductDetailsPage', segment: 'product-details', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/product-size-pop-over/product-size-pop-over.module#ProductSizePopOverPageModule', name: 'ProductSizePopOverPage', segment: 'product-size-pop-over', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/recommended-pop-over/recommended-pop-over.module#RecommendedPopOverPageModule', name: 'RecommendedPopOverPage', segment: 'recommended-pop-over', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/products/products.module#ProductsPageModule', name: 'ProductsPage', segment: 'products', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/saved-items/saved-items.module#SavedItemsPageModule', name: 'SavedItemsPage', segment: 'saved-items', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/speaker-detail1/speaker-detail1.module#SpeakerDetail1PageModule', name: 'SpeakerDetail1Page', segment: 'speaker-detail1', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/speaker-detail10/speaker-detail10.module#SpeakerDetail10PageModule', name: 'SpeakerDetail10Page', segment: 'speaker-detail10', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/speaker-detail2/speaker-detail2.module#SpeakerDetail2PageModule', name: 'SpeakerDetail2Page', segment: 'speaker-detail2', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/speaker-detail3/speaker-detail3.module#SpeakerDetail3PageModule', name: 'SpeakerDetail3Page', segment: 'speaker-detail3', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/speaker-detail4/speaker-detail4.module#SpeakerDetail4PageModule', name: 'SpeakerDetail4Page', segment: 'speaker-detail4', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/speaker-detail5/speaker-detail5.module#SpeakerDetail5PageModule', name: 'SpeakerDetail5Page', segment: 'speaker-detail5', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/speaker-detail6/speaker-detail6.module#SpeakerDetail6PageModule', name: 'SpeakerDetail6Page', segment: 'speaker-detail6', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/speaker-detail7/speaker-detail7.module#SpeakerDetail7PageModule', name: 'SpeakerDetail7Page', segment: 'speaker-detail7', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/speaker-detail8/speaker-detail8.module#SpeakerDetail8PageModule', name: 'SpeakerDetail8Page', segment: 'speaker-detail8', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/speaker-detail9/speaker-detail9.module#SpeakerDetail9PageModule', name: 'SpeakerDetail9Page', segment: 'speaker-detail9', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/category/category.module#CategoryPageModule', name: 'CategoryPage', segment: 'category', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_25__ionic_storage__["a" /* IonicStorageModule */].forRoot({ name: '__mydb',
                    driverOrder: ['indexeddb', 'sqlite', 'websql'] }),
                __WEBPACK_IMPORTED_MODULE_20__angular_http__["b" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_19__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_13__pages_saved_items_saved_items_module__["SavedItemsPageModule"]
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_6__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_about_about__["a" /* AboutPage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_men_women_category_pop_over_men_women_category_pop_over__["a" /* MenWomenCategoryPopOverPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_recommended_pop_over_recommended_pop_over__["a" /* RecommendedPopOverPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_product_size_pop_over_product_size_pop_over__["a" /* ProductSizePopOverPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_product_colour_pop_over_product_colour_pop_over__["a" /* ProductColourPopOverPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_saved_items_saved_items__["a" /* SavedItemsPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_account_account__["a" /* AccountPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_app_settings_app_settings__["a" /* AppSettingsPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_help_faq_help_faq__["a" /* HelpFaqPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_filter_filter__["a" /* FilterPage */],
                __WEBPACK_IMPORTED_MODULE_22__pages_product_details_product_details__["a" /* ProductDetailsPage */],
                __WEBPACK_IMPORTED_MODULE_23__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_24__pages_change_password_change_password__["a" /* ChangePasswordPage */],
                __WEBPACK_IMPORTED_MODULE_26__pages_speaker_detail1_speaker_detail1__["a" /* SpeakerDetail1Page */],
                __WEBPACK_IMPORTED_MODULE_27__pages_speaker_detail2_speaker_detail2__["a" /* SpeakerDetail2Page */],
                __WEBPACK_IMPORTED_MODULE_28__pages_speaker_detail3_speaker_detail3__["a" /* SpeakerDetail3Page */],
                __WEBPACK_IMPORTED_MODULE_29__pages_speaker_detail4_speaker_detail4__["a" /* SpeakerDetail4Page */],
                __WEBPACK_IMPORTED_MODULE_30__pages_speaker_detail5_speaker_detail5__["a" /* SpeakerDetail5Page */],
                __WEBPACK_IMPORTED_MODULE_31__pages_speaker_detail6_speaker_detail6__["a" /* SpeakerDetail6Page */],
                __WEBPACK_IMPORTED_MODULE_32__pages_speaker_detail7_speaker_detail7__["a" /* SpeakerDetail7Page */],
                __WEBPACK_IMPORTED_MODULE_33__pages_speaker_detail8_speaker_detail8__["a" /* SpeakerDetail8Page */],
                __WEBPACK_IMPORTED_MODULE_34__pages_speaker_detail9_speaker_detail9__["a" /* SpeakerDetail9Page */],
                __WEBPACK_IMPORTED_MODULE_35__pages_speaker_detail10_speaker_detail10__["a" /* SpeakerDetail10Page */],
                __WEBPACK_IMPORTED_MODULE_36__pages_popup_modal_popup_modal__["a" /* PopupModalPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_3__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_splash_screen__["a" /* SplashScreen */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_18__providers_api_service_api_service__["a" /* ApiServiceProvider */],
                __WEBPACK_IMPORTED_MODULE_21__ionic_native_in_app_browser_ngx__["a" /* InAppBrowser */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 44:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ApiServiceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(77);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/*
  Generated class for the ApiServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
//Declare a httpOptions and mention the header in json format so that HTTP Post method can use this option.
var httpOptions = {
    headers: new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpHeaders */]({ 'Content-Type': 'application/json' })
};
var ApiServiceProvider = /** @class */ (function () {
    function ApiServiceProvider(https) {
        this.https = https;
        this.UserLoginAPI = "https://api.acccim-registration.org.my/api/SignIn?id=";
        this.DiamondSponsorAds = 'https://api.acccim-registration.org.my/api/DiamondSponsorAds';
        this.PlatinumSlider = 'https://api.acccim-registration.org.my/api/HomePagePlatinum';
        this.GoldSlider = 'https://api.acccim-registration.org.my/api/GoldSponsorSlider';
        this.SilverSlider = 'https://api.acccim-registration.org.my/api/HomePageSilver';
        this.SupportingSlider = 'https://api.acccim-registration.org.my/api/HomePageSupporting';
        this.advertisement = 'https://api.acccim-registration.org.my/api/HomePageAdvertisement';
        this.PlatinumCategories = 'https://api.acccim-registration.org.my/api/SponsorPageCategoriesPlatinum';
        this.GoldCategories = 'https://api.acccim-registration.org.my/api/SponsorPageCategoriesGold';
        this.SilverCategories = 'https://api.acccim-registration.org.my/api/SponsorPageCategoriesSilver';
        this.SupportingCategories = 'https://api.acccim-registration.org.my/api/SponsorPageCategoriesSupporting';
        this.ClickToPrompt = "https://api.acccim-registration.org.my/api/ClickToPromptAds?AdsId=";
        this.SponsorPageAdvertisement = 'https://api.acccim-registration.org.my/api/SponsorPageAdvertisement';
        this.HomePageDiamond = "https://api.acccim-registration.org.my/api/HomePageDiamond";
        this.SponsorArrangementAPI = "https://api.acccim-registration.org.my/api/SponsorArrangementDisplay";
        this.eventListing = "https://api.acccim-registration.org.my/api/EventListing";
        this.specificevent = "https://api.acccim-registration.org.my/api/SpecificEvent?eventid=";
        this.PopularEvent = "https://api.acccim-registration.org.my/api/PopularEvent";
        this.changePassword = "https://api.acccim-registration.org.my/api/ChangePassword?id=";
        this.getUserData = "https://api.acccim-registration.org.my/api/UserRegistrationData?id=";
    }
    //User Login
    ApiServiceProvider.prototype.userlogin = function (user, pass) {
        var id = user;
        var pw = pass;
        if (id != null && pw != null) {
            return this.https.get(this.UserLoginAPI + id + '&&pw=' + pw);
        }
    };
    ApiServiceProvider.prototype.getDiamondSponsors = function () {
        //using https.get to retrieve the api url
        return this.https.get(this.DiamondSponsorAds);
    };
    ApiServiceProvider.prototype.getPlatinumSponsorSlider = function () {
        //using https.get to retrieve the api url
        return this.https.get(this.PlatinumSlider);
    };
    ApiServiceProvider.prototype.getGoldSponsorSlider = function () {
        //using https.get to retrieve the api url
        return this.https.get(this.GoldSlider);
    };
    ApiServiceProvider.prototype.getSilverSponsorSlider = function () {
        //using https.get to retrieve the api url
        return this.https.get(this.SilverSlider);
    };
    ApiServiceProvider.prototype.getSupportingSponsorSlider = function () {
        //using https.get to retrieve the api url
        return this.https.get(this.SupportingSlider);
    };
    ApiServiceProvider.prototype.getAdvertisement = function () {
        return this.https.get(this.advertisement);
    };
    ApiServiceProvider.prototype.getPlatinumCategories = function () {
        return this.https.get(this.PlatinumCategories);
    };
    ApiServiceProvider.prototype.getGoldCategories = function () {
        return this.https.get(this.GoldCategories);
    };
    ApiServiceProvider.prototype.getSilverCategories = function () {
        return this.https.get(this.SilverCategories);
    };
    ApiServiceProvider.prototype.getSupportingCategories = function () {
        return this.https.get(this.SupportingCategories);
    };
    ApiServiceProvider.prototype.getHomePageDiamond = function () {
        return this.https.get(this.HomePageDiamond);
    };
    ApiServiceProvider.prototype.getSponsorAdvertisement = function () {
        return this.https.get(this.SponsorPageAdvertisement);
    };
    ApiServiceProvider.prototype.getSponsorGridList = function () {
        return this.https.get(this.SponsorArrangementAPI);
    };
    ApiServiceProvider.prototype.getEvent = function () {
        return this.https.get(this.eventListing);
    };
    ApiServiceProvider.prototype.getSpecificEvent = function (evtid) {
        var eventid = evtid;
        return this.https.get(this.specificevent + eventid);
    };
    ApiServiceProvider.prototype.getSpecificAds = function (adsid) {
        var ads = adsid;
        return this.https.get(this.ClickToPrompt + ads);
    };
    ApiServiceProvider.prototype.popularevent = function () {
        return this.https.get(this.PopularEvent);
    };
    ApiServiceProvider.prototype.UpdatePassword = function (id, oldpassword, newpassword) {
        return this.https.put(this.changePassword + id + "&oldpassword=" + oldpassword + "&newpassword=" + newpassword, httpOptions)
            .pipe();
    };
    ApiServiceProvider.prototype.getUser = function (id) {
        return this.https.get(this.getUserData + id);
    };
    ApiServiceProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Http */]])
    ], ApiServiceProvider);
    return ApiServiceProvider;
}());

//# sourceMappingURL=api-service.js.map

/***/ }),

/***/ 703:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(368);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(369);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_home_home__ = __webpack_require__(76);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_about_about__ = __webpack_require__(90);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_saved_items_saved_items__ = __webpack_require__(138);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_account_account__ = __webpack_require__(173);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_app_settings_app_settings__ = __webpack_require__(174);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_help_faq_help_faq__ = __webpack_require__(175);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_change_password_change_password__ = __webpack_require__(91);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ionic_storage__ = __webpack_require__(65);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__providers_api_service_api_service__ = __webpack_require__(44);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};













var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen, storage, alertCtrl, loadingController, ApiService, menu) {
        var _this = this;
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.storage = storage;
        this.alertCtrl = alertCtrl;
        this.loadingController = loadingController;
        this.ApiService = ApiService;
        this.menu = menu;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */];
        this.menu.enable(false, 'myMenu');
        this.initializeApp();
        this.storage.get("userid").then(function (val) {
            _this.id = val;
            if (_this.id === null) {
                _this.nouser = true;
            }
            else {
                _this.ApiService.getUser(_this.id)
                    .subscribe(function (res) {
                    _this.userCredential = res.json();
                });
                _this.nouser = false;
            }
        });
    }
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            _this.statusBar.styleDefault();
            _this.splashScreen.hide();
        });
    };
    MyApp.prototype.goToHome = function () {
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */]);
    };
    MyApp.prototype.goToBag = function () {
        this.nav.push(__WEBPACK_IMPORTED_MODULE_5__pages_about_about__["a" /* AboutPage */]);
    };
    MyApp.prototype.goToSavedItems = function () {
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_6__pages_saved_items_saved_items__["a" /* SavedItemsPage */]);
    };
    MyApp.prototype.goToChangePassword = function () {
        this.nav.push(__WEBPACK_IMPORTED_MODULE_10__pages_change_password_change_password__["a" /* ChangePasswordPage */]);
    };
    MyApp.prototype.goToMyAccount = function () {
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_7__pages_account_account__["a" /* AccountPage */]);
    };
    MyApp.prototype.goToAppSettings = function () {
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_8__pages_app_settings_app_settings__["a" /* AppSettingsPage */]);
    };
    MyApp.prototype.goToHelpAndFaq = function () {
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_9__pages_help_faq_help_faq__["a" /* HelpFaqPage */]);
    };
    MyApp.prototype.signOut = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Are You Sure?',
            message: 'You will be Signed out.',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: function () {
                        console.log('Cancel');
                    }
                },
                {
                    text: 'Ok',
                    handler: function () {
                        _this.storage.clear();
                        _this.presentLoadingWithOptions();
                        //this.DisplaySuccessSignOutMessage();
                        _this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */]);
                    }
                }
            ]
        });
        alert.present();
    };
    MyApp.prototype.presentLoadingWithOptions = function () {
        var loading = this.loadingController.create({
            spinner: null,
            duration: 5000,
            content: 'Signing Out...',
            //translucent: true,
            cssClass: 'custom-class custom-loading'
        });
        loading.present();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Nav */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Nav */])
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"C:\Users\domin\Documents\myacccim\myACCCIM App\asos-user\src\app\app.html"*/'<ion-menu id="myMenu" [content]="content">\n\n  <ion-header>\n\n    <ion-toolbar>\n\n      <ion-title class="welcome-title">\n\n        <p>Hey,\n\n          <strong>Julian Saik</strong>\n\n        </p>\n\n      </ion-title>\n\n    </ion-toolbar>\n\n   \n\n  </ion-header> \n\n   \n\n\n\n  <ion-content>\n\n    <ion-list class="menu-list" no-lines>\n\n      <ion-item class="side-menu-item" (click)="goToHome()">\n\n        <ion-icon name="md-home" item-start></ion-icon>\n\n        <button menuClose ion-item>HOME</button>\n\n      </ion-item>\n\n\n\n      <ion-item class="side-menu-item" (click)="goToBag()">\n\n        <ion-icon name="md-information-circle" item-start></ion-icon>\n\n        <button menuClose ion-item>About ACCCIM</button>\n\n      </ion-item>\n\n\n\n      <ion-item class="side-menu-item" (click)="goToChangePassword()">\n\n          <ion-icon name="md-settings" item-start></ion-icon>\n\n          <button menuClose ion-item>Change Password</button>\n\n        </ion-item>\n\n        \n\n     \n\n    </ion-list>\n\n\n\n    \n\n\n\n    <ion-list class="menu-list" *ngIf="checkSignOut">\n\n      <ion-item class="side-menu-item" (click)="signOut()">\n\n        <ion-icon name="md-log-out" item-start></ion-icon>\n\n        <button menuClose ion-item>SIGN OUT</button>\n\n      </ion-item>\n\n    </ion-list>\n\n\n\n   \n\n\n\n    \n\n  </ion-content>\n\n\n\n </ion-menu>\n\n\n\n<!-- <ion-header>\n\n    <ion-navbar>\n\n       \n\n  \n\n      <ion-title *ngIf="nouser">Welcome To ACCCIM</ion-title>\n\n\n\n    </ion-navbar>\n\n  </ion-header> -->\n\n\n\n<ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>'/*ion-inline-end:"C:\Users\domin\Documents\myacccim\myACCCIM App\asos-user\src\app\app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */], __WEBPACK_IMPORTED_MODULE_11__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_12__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* MenuController */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 76:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__men_women_category_pop_over_men_women_category_pop_over__ = __webpack_require__(160);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_common_http__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_http__ = __webpack_require__(77);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_in_app_browser_ngx__ = __webpack_require__(123);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__product_details_product_details__ = __webpack_require__(161);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_storage__ = __webpack_require__(65);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__login_login__ = __webpack_require__(137);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__angular_platform_browser__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__about_about__ = __webpack_require__(90);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__change_password_change_password__ = __webpack_require__(91);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__speaker_detail1_speaker_detail1__ = __webpack_require__(162);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__speaker_detail2_speaker_detail2__ = __webpack_require__(163);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__speaker_detail3_speaker_detail3__ = __webpack_require__(164);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__speaker_detail5_speaker_detail5__ = __webpack_require__(165);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__speaker_detail4_speaker_detail4__ = __webpack_require__(166);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__speaker_detail6_speaker_detail6__ = __webpack_require__(167);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__speaker_detail10_speaker_detail10__ = __webpack_require__(168);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__speaker_detail9_speaker_detail9__ = __webpack_require__(169);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__speaker_detail8_speaker_detail8__ = __webpack_require__(170);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__speaker_detail7_speaker_detail7__ = __webpack_require__(171);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__popup_modal_popup_modal__ = __webpack_require__(172);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};

























var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, popoverCtrl, events, ApiService, httpClient, https, iab, storage, alertCtrl, loadingController, sanitizer, renderer, modal, menu) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.popoverCtrl = popoverCtrl;
        this.events = events;
        this.ApiService = ApiService;
        this.httpClient = httpClient;
        this.https = https;
        this.iab = iab;
        this.storage = storage;
        this.alertCtrl = alertCtrl;
        this.loadingController = loadingController;
        this.sanitizer = sanitizer;
        this.renderer = renderer;
        this.modal = modal;
        this.menu = menu;
        this.homeSegment = "home";
        this.isMenSelected = true;
        this.pageTitle = 'MEN';
        this.gridlistview = "sponsors";
        this.eventinfo = "programme";
        this.fakeadvertisementresult = new Array(10);
        this.fakeshowEvent = new Array(10);
        this.fakeplatinumresult = new Array(10);
        this.fakegoldresult = new Array(10);
        this.fakesilverresult = new Array(10);
        this.fakesupportingresult = new Array(20);
        this.menu.enable(false, 'myMenu');
        this.allRefresh(0);
        this.storage.get("userid").then(function (val) {
            _this.id = val;
            if (_this.id === null) {
                _this.userisnotsignin = true;
                _this.userissignin = false;
            }
            else {
                _this.ApiService.getUser(_this.id)
                    .subscribe(function (res) {
                    _this.userCredential = res.json();
                });
                _this.userisnotsignin = false;
                _this.userissignin = true;
            }
        });
    }
    HomePage_1 = HomePage;
    HomePage.prototype.goToAbout = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_11__about_about__["a" /* AboutPage */]);
    };
    HomePage.prototype.checkStorage = function () {
        var _this = this;
        this.storage.get("userid").then(function (val) {
            _this.id = val;
            if (_this.id === null) {
                _this.usernotyetlogin = true;
                _this.loginbutton = true;
                _this.userislogin = false;
            }
            else {
                _this.ApiService.getUser(_this.id)
                    .subscribe(function (res) {
                    _this.userCredential = res.json();
                });
                _this.userislogin = true;
                _this.loginbutton = false;
                _this.usernotyetlogin = false;
            }
        });
    };
    HomePage.prototype.signIn = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_9__login_login__["a" /* LoginPage */]);
    };
    HomePage.prototype.goToChangePassword = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_12__change_password_change_password__["a" /* ChangePasswordPage */]);
    };
    HomePage.prototype.allRefresh = function (refresher) {
        var _this = this;
        /*#region Home */
        setTimeout(function () {
            _this.ApiService.getHomePageDiamond().subscribe(function (res) {
                var getDiamondSponsor = res.json();
                _this.Diamond = getDiamondSponsor;
                if (refresher != 0) {
                    setTimeout(function () {
                        refresher.complete();
                    }, 2000);
                }
            });
        }, 1000);
        setTimeout(function () {
            _this.ApiService.getPlatinumSponsorSlider().subscribe(function (res) {
                var getPlatinumSponsor = res.json();
                _this.Platinum = getPlatinumSponsor;
                if (refresher != 0) {
                    setTimeout(function () {
                        refresher.complete();
                    }, 2000);
                }
            });
        }, 1000);
        setTimeout(function () {
            _this.ApiService.getGoldSponsorSlider().subscribe(function (res) {
                var getGoldSponsor = res.json();
                _this.Gold = getGoldSponsor;
                if (refresher != 0) {
                    setTimeout(function () {
                        refresher.complete();
                    }, 2000);
                }
            });
        }, 1000);
        setTimeout(function () {
            _this.ApiService.getSilverSponsorSlider().subscribe(function (res) {
                var getSilverSponsor = res.json();
                _this.Silver = getSilverSponsor;
                if (refresher != 0) {
                    setTimeout(function () {
                        refresher.complete();
                    }, 2000);
                }
            });
        }, 1000);
        setTimeout(function () {
            _this.ApiService.getSupportingSponsorSlider().subscribe(function (res) {
                var getSupportingSponsor = res.json();
                _this.Supporting = getSupportingSponsor;
                if (refresher != 0) {
                    setTimeout(function () {
                        refresher.complete();
                    }, 2000);
                }
            });
        }, 1000);
        setTimeout(function () {
            _this.ApiService.getAdvertisement().subscribe(function (res) {
                var getAdvertisementImg = res.json();
                _this.Advertisement = getAdvertisementImg;
                if (refresher != 0) {
                    setTimeout(function () {
                        refresher.complete();
                    }, 2000);
                }
            });
        }, 1000);
        setTimeout(function () {
            _this.ApiService.popularevent()
                .subscribe(function (res) {
                var getPopularEvent = res.json();
                _this.showPopularEvent = getPopularEvent;
                if (refresher != 0) {
                    setTimeout(function () {
                        refresher.complete();
                    }, 2000);
                }
            });
        }, 1000);
        /*#endregion*/
        /*#region Sponsor */
        setTimeout(function () {
            _this.ApiService.getPlatinumCategories()
                .subscribe(function (res) {
                var platinum = res.json();
                _this.platinumitems = platinum;
                _this.initializePlatinumCategories();
                if (refresher != 0) {
                    setTimeout(function () {
                        refresher.complete();
                    }, 2000);
                }
            });
        }, 5000);
        setTimeout(function () {
            _this.ApiService.getGoldCategories()
                .subscribe(function (res) {
                var gold = res.json();
                _this.golditems = gold;
                _this.initializeGoldCategories();
                if (refresher != 0) {
                    setTimeout(function () {
                        refresher.complete();
                    }, 2000);
                }
            });
        }, 5000);
        setTimeout(function () {
            _this.ApiService.getSilverCategories()
                .subscribe(function (res) {
                var silver = res.json();
                _this.silveritems = silver;
                _this.initializeSilverCategories();
                if (refresher != 0) {
                    setTimeout(function () {
                        refresher.complete();
                    }, 2000);
                }
            });
        }, 5000);
        setTimeout(function () {
            _this.ApiService.getSupportingCategories()
                .subscribe(function (res) {
                var supporting = res.json();
                _this.supportingitems = supporting;
                _this.initializeSupportingCategories();
                if (refresher != 0) {
                    setTimeout(function () {
                        refresher.complete();
                    }, 2000);
                }
            });
        }, 5000);
        setTimeout(function () {
            _this.ApiService.getSponsorAdvertisement()
                .subscribe(function (res) {
                var advertisement = res.json();
                _this.advertisementitems = advertisement;
                _this.initializeAdvertisementCategories();
                if (refresher != 0) {
                    setTimeout(function () {
                        refresher.complete();
                    }, 2000);
                }
            });
        }, 5000);
        /*#endregion */
        /*#region Programme */
        setTimeout(function () {
            _this.ApiService.getEvent().subscribe(function (res) {
                _this.items = res.json();
                _this.initializeEvent();
                if (refresher != 0) {
                    setTimeout(function () {
                        refresher.complete();
                    }, 2000);
                }
            });
        }, 5000);
        /*#endregion */
    };
    /*#region Programme Search */
    HomePage.prototype.getEventTitle = function (evt) {
        this.initializeEvent();
        var serVal = evt.target.value;
        if (serVal && serVal.trim() != '') {
            this.showEvent = this.showEvent.filter(function (getevent) {
                return (getevent.EventTitle.toLowerCase().indexOf(serVal.toLowerCase()) > -1);
            });
            this.renderer.invokeElementMethod(evt.target, 'blur');
        }
    };
    HomePage.prototype.initializeEvent = function () {
        this.showEvent = this.items;
    };
    /*#endregion */
    /*#region Sponsor  */
    HomePage.prototype.platinumCategories = function (plt) {
        this.initializePlatinumCategories();
        var serVal = plt.target.value;
        if (serVal && serVal.trim() != '') {
            this.platinumresult = this.platinumresult.filter(function (getplt) {
                return (getplt.CompanyName.toLowerCase().indexOf(serVal.toLowerCase()) > -1);
            });
            this.renderer.invokeElementMethod(plt.target, 'blur');
        }
    };
    HomePage.prototype.initializePlatinumCategories = function () {
        this.platinumresult = this.platinumitems;
    };
    HomePage.prototype.goldCategories = function (gold) {
        this.initializeGoldCategories();
        var serVal = gold.target.value;
        if (serVal && serVal.trim() != '') {
            this.goldresult = this.goldresult.filter(function (getgold) {
                return (getgold.CompanyName.toLowerCase().indexOf(serVal.toLowerCase()) > -1);
            });
            this.renderer.invokeElementMethod(gold.target, 'blur');
        }
    };
    HomePage.prototype.initializeGoldCategories = function () {
        this.goldresult = this.golditems;
    };
    HomePage.prototype.SilverCategories = function (sil) {
        this.initializeSilverCategories();
        var serVal = sil.target.value;
        if (serVal && serVal.trim() != '') {
            this.silverresult = this.silverresult.filter(function (getsil) {
                return (getsil.CompanyName.toLowerCase().indexOf(serVal.toLowerCase()) > -1);
            });
            this.renderer.invokeElementMethod(sil.target, 'blur');
        }
    };
    HomePage.prototype.initializeSilverCategories = function () {
        this.silverresult = this.silveritems;
    };
    HomePage.prototype.SupportingCategories = function (sup) {
        this.initializeSupportingCategories();
        var serVal = sup.target.value;
        if (serVal && serVal.trim() != '') {
            this.supportingresult = this.supportingresult.filter(function (getsup) {
                return (getsup.CompanyName.toLowerCase().indexOf(serVal.toLowerCase()) > -1);
            });
            this.renderer.invokeElementMethod(sup.target, 'blur');
        }
    };
    HomePage.prototype.initializeSupportingCategories = function () {
        this.supportingresult = this.supportingitems;
    };
    HomePage.prototype.advertisementCategories = function (ads) {
        this.initializeAdvertisementCategories();
        var serVal = ads.target.value;
        if (serVal && serVal.trim() != '') {
            this.advertisementresult = this.advertisementresult.filter(function (getads) {
                return (getads.CompanyName.toLowerCase().indexOf(serVal.toLowerCase()) > -1);
            });
            this.renderer.invokeElementMethod(ads.target, 'blur');
        }
    };
    HomePage.prototype.initializeAdvertisementCategories = function () {
        this.advertisementresult = this.advertisementitems;
    };
    HomePage.prototype.OpenLink = function (link) {
        this.iab.create(link, '_blank');
    };
    HomePage.prototype.OpenLink2 = function (link) {
        this.iab.create(link, '_blank');
    };
    HomePage.prototype.OpenLink3 = function (link) {
        this.iab.create(link, '_blank');
    };
    HomePage.prototype.gotoEvent = function (EventID) {
        var _this = this;
        this.ApiService.getSpecificEvent(EventID).subscribe(function (res) {
            var getSpecificEvent = res.json();
            _this.showSpecificEvent = getSpecificEvent;
            _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__product_details_product_details__["a" /* ProductDetailsPage */], {
                data: EventID
            });
        });
    };
    HomePage.prototype.PopUpAds = function (AdsID) {
        var _this = this;
        this.ApiService.getSpecificAds(AdsID).subscribe(function (res) {
            var getSpecificAds = res.json();
            _this.showSpecificAds = getSpecificAds;
            _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_23__popup_modal_popup_modal__["a" /* PopupModalPage */], {
                data: AdsID
            });
        });
    };
    /*#region Speaker Detail*/
    HomePage.prototype.gotoSpeakers1 = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_13__speaker_detail1_speaker_detail1__["a" /* SpeakerDetail1Page */]);
    };
    HomePage.prototype.gotoSpeakers2 = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_14__speaker_detail2_speaker_detail2__["a" /* SpeakerDetail2Page */]);
    };
    HomePage.prototype.gotoSpeakers3 = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_15__speaker_detail3_speaker_detail3__["a" /* SpeakerDetail3Page */]);
    };
    HomePage.prototype.gotoSpeakers4 = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_17__speaker_detail4_speaker_detail4__["a" /* SpeakerDetail4Page */]);
    };
    HomePage.prototype.gotoSpeakers5 = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_16__speaker_detail5_speaker_detail5__["a" /* SpeakerDetail5Page */]);
    };
    HomePage.prototype.gotoSpeakers6 = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_18__speaker_detail6_speaker_detail6__["a" /* SpeakerDetail6Page */]);
    };
    HomePage.prototype.gotoSpeakers7 = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_22__speaker_detail7_speaker_detail7__["a" /* SpeakerDetail7Page */]);
    };
    HomePage.prototype.gotoSpeakers8 = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_21__speaker_detail8_speaker_detail8__["a" /* SpeakerDetail8Page */]);
    };
    HomePage.prototype.gotoSpeakers9 = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_20__speaker_detail9_speaker_detail9__["a" /* SpeakerDetail9Page */]);
    };
    HomePage.prototype.gotoSpeakers10 = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_19__speaker_detail10_speaker_detail10__["a" /* SpeakerDetail10Page */]);
    };
    /*#endregion*/
    HomePage.prototype.signOut = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Are You Sure?',
            message: 'You will be Signed out.',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: function () {
                        console.log('Cancel');
                    }
                },
                {
                    text: 'Ok',
                    handler: function () {
                        _this.storage.clear();
                        _this.presentLoadingWithOptions();
                        //this.DisplaySuccessSignOutMessage();
                        _this.navCtrl.setRoot(HomePage_1);
                    }
                }
            ]
        });
        alert.present();
    };
    HomePage.prototype.presentLoadingWithOptions = function () {
        return __awaiter(this, void 0, void 0, function () {
            var loading;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingController.create({
                            spinner: null,
                            duration: 3000,
                            content: 'Signing Out...',
                            //translucent: true,
                            cssClass: 'custom-class custom-loading'
                        })];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, loading.present()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    HomePage.prototype.changeTab = function (value) {
        this.gridlistview = value;
    };
    HomePage.prototype.showPopOver = function (myEvent) {
        var popover = this.popoverCtrl.create(__WEBPACK_IMPORTED_MODULE_2__men_women_category_pop_over_men_women_category_pop_over__["a" /* MenWomenCategoryPopOverPage */]);
        popover.present({
            ev: myEvent
        });
    };
    HomePage.prototype.goToSavedItems = function () {
        this.navCtrl.push('SavedItemsPage');
    };
    HomePage.prototype.goToCategory = function (category) {
        var navParams = {
            category: category,
            isMenSelected: this.isMenSelected
        };
        this.navCtrl.push('CategoryPage', navParams);
    };
    HomePage.prototype.goToProducts = function () {
        var navParams = {
            category: 'CLOTHING',
            isMenSelected: this.isMenSelected
        };
        this.navCtrl.push('ProductsPage', navParams);
    };
    //Move to Next slide
    HomePage.prototype.slideNext = function (object, slideView) {
        var _this = this;
        slideView.slideNext(500).then(function () {
            _this.checkIfNavDisabled(object, slideView);
        });
    };
    //Move to previous slide
    HomePage.prototype.slidePrev = function (object, slideView) {
        var _this = this;
        slideView.slidePrev(500).then(function () {
            _this.checkIfNavDisabled(object, slideView);
        });
        ;
    };
    //Method called when slide is changed by drag or navigation
    HomePage.prototype.SlideDidChange = function (object, slideView) {
        this.checkIfNavDisabled(object, slideView);
    };
    //Call methods to check if slide is first or last to enable disbale navigation  
    HomePage.prototype.checkIfNavDisabled = function (object, slideView) {
        this.checkisBeginning(object, slideView);
        this.checkisEnd(object, slideView);
    };
    HomePage.prototype.checkisBeginning = function (object, slideView) {
        slideView.isBeginning().then(function (istrue) {
            object.isBeginningSlide = istrue;
        });
    };
    HomePage.prototype.checkisEnd = function (object, slideView) {
        slideView.isEnd().then(function (istrue) {
            object.isEndSlide = istrue;
        });
    };
    HomePage = HomePage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"C:\Users\domin\Documents\myacccim\myACCCIM App\asos-user\src\pages\home\home.html"*/'<ion-header>\n\n  <ion-navbar>\n\n\n\n    <ion-title *ngIf="userisnotsignin">Welcome To ACCCIM</ion-title>\n\n    <div *ngFor="let username of userCredential">\n\n      <ion-title *ngIf="userissignin">Hi {{username.EnglishName}} </ion-title>\n\n    </div>\n\n\n\n\n\n\n\n    <ion-buttons right>\n\n    </ion-buttons>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content fullscreen>\n\n  \n\n    <ion-refresher slot="fixed" (ionRefresh)="allRefresh($event);">\n\n        <ion-refresher-content></ion-refresher-content>\n\n      </ion-refresher>\n\n  <div [ngSwitch]="homeSegment">\n\n\n\n    <div *ngSwitchCase="\'home\'">\n\n       \n\n         \n\n         <ion-row style="margin-top: 5px;">\n\n            <ion-col col-12>\n\n              <ion-slides *ngIf="Diamond && Diamond.length" class="image-slider" loop="true" slidesPerView="1.0"\n\n                autoplay="2000" speed="500" style="width: 100%; height: 100%px; ">\n\n                <ion-slide *ngFor="let img of Diamond">\n\n                  <img style="width: 100%; height:100%;" src="https://admin.acccim-registration.org.my{{img}}"\n\n                    class="thumb-img" imageViewer/>\n\n                </ion-slide>\n\n              </ion-slides>\n\n            </ion-col>\n\n            </ion-row>\n\n          \n\n      <ion-row style="margin-bottom: -75px;">\n\n        <h6 style="margin-left: 18px;">Platinum Sponsors</h6>\n\n       \n\n        <ion-grid id="related-products"> \n\n          <ion-scroll scrollX="true" scroll-avatar>\n\n            <ion-col class="scroll-item" padding *ngFor="let img of Platinum">\n\n              <div (click)="PopUpAds(img.AdsID)">\n\n\n\n\n\n                <img class="product-image" src="https://admin.acccim-registration.org.my{{img.HomePageAds}}">\n\n\n\n\n\n                <ion-item no-lines>\n\n                  <p class="discount-price" item-start>{{img.CompanyName}}</p>\n\n                </ion-item>\n\n\n\n\n\n              </div>\n\n            </ion-col>\n\n\n\n          </ion-scroll>\n\n        </ion-grid>\n\n      </ion-row>\n\n\n\n      <ion-row style="margin-bottom: -75px;">\n\n        <h6 style="margin-left: 18px;">Gold Sponsors</h6>\n\n        <ion-grid id="related-products">\n\n\n\n\n\n          <ion-scroll scrollX="true" scroll-avatar>\n\n            <ion-col class="scroll-item" padding *ngFor="let img of Gold">\n\n              <div (click)="PopUpAds(img.AdsID)">\n\n\n\n\n\n                <img class="product-image" src="https://admin.acccim-registration.org.my{{img.HomePageAds}}">\n\n\n\n\n\n                <ion-item no-lines>\n\n                  <p class="discount-price" item-start>{{img.CompanyName}}</p>\n\n                </ion-item>\n\n\n\n\n\n              </div>\n\n            </ion-col>\n\n\n\n          </ion-scroll>\n\n        </ion-grid>\n\n      </ion-row>\n\n\n\n      <ion-row style="margin-bottom: -75px;">\n\n        <h6 style="margin-left: 18px;">Silver Sponsors</h6>\n\n        <ion-grid id="related-products">\n\n\n\n\n\n          <ion-scroll scrollX="true" scroll-avatar>\n\n            <ion-col class="scroll-item" padding *ngFor="let img of Silver">\n\n              <div (click)="PopUpAds(img.AdsID)">\n\n\n\n\n\n                <img class="product-image" src="https://admin.acccim-registration.org.my{{img.HomePageAds}}">\n\n\n\n\n\n                <ion-item no-lines>\n\n                  <p class="discount-price" item-start>{{img.CompanyName}}</p>\n\n                </ion-item>\n\n\n\n\n\n              </div>\n\n            </ion-col>\n\n\n\n          </ion-scroll>\n\n        </ion-grid>\n\n      </ion-row>\n\n\n\n      <ion-row style="margin-bottom: -75px;">\n\n        <h6 style="margin-left: 18px;">Supporting Sponsors</h6>\n\n        <ion-grid id="related-products">\n\n\n\n\n\n          <ion-scroll scrollX="true" scroll-avatar>\n\n            <ion-col class="scroll-item" padding *ngFor="let img of Supporting">\n\n              <div (click)="PopUpAds(img.AdsID)">\n\n\n\n\n\n                <img class="product-image" src="https://admin.acccim-registration.org.my{{img.HomePageAds}}">\n\n\n\n\n\n                <ion-item no-lines>\n\n                  <p class="discount-price" item-start>{{img.CompanyName}}</p>\n\n                </ion-item>\n\n\n\n\n\n              </div>\n\n            </ion-col>\n\n\n\n          </ion-scroll>\n\n        </ion-grid>\n\n\n\n      </ion-row>\n\n      <ion-row style="margin-top: 5px;">\n\n          <h6 style="margin-left: 18px;">Advertisement</h6>\n\n        <ion-grid id="related-products">\n\n\n\n\n\n          <ion-scroll scrollX="true" scroll-avatar>\n\n            <ion-col class="scroll-item" padding *ngFor="let img of Advertisement">\n\n              <div (click)="PopUpAds(img.AdsID)">\n\n\n\n\n\n                <img class="product-image" src="https://admin.acccim-registration.org.my{{img.FullListing}}">\n\n\n\n\n\n                <ion-item no-lines>\n\n                  <p class="discount-price" item-start>{{img.CompanyName}}</p>\n\n                </ion-item>\n\n\n\n\n\n              </div>\n\n            </ion-col>\n\n\n\n          </ion-scroll>\n\n        </ion-grid>\n\n      </ion-row>\n\n\n\n\n\n\n\n\n\n\n\n      <ion-row style="margin-top: 5px;">\n\n\n\n          <h6 style="margin-left: 18px;">Browse Event</h6>\n\n        <ion-grid id="related-products">\n\n\n\n\n\n          <ion-scroll scrollX="true" scroll-avatar>\n\n            <ion-col class="scroll-item" padding *ngFor="let eventhome of showPopularEvent">\n\n              <div (click)="gotoEvent(eventhome.EventID);">\n\n\n\n\n\n                <img class="product-image" *ngIf="eventhome.EventImage"\n\n                  src="http://admin.acccim-registration.org.my{{eventhome.EventImage}}">\n\n                <img class="product-image" *ngIf="!eventhome.EventImage" src="../../assets/imgs/ACCIMlogo.png">\n\n\n\n                <ion-item no-lines>\n\n                  <p class="discount-price" item-start>{{eventhome.EventTitle}}</p>\n\n                </ion-item>\n\n\n\n                <p class="product-brand-description" *ngIf="eventhome.VenueName">Venue: {{eventhome.VenueName}}</p>\n\n                <p class="product-brand-description" *ngIf="!eventhome.VenueName">Venue: N/A</p>\n\n              </div>\n\n            </ion-col>\n\n\n\n          </ion-scroll>\n\n        </ion-grid>\n\n\n\n      </ion-row>\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n    </div>\n\n\n\n    <div *ngSwitchCase="\'event\'">\n\n      \n\n\n\n      <ion-segment [(ngModel)]="eventinfo">\n\n\n\n        <ion-segment-button value="programme">\n\n          <p class="item-title" style="color:#ED6D10;">Programme</p>\n\n        </ion-segment-button>\n\n\n\n        <ion-segment-button value="speakers">\n\n          <p class="item-title" style="color:#ED6D10;">Speakers</p>\n\n\n\n        </ion-segment-button>\n\n\n\n\n\n\n\n      </ion-segment>\n\n\n\n      <div [ngSwitch]="eventinfo">\n\n        <div *ngSwitchCase="\'speakers\'">\n\n          <ion-card>\n\n            <ion-grid (click)="gotoSpeakers1();">\n\n              <ion-row>\n\n                <ion-col col-6>\n\n\n\n\n\n                  <img style=" border-radius: 9px"\n\n                    src="../../assets/imgs/1.png" class="responsive">\n\n\n\n\n\n\n\n                </ion-col>\n\n\n\n                <ion-col col-6>\n\n                  <p style="margin-top: 8px;">马来西亚全国总商会总会长暨马来西亚中华总商会（中总）总会长</p>\n\n                  <p style="margin-top: 8px;"><b>丹斯里拿督戴良业</b></p>\n\n                  <p style="margin-top: 8px;"><b>Tan Sri Datuk Ter Leong Yap,</b></p>\n\n                  <p><b>President of The National Chamber of Commerce and Industry of Malaysia (NCCIM) and President of\n\n                      The Associated Chinese Chambers of Commerce and Industry of Malaysia (ACCCIM)</b></p>\n\n                </ion-col>\n\n              </ion-row>\n\n            </ion-grid>\n\n          </ion-card>\n\n\n\n          <ion-card>\n\n            <ion-grid (click)="gotoSpeakers2();">\n\n              <ion-row>\n\n                <ion-col col-6>\n\n\n\n\n\n                  <img style=" border-radius: 9px"\n\n                    src="../../assets/imgs/3.png" class="responsive">\n\n\n\n\n\n\n\n                </ion-col>\n\n\n\n                <ion-col col-6>\n\n                  <p style="margin-top: 8px;">Pekat集团董事总经理秦树茂先生</p>\n\n                  <p style="margin-top: 8px;"><b>Mr. Chin Soo Mau, Group Managing Director of Pekat Group of Companies\n\n                    </b></p>\n\n\n\n\n\n\n\n\n\n                </ion-col>\n\n              </ion-row>\n\n            </ion-grid>\n\n          </ion-card>\n\n\n\n          <ion-card>\n\n            <ion-grid (click)="gotoSpeakers3();">\n\n              <ion-row>\n\n                <ion-col col-6>\n\n\n\n\n\n                  <img style=" border-radius: 9px"\n\n                    src="../../assets/imgs/2.png" class="responsive">\n\n\n\n\n\n\n\n                </ion-col>\n\n\n\n                <ion-col col-6>\n\n                  <p style="margin-top: 8px;">创奕能源科技股份有限公司技术长庄嘉明先生</p>\n\n                  <p style="margin-top: 8px;"><b>Mr. Charles Chuang, Chief Technology Officer of Tron Energy Technology\n\n                      Corp</b></p>\n\n\n\n\n\n\n\n\n\n                </ion-col>\n\n              </ion-row>\n\n            </ion-grid>\n\n          </ion-card>\n\n\n\n          <ion-card>\n\n            <ion-grid (click)="gotoSpeakers4();">\n\n              <ion-row>\n\n                <ion-col col-6>\n\n\n\n\n\n                  <img style="border-radius: 9px"\n\n                    src="../../assets/imgs/9.png" class="responsive">\n\n\n\n\n\n\n\n                </ion-col>\n\n\n\n                <ion-col col-6>\n\n                  <p style="margin-top: 8px;">高昇集团董事经理胡琬真女士</p>\n\n                  <p style="margin-top: 8px;"><b>Ms. Stefanie Aw, Managing Director of Acten Group</b></p>\n\n\n\n\n\n\n\n\n\n                </ion-col>\n\n              </ion-row>\n\n            </ion-grid>\n\n          </ion-card>\n\n\n\n          <ion-card>\n\n            <ion-grid (click)="gotoSpeakers5();">\n\n              <ion-row>\n\n                <ion-col col-6>\n\n\n\n\n\n                  <img style=" border-radius: 9px"\n\n                    src="../../assets/imgs/7.png" class="responsive">\n\n\n\n\n\n\n\n                </ion-col>\n\n\n\n                <ion-col col-6>\n\n                  <p style="margin-top: 8px;">Tera VA Sdn. Bhd.首席执行官暨筹委会主席梁哲民先生</p>\n\n                  <p style="margin-top: 8px;"><b>Mr. Michael Leong, </b></p>\n\n                  <p><b>CEO of Tera VA Sdn. Bhd. cum Chairman of Organising Committee</b></p>\n\n\n\n\n\n\n\n\n\n                </ion-col>\n\n              </ion-row>\n\n            </ion-grid>\n\n          </ion-card>\n\n\n\n          <ion-card>\n\n            <ion-grid (click)="gotoSpeakers6();">\n\n              <ion-row>\n\n                <ion-col col-6>\n\n\n\n\n\n                  <img style=" border-radius: 9px" src="../../assets/imgs/10.png" class="responsive">\n\n\n\n\n\n\n\n                </ion-col>\n\n\n\n                <ion-col col-6>\n\n                  <p style="margin-top: 8px;">iPay88 Sdn. Bhd.执行董事林国兴先生</p>\n\n                  <p style="margin-top: 8px;"><b>Mr. Lim Kok Hing, Executive Director of iPay88 Sdn. Bhd.</b></p>\n\n\n\n\n\n\n\n\n\n                </ion-col>\n\n              </ion-row>\n\n            </ion-grid>\n\n          </ion-card>\n\n\n\n          <ion-card>\n\n            <ion-grid (click)="gotoSpeakers7();">\n\n              <ion-row>\n\n                <ion-col col-6>\n\n\n\n\n\n                  <img style="border-radius: 9px" src="../../assets/imgs/5.png" class="responsive">\n\n\n\n\n\n\n\n                </ion-col>\n\n\n\n                <ion-col col-6>\n\n                  <p style="margin-top: 8px;">Axiata Digital eCode私人有限公司策略及特别项目董事涂汉鑫先生</p>\n\n                  <p style="margin-top: 8px;"><b>Mr. Hanson Toh,</b></p>\n\n                  <p><b> Director, Strategy & Special Projects, Axiata Digital eCode Sdn Bhd</b></p>\n\n\n\n\n\n\n\n\n\n                </ion-col>\n\n              </ion-row>\n\n            </ion-grid>\n\n          </ion-card>\n\n\n\n          <ion-card>\n\n            <ion-grid (click)="gotoSpeakers8();">\n\n              <ion-row>\n\n                <ion-col col-6>\n\n\n\n\n\n                  <img style="border-radius: 9px"\n\n                    src="../../assets/imgs/6.png" class="responsive">\n\n\n\n\n\n\n\n                </ion-col>\n\n\n\n                <ion-col col-6>\n\n                  <p style="margin-top: 8px;">Setel首席执行官 Mr. Iskandar Ezzahuddin</p>\n\n                  <p style="margin-top: 8px;"><b>Mr. Iskandar Ezzahuddin, Chief Executive Officer, Setel</b></p>\n\n\n\n\n\n\n\n\n\n                </ion-col>\n\n              </ion-row>\n\n            </ion-grid>\n\n          </ion-card>\n\n\n\n          <ion-card>\n\n            <ion-grid (click)="gotoSpeakers9();">\n\n              <ion-row>\n\n                <ion-col col-6>\n\n\n\n\n\n                  <img style=" border-radius: 9px"\n\n                    src="../../assets/imgs/4.png" class="responsive">\n\n\n\n\n\n\n\n                </ion-col>\n\n\n\n                <ion-col col-6>\n\n                  <p style="margin-top: 8px;">Soft Space策略总监梁耀聪先生</p>\n\n                  <p style="margin-top: 8px;"><b>Mr. Chris Leong,</b></p>\n\n                  <p><b>Chief Strategy Officer of Soft Space</b></p>\n\n\n\n\n\n\n\n\n\n                </ion-col>\n\n              </ion-row>\n\n            </ion-grid>\n\n          </ion-card>\n\n\n\n          <ion-card>\n\n            <ion-grid (click)="gotoSpeakers10();">\n\n              <ion-row>\n\n                <ion-col col-6>\n\n\n\n\n\n                  <img style=" border-radius: 9px"\n\n                    src="../../assets/imgs/8.png" class="responsive">\n\n\n\n\n\n\n\n                </ion-col>\n\n\n\n                <ion-col col-6>\n\n                  <p style="margin-top: 8px;">马来西亚中华总商会第一副总秘书蔡文洲律师</p>\n\n                  <p style="margin-top: 8px;"><b>Mr. Michael Chai Woon Chew,</b></p>\n\n                  <p><b>Deputy Secretary-General I of ACCCIM</b></p>\n\n\n\n\n\n\n\n\n\n                </ion-col>\n\n              </ion-row>\n\n            </ion-grid>\n\n          </ion-card>\n\n\n\n        </div>\n\n      </div>\n\n\n\n\n\n      <div [ngSwitch]="eventinfo">\n\n        <div *ngSwitchCase="\'programme\'">\n\n          <ion-searchbar (ionInput)="getEventTitle($event);" [debounce]="500" placeholder="Search By Event Title Only">\n\n          </ion-searchbar>\n\n\n\n          <div *ngIf="showEvent">\n\n              <div *ngFor="let item of showEvent" style="margin-top:30px;">\n\n                  <div>\n\n                    <ion-card>\n\n                      <ion-grid (click)="gotoEvent(item.EventID);">\n\n                        <ion-row>\n\n                          <ion-col col-12>\n\n      \n\n                            <div *ngIf="item.EventImage">\n\n                              <img style="width: 100%; height: 100%; border-radius: 9px"\n\n                                src="http://admin.acccim-registration.org.my{{item.EventImage}}">\n\n                            </div>\n\n                            <div *ngIf="!item.EventImage">\n\n                              <img style="width: 100%; height: 100%; border-radius: 9px"\n\n                                src="../../assets/imgs/ACCIMlogo.png">\n\n                            </div>\n\n      \n\n      \n\n      \n\n                          </ion-col>\n\n                        </ion-row>\n\n                        <ion-row>\n\n      \n\n                          <ion-col col-12>\n\n                            <p style="margin-top: 8px;" text-center><b>{{item.EventTitle}}</b></p>\n\n      \n\n      \n\n      \n\n                          </ion-col>\n\n                        </ion-row>\n\n                        <ion-row>\n\n                          <ion-col col-2>\n\n                            <p>\n\n                              <ion-icon name="calendar" style="font-size:30px; margin-right:20px;"></ion-icon>\n\n                            </p>\n\n                          </ion-col>\n\n                          <ion-col col-10>\n\n                            <p class="product-price">{{item.StartDate | date: "yyyy/MM/dd"}}</p>\n\n                            <p class="product-price"> {{item.StartTime}}</p>\n\n                          </ion-col>\n\n      \n\n                          <ion-col col-2>\n\n                            <p>\n\n                              <ion-icon name="person" style="font-size:30px; margin-right:20px;"></ion-icon>\n\n                            </p>\n\n                          </ion-col>\n\n                          <ion-col col-10>\n\n                            <p><i>Available For :</i></p>\n\n                            <div *ngFor="let gett of item.MemberAvailability_tb">\n\n                              <p>{{gett.MemberType}}</p>\n\n                            </div>\n\n                          </ion-col>\n\n                        </ion-row>\n\n                      </ion-grid>\n\n                    </ion-card>\n\n                  </div>\n\n      \n\n      \n\n                </div>\n\n          </div>\n\n\n\n          <div *ngIf="!showEvent">\n\n              <div *ngFor="let item of fakeshowEvent" style="margin-top:30px;" class="fakeshowEvent">\n\n                  <div>\n\n                    <ion-card>\n\n                      <ion-grid>\n\n                        <ion-row>\n\n                          <ion-col col-12>\n\n      \n\n                            <div>\n\n                              <img style="width: 100%; height: 100%; border-radius: 9px"\n\n                                src="../../assets/imgs/thumbnail.jpg">\n\n                            </div>\n\n                            \n\n      \n\n      \n\n      \n\n                          </ion-col>\n\n                        </ion-row>\n\n                        <ion-row>\n\n      \n\n                          <ion-col col-12>\n\n                            <p></p>\n\n      \n\n      \n\n      \n\n                          </ion-col>\n\n                        </ion-row>\n\n                        <ion-row>\n\n                          <ion-col col-2>\n\n                            <label>\n\n                              <ion-icon></ion-icon>\n\n                            </label>\n\n                          </ion-col>\n\n                          <ion-col col-10>\n\n                            <label class="product-price"></label>\n\n                            <label class="product-price"></label>\n\n                          </ion-col>\n\n      \n\n                          <ion-col col-2>\n\n                            <label>\n\n                              <ion-icon></ion-icon>\n\n                            </label>\n\n                          </ion-col>\n\n                         \n\n                        </ion-row>\n\n                      </ion-grid>\n\n                    </ion-card>\n\n                  </div>\n\n      \n\n      \n\n                </div>\n\n          </div>\n\n\n\n        \n\n         \n\n\n\n\n\n          \n\n        </div>\n\n      </div>\n\n\n\n\n\n\n\n\n\n    </div>\n\n\n\n    <div *ngSwitchCase="\'sponsors\'">\n\n      \n\n\n\n      <ion-segment [(ngModel)]="gridlistview">\n\n\n\n        <ion-segment-button value="sponsors">\n\n          <p class="item-title" style="color:#ED6D10;">Sponsors</p>\n\n\n\n        </ion-segment-button>\n\n\n\n        <ion-segment-button value="advertisement">\n\n          <p class="item-title" style="color:#ED6D10;">Advertisement</p>\n\n        </ion-segment-button>\n\n\n\n      </ion-segment>\n\n\n\n      <div [ngSwitch]="gridlistview">\n\n        <div *ngSwitchCase="\'sponsors\'">\n\n          <h1 text-center style="margin-top: 20px;">Platinum Sponsors</h1>\n\n        \n\n          <ion-row *ngIf="platinumresult">\n\n            <ion-col col-6 *ngFor="let getadsimage of platinumresult">\n\n              <img style="width: 100%; height: 100%;"\n\n                src="http://admin.acccim-registration.org.my{{getadsimage.GridListing}}" (click)="PopUpAds(getadsimage.AdsID)"/>\n\n            </ion-col>\n\n          </ion-row>\n\n\n\n          <ion-row *ngIf="!platinumresult">\n\n              <ion-col col-6 *ngFor="let getadsimage of fakeplatinumresult" class="fakeplatinumresult">\n\n                <img style="width: 100%; height: 100%;"\n\n                  src="../../assets/imgs/thumbnail.jpg"/>\n\n              </ion-col>\n\n            </ion-row>\n\n\n\n\n\n\n\n          <h1 text-center style="margin-top: 20px;">Gold Sponsors</h1>\n\n          <ion-row *ngIf="goldresult">\n\n            <ion-col col-6 *ngFor="let getadsimage of goldresult">\n\n              <img style="width: 100%; height: 100%;"\n\n                src="http://admin.acccim-registration.org.my{{getadsimage.GridListing}}" (click)="PopUpAds(getadsimage.AdsID)"/>\n\n            </ion-col>\n\n          </ion-row>\n\n          \n\n          <ion-row *ngIf="!goldresult">\n\n              <ion-col col-6 *ngFor="let getadsimage of fakegoldresult" class="fakegoldresult">\n\n                <img style="width: 100%; height: 100%;"\n\n                  src="../../assets/imgs/thumbnail.jpg"/>\n\n              </ion-col>\n\n            </ion-row>\n\n\n\n          <h1 text-center style="margin-top: 20px;">Silver Sponsors</h1>\n\n          <ion-row *ngIf="silverresult">\n\n            <ion-col col-4 *ngFor="let getadsimage of silverresult">\n\n              <img style="width: 100%; height: 100%;"\n\n                src="http://admin.acccim-registration.org.my{{getadsimage.GridListing}}" (click)="PopUpAds(getadsimage.AdsID)"/>\n\n            </ion-col>\n\n          </ion-row>\n\n\n\n          <ion-row *ngIf="!silverresult">\n\n              <ion-col col-4 *ngFor="let getadsimage of fakesilverresult" class="fakesilverresult">\n\n                <img style="width: 100%; height: 100%;"\n\n                  src="../../assets/imgs/thumbnail.jpg"/>\n\n              </ion-col>\n\n            </ion-row>\n\n\n\n          <h1 text-center style="margin-top: 20px;">Supporting Sponsors</h1>\n\n          <ion-row *ngIf="supportingresult">\n\n            <ion-col col-3 *ngFor="let getadsimage of supportingresult">\n\n              <img style="width: 100%; height: 100%;"\n\n                src="http://admin.acccim-registration.org.my{{getadsimage.GridListing}}" (click)="PopUpAds(getadsimage.AdsID)"/>\n\n            </ion-col>\n\n          </ion-row>\n\n\n\n          <ion-row *ngIf="!supportingresult">\n\n              <ion-col col-3 *ngFor="let getadsimage of fakesupportingresult" class="fakesupportingresult">\n\n                <img style="width: 100%; height: 100%;"\n\n                  src="../../assets/imgs/thumbnail.jpg"/>\n\n              </ion-col>\n\n            </ion-row>\n\n        </div>\n\n      </div>\n\n      <div [ngSwitch]="gridlistview">\n\n        <div *ngSwitchCase="\'advertisement\'">\n\n            <ion-searchbar (ionInput)="advertisementCategories($event);" [debounce]="500" placeholder="Search By Company Name">\n\n              </ion-searchbar>\n\n         <ion-row *ngIf="advertisementresult">\n\n            <ion-col col-6 *ngFor="let img of advertisementresult">\n\n                <ion-card>\n\n                    <ion-grid id="related-products">\n\n                      <ion-row>\n\n                        <ion-col col-12>\n\n                          \n\n                          <img style="width: 100%; height: 100%;" src="https://admin.acccim-registration.org.my{{img.FullListing}}" (click)="PopUpAds(img.AdsID)"/>\n\n   \n\n                        </ion-col>\n\n        \n\n                        \n\n                            <ion-item no-lines>\n\n                                <p class="discount-price" style="text-align:center;" item-start>{{img.CompanyName}}</p>\n\n                              </ion-item>\n\n                        \n\n                      </ion-row>\n\n                    </ion-grid>\n\n                  </ion-card>\n\n            </ion-col>\n\n          </ion-row> \n\n\n\n          <ion-row *ngIf="!advertisementresult">\n\n              <ion-col col-6 *ngFor="let img of fakeadvertisementresult" class="fakeadvertisementresult">\n\n                  <ion-card>\n\n                      <ion-grid id="related-products">\n\n                        <ion-row>\n\n                            \n\n                            <ion-item no-lines>\n\n                              <h3>\n\n                                \n\n                              </h3>\n\n                            </ion-item>\n\n                         \n\n          \n\n                          \n\n                              <ion-item no-lines>\n\n                                  <p>\n\n                                     \n\n                                  </p>\n\n                                </ion-item>\n\n                          \n\n                        </ion-row>\n\n                      </ion-grid>\n\n                    </ion-card>\n\n              </ion-col>\n\n            </ion-row> \n\n         \n\n        </div>\n\n        </div>\n\n     \n\n\n\n    </div>\n\n\n\n    <div *ngSwitchCase="\'profile\'">\n\n\n\n      <ion-item *ngIf="usernotyetlogin" (click)="goToAbout()">\n\n        <ion-icon name="md-information-circle" item-start></ion-icon>\n\n        <p>About ACCCIM</p>\n\n      </ion-item>\n\n\n\n      <div *ngFor="let user of userCredential">\n\n        <img [src]="sanitizer.bypassSecurityTrustUrl(user.QRCodeImage)">\n\n\n\n        <div style="text-align:center">\n\n          <ion-row>\n\n            <ion-col col-12>\n\n              <p style="font-size: 20px;">{{user.EventRegID}}</p>\n\n              <p style="font-size: 15px;">{{user.UserType}}</p>\n\n\n\n            </ion-col>\n\n          </ion-row>\n\n\n\n\n\n\n\n\n\n        </div>\n\n        <ion-list class="orders-list">\n\n\n\n\n\n          <ion-item>\n\n            <ion-icon item-start name="md-person"></ion-icon>\n\n            <p>{{user.EnglishName}}</p>\n\n          </ion-item>\n\n\n\n          <ion-item>\n\n            <ion-icon item-start name="md-mail"></ion-icon>\n\n            <p>{{user.Email}}</p>\n\n          </ion-item>\n\n          <ion-item>\n\n            <ion-icon item-start name="md-call"></ion-icon>\n\n            <p>{{user.Contact}}</p>\n\n          </ion-item>\n\n\n\n          <ion-item>\n\n            <ion-icon item-start name="md-bookmark"></ion-icon>\n\n            <p>{{user.Title}}</p>\n\n          </ion-item>\n\n        </ion-list>\n\n\n\n        <ion-list>\n\n          <ion-item>\n\n            <ion-icon item-start name="md-locate"></ion-icon>\n\n            <p>{{user.Constituent}}</p>\n\n          </ion-item>\n\n\n\n          <ion-item>\n\n\n\n\n\n            <ion-icon item-start name="md-briefcase"></ion-icon>\n\n            <p>{{user.CompanyName}}</p>\n\n          </ion-item>\n\n\n\n          <ion-item>\n\n            <ion-icon item-start name="md-people"></ion-icon>\n\n            <p>{{user.OrganizationName}}</p>\n\n          </ion-item>\n\n\n\n\n\n        </ion-list>\n\n\n\n        <ion-item *ngIf="userislogin" (click)="goToAbout()">\n\n          <ion-icon name="md-information-circle" item-start></ion-icon>\n\n          <p>About ACCCIM</p>\n\n        </ion-item>\n\n\n\n        <ion-item (click)="goToChangePassword()">\n\n          <ion-icon item-start name="md-lock"></ion-icon>\n\n          <p>Change Password</p>\n\n        </ion-item>\n\n\n\n        <ion-item (click)="signOut();">\n\n          <ion-icon item-start name="md-log-out"></ion-icon>\n\n          <p>Sign out</p>\n\n        </ion-item>\n\n\n\n\n\n      </div>\n\n\n\n\n\n\n\n      <div style="bottom: 0px; width: 100%" center>\n\n        <div style="text-align: center; padding: right 10px;">\n\n          <ion-row>\n\n            <ion-col col-12>\n\n              <button ion-button style="width:100%;" *ngIf="loginbutton" (click)="signIn()">Sign In</button>\n\n\n\n\n\n            </ion-col>\n\n          </ion-row>\n\n        </div>\n\n      </div>\n\n    </div>\n\n\n\n\n\n  </div>\n\n\n\n\n\n\n\n</ion-content>\n\n\n\n\n\n\n\n<ion-footer>\n\n\n\n  <ion-toolbar class="footer-color">\n\n\n\n    <ion-segment [(ngModel)]="homeSegment">\n\n      <ion-segment-button value="home">\n\n        <ion-icon name="home" style="margin-top: -50px !important; ">\n\n        </ion-icon>\n\n\n\n        <div style="margin-top: -40px !important; ">\n\n          <p class="item-title" style="color:#ED6D10;">Home</p>\n\n        </div>\n\n      </ion-segment-button>\n\n\n\n\n\n\n\n      <ion-segment-button value="event">\n\n        <ion-icon name="list-box" style="margin-top: -50px !important; "></ion-icon>\n\n        <div style="margin-top: -40px !important; ">\n\n          <p class="item-title" style="color:#ED6D10;">Programme</p>\n\n        </div>\n\n      </ion-segment-button>\n\n      <ion-segment-button value="sponsors">\n\n        <ion-icon name="document" style="margin-top: -50px !important; "></ion-icon>\n\n        <div style="margin-top: -40px !important; ">\n\n          <p class="item-title" style="color:#ED6D10;">Sponsors</p>\n\n        </div>\n\n      </ion-segment-button>\n\n\n\n      <ion-segment-button value="profile" (click)="checkStorage();">\n\n        <ion-icon name="person" style="margin-top: -50px !important; "></ion-icon>\n\n        <div style="margin-top: -40px !important; ">\n\n          <p class="item-title" style="color:#ED6D10;">Me</p>\n\n        </div>\n\n      </ion-segment-button>\n\n\n\n\n\n    </ion-segment>\n\n  </ion-toolbar>\n\n</ion-footer>'/*ion-inline-end:"C:\Users\domin\Documents\myacccim\myACCCIM App\asos-user\src\pages\home\home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* PopoverController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */],
            __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_4__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_5__angular_http__["a" /* Http */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_native_in_app_browser_ngx__["a" /* InAppBrowser */],
            __WEBPACK_IMPORTED_MODULE_8__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_10__angular_platform_browser__["c" /* DomSanitizer */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["V" /* Renderer */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* MenuController */]])
    ], HomePage);
    return HomePage;
    var HomePage_1;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 90:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AboutPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_home__ = __webpack_require__(76);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AboutPage = /** @class */ (function () {
    function AboutPage(navCtrl, navParams, app, view) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.app = app;
        this.view = view;
    }
    AboutPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AboutPage');
    };
    AboutPage.prototype.goToSavedItems = function () {
        this.navCtrl.push('SavedItemsPage');
    };
    AboutPage.prototype.goToHome = function () {
        this.app.getRootNav().setRoot(__WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */]);
    };
    AboutPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-about',template:/*ion-inline-start:"C:\Users\domin\Documents\myacccim\myACCCIM App\asos-user\src\pages\about\about.html"*/'<ion-header>\n\n    \n\n  <ion-navbar>\n\n      <ion-title>ABOUT ACCCIM</ion-title>\n\n   \n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n  <ion-col text-center>\n\n      <div class="logo" center>\n\n        <ion-img style="width: 160px; height: 80px;background:none;" src="../../assets/imgs/acccim_logo2_resize.png"></ion-img>\n\n     </div>\n\n    <p class="bag-empty">ACCCIM 中总</p>\n\n<div class="ion-text-justify">\n\n    <p class="bag-description" style="line-height: 2.0em;">中总中央理事会于2011 年8月25日会议议决成立青商组，并于2012年中总第66届第一次中央理事会会议决定，中总青商组正名为青商团。 中总共有17个属会，分布在国内13州及联邦直辖区。迄今中总全马17个属会皆已成立青商团，吸纳各行业青年企业家和专业人士参与。目前全国青商团员总数超过5,000人，成为中总及各属会重要的工作组之一。</p>\n\n  <br/>\n\n    <p class="bag-description" style="line-height: 2.0em;">The ACCCIM National Council, at its meeting held on 25th August 2011, formally resolved to form the ACCCIM Young Entrepreneurs Working Group, and at the 1st Meeting of ACCCIM 66th National Council, decided to change the name to ACCCIM Young Entrepreneurs Committee (YEC).</p>\n\n    <br/>\n\n    <p class="bag-description" style="line-height: 2.0em;">ACCCIM has 17 Constituent Chambers in 13 states and Federal Territory of Malaysia. All the 17 Constituent Chambers have formed their respective YEC, attracting participation by young entrepreneurs and professionals from all sectors. These YECs have now a combined membership of more than 5,000 and have become one of the key working committees of ACCCIM and its respective Constituent Chambers.</p>\n\n  </div>\n\n  \n\n  </ion-col>\n\n</ion-content>'/*ion-inline-end:"C:\Users\domin\Documents\myacccim\myACCCIM App\asos-user\src\pages\about\about.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* App */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* ViewController */]])
    ], AboutPage);
    return AboutPage;
}());

//# sourceMappingURL=about.js.map

/***/ }),

/***/ 91:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChangePasswordPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__login_login__ = __webpack_require__(137);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_storage__ = __webpack_require__(65);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the ChangePasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ChangePasswordPage = /** @class */ (function () {
    function ChangePasswordPage(navCtrl, navParams, formBuilder, ApiService, alertCtrl, loadingCtrl, storage) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.formBuilder = formBuilder;
        this.ApiService = ApiService;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.storage = storage;
        this.ChangePasswordForm = this.formBuilder.group({
            'oldpassword': ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required])],
            'newpassword': ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required])]
        });
        this.storage.get("userid").then(function (val) {
            _this.id = val;
        });
    }
    ChangePasswordPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ChangePasswordPage');
    };
    ChangePasswordPage.prototype.changepassword = function () {
        var _this = this;
        var loading = this.loadingCtrl.create({
            spinner: null,
            duration: 5000,
            content: 'Please Wait...',
            //translucent: true,
            cssClass: 'custom-class custom-loading'
        });
        var alert = this.alertCtrl.create({
            title: 'Password Successfully Changed',
            subTitle: 'You will be signed out to sign in with your new password',
            buttons: [
                {
                    text: "Ok",
                    handler: function () {
                        _this.storage.clear();
                        _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__login_login__["a" /* LoginPage */]);
                    }
                }
            ],
        });
        loading.present();
        this.ApiService.UpdatePassword(this.id, this.oldpassword, this.newpassword)
            .subscribe(function (result) {
            loading.dismiss();
            alert.present();
        });
    };
    ChangePasswordPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-change-password',template:/*ion-inline-start:"C:\Users\domin\Documents\myacccim\myACCCIM App\asos-user\src\pages\change-password\change-password.html"*/'<!--\n\n  Generated template for the ChangePasswordPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>ChangePassword</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n  <div class="login-content">\n\n\n\n    <!-- Logo -->\n\n    <div padding-horizontal text-center class="animated fadeInDown">\n\n      <div class="logo">\n\n        <ion-img style="width: 160px; height: 80px;background:none;" src="../../assets/imgs/ACCIMlogo.png"></ion-img>\n\n      </div>\n\n      \n\n    </div>\n\n\n\n    <!-- Login form -->\n\n<div [formGroup]="ChangePasswordForm"> \n\n    <div class="list-form">\n\n      <ion-item>\n\n        <ion-label floating>\n\n          <ion-icon name="lock" item-start class="text-primary"></ion-icon>\n\n          Old Password\n\n        </ion-label>\n\n        <ion-input type="password" [(ngModel)]="oldpassword" name="oldpassword" formControlName="oldpassword"></ion-input>\n\n      </ion-item>\n\n\n\n      <ion-item>\n\n        <ion-label floating>\n\n          <ion-icon name="lock" item-start class="text-primary"></ion-icon>\n\n          New Password\n\n        </ion-label>\n\n        <ion-input type="password" [(ngModel)]="newpassword" name="newpassword" formControlName="newpassword"></ion-input>\n\n      </ion-item>\n\n    </div>\n\n</div>\n\n\n\n    <div>\n\n\n\n    <br>\n\n      <button ion-button icon-start block class="changepasswordbutton" tappable [disabled]="!ChangePasswordForm.valid" (click)="changepassword()">\n\n       Change Password\n\n      </button>\n\n    \n\n     \n\n\n\n    </div>\n\n\n\n\n\n   \n\n  </div>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\domin\Documents\myacccim\myACCCIM App\asos-user\src\pages\change-password\change-password.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_storage__["b" /* Storage */]])
    ], ChangePasswordPage);
    return ChangePasswordPage;
}());

//# sourceMappingURL=change-password.js.map

/***/ })

},[372]);
//# sourceMappingURL=main.js.map