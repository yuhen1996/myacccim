webpackJsonp([2],{

/***/ 708:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FilterMultiSelectionPageModule", function() { return FilterMultiSelectionPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__filter_multi_selection__ = __webpack_require__(730);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var FilterMultiSelectionPageModule = /** @class */ (function () {
    function FilterMultiSelectionPageModule() {
    }
    FilterMultiSelectionPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__filter_multi_selection__["a" /* FilterMultiSelectionPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__filter_multi_selection__["a" /* FilterMultiSelectionPage */]),
            ],
        })
    ], FilterMultiSelectionPageModule);
    return FilterMultiSelectionPageModule;
}());

//# sourceMappingURL=filter-multi-selection.module.js.map

/***/ }),

/***/ 730:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FilterMultiSelectionPage; });
/* unused harmony export MultiSelectionItem */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var FilterMultiSelectionPage = /** @class */ (function () {
    function FilterMultiSelectionPage(navCtrl, navParams, viewController) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewController = viewController;
        this.anySelection = false;
        this.multiSelectionItems = [];
        this.pageTitle = navParams.get('selectionType');
        this.initialiseMultiSelectionItems();
    }
    FilterMultiSelectionPage.prototype.initialiseMultiSelectionItems = function () {
        this.multiSelectionItems.push(new MultiSelectionItem('Blazers', 114));
        this.multiSelectionItems.push(new MultiSelectionItem('Bomber Jackets', 64));
        this.multiSelectionItems.push(new MultiSelectionItem('Coach Jackets', 451));
        this.multiSelectionItems.push(new MultiSelectionItem('Denim Jackets', 89));
        this.multiSelectionItems.push(new MultiSelectionItem('Overcoats', 154));
        this.multiSelectionItems.push(new MultiSelectionItem('Parkas', 547));
    };
    FilterMultiSelectionPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad FilterMultiSelectionPage');
    };
    FilterMultiSelectionPage.prototype.selectAll = function () {
        this.multiSelectionItems.forEach(function (item) {
            item.isSelected = true;
        });
        this.setAnySelection();
    };
    FilterMultiSelectionPage.prototype.clearAll = function () {
        this.multiSelectionItems.forEach(function (item) {
            item.isSelected = false;
        });
        this.setAnySelection();
    };
    FilterMultiSelectionPage.prototype.selectUnselectedItem = function (item) {
        item.isSelected = !item.isSelected;
        this.setAnySelection();
    };
    FilterMultiSelectionPage.prototype.setAnySelection = function () {
        var hasAnySelected = false;
        for (var i = 0; i < this.multiSelectionItems.length; i++) {
            var item = this.multiSelectionItems[i];
            if (item.isSelected) {
                hasAnySelected = true;
                break;
            }
        }
        this.anySelection = hasAnySelected;
    };
    FilterMultiSelectionPage.prototype.closeMultiSelection = function () {
        this.viewController.dismiss();
    };
    FilterMultiSelectionPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-filter-multi-selection',template:/*ion-inline-start:"C:\Users\domin\Documents\myacccim\myACCCIM App\asos-user\src\pages\filter-multi-selection\filter-multi-selection.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <ion-title>\n\n      <p>{{pageTitle}}</p>\n\n    </ion-title>\n\n\n\n    <ion-buttons right>\n\n      <button *ngIf="!anySelection" padding ion-button icon-only outline (click)="selectAll()">\n\n        <ion-icon name="md-checkmark"></ion-icon>\n\n        <p>ALL</p>\n\n      </button>\n\n\n\n      <button *ngIf="anySelection" padding ion-button icon-only outline (click)="clearAll()">\n\n        <ion-icon name="md-close"></ion-icon>\n\n        <p>CLEAR</p>\n\n      </button>\n\n    </ion-buttons>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n  <ion-list>\n\n    <ion-item *ngFor="let item of multiSelectionItems" (click)="selectUnselectedItem(item)">\n\n      <ion-icon [class.isSelected]="!item.isSelected" item-start name="md-checkmark"></ion-icon>\n\n\n\n      <ion-row>\n\n        <p class="item-name">{{item.name}}</p>\n\n        <p class="item-count">({{item.count}})</p>\n\n      </ion-row>\n\n    </ion-item>\n\n  </ion-list>\n\n</ion-content>\n\n\n\n<ion-footer padding>\n\n  <button ion-button full (click)="closeMultiSelection()">\n\n    <p>DONE</p>\n\n  </button>\n\n</ion-footer>'/*ion-inline-end:"C:\Users\domin\Documents\myacccim\myACCCIM App\asos-user\src\pages\filter-multi-selection\filter-multi-selection.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* ViewController */]])
    ], FilterMultiSelectionPage);
    return FilterMultiSelectionPage;
}());

var MultiSelectionItem = /** @class */ (function () {
    function MultiSelectionItem(name, count) {
        this.name = name;
        this.count = count;
    }
    return MultiSelectionItem;
}());

//# sourceMappingURL=filter-multi-selection.js.map

/***/ })

});
//# sourceMappingURL=2.js.map