webpackJsonp([1],{

/***/ 709:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FilterRangePageModule", function() { return FilterRangePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__filter_range__ = __webpack_require__(731);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var FilterRangePageModule = /** @class */ (function () {
    function FilterRangePageModule() {
    }
    FilterRangePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__filter_range__["a" /* FilterRangePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__filter_range__["a" /* FilterRangePage */]),
            ],
        })
    ], FilterRangePageModule);
    return FilterRangePageModule;
}());

//# sourceMappingURL=filter-range.module.js.map

/***/ }),

/***/ 731:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FilterRangePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var FilterRangePage = /** @class */ (function () {
    function FilterRangePage(navCtrl, navParams, viewController) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewController = viewController;
        this.priceRange = { lower: 20, upper: 500 };
        this.pageTitle = navParams.get('selectionType');
    }
    FilterRangePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad FilterRangePage');
    };
    FilterRangePage.prototype.reset = function () {
        this.priceRange = { lower: 20, upper: 500 };
    };
    FilterRangePage.prototype.closePriceRange = function () {
        this.viewController.dismiss();
    };
    FilterRangePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-filter-range',template:/*ion-inline-start:"C:\Users\domin\Documents\myacccim\myACCCIM App\asos-user\src\pages\filter-range\filter-range.html"*/'<ion-header>\n\n    <ion-navbar>\n\n      <ion-title>\n\n        <p>{{pageTitle}}</p>\n\n      </ion-title>\n\n  \n\n      <ion-buttons right>  \n\n        <button padding ion-button icon-only outline (click)="reset()">\n\n          <p>RESET</p>\n\n        </button>\n\n      </ion-buttons>\n\n    </ion-navbar>\n\n  </ion-header>\n\n  \n\n  <ion-content padding>\n\n    <ion-range dualKnobs="true" pin="true" [(ngModel)]="priceRange" color="dark" min="20" max="500">\n\n      <p range-left>$20</p>\n\n      <p range-right>$500</p>\n\n    </ion-range>\n\n  </ion-content>\n\n  \n\n  <ion-footer padding>\n\n    <button ion-button full (click)="closePriceRange()">\n\n      <p>DONE</p>\n\n    </button>\n\n  </ion-footer>'/*ion-inline-end:"C:\Users\domin\Documents\myacccim\myACCCIM App\asos-user\src\pages\filter-range\filter-range.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* ViewController */]])
    ], FilterRangePage);
    return FilterRangePage;
}());

//# sourceMappingURL=filter-range.js.map

/***/ })

});
//# sourceMappingURL=1.js.map