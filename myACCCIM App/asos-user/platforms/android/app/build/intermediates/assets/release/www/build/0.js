webpackJsonp([0],{

/***/ 718:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductsPageModule", function() { return ProductsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__products__ = __webpack_require__(732);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ProductsPageModule = /** @class */ (function () {
    function ProductsPageModule() {
    }
    ProductsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__products__["a" /* ProductsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__products__["a" /* ProductsPage */]),
            ],
        })
    ], ProductsPageModule);
    return ProductsPageModule;
}());

//# sourceMappingURL=products.module.js.map

/***/ }),

/***/ 732:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProductsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__recommended_pop_over_recommended_pop_over__ = __webpack_require__(371);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__filter_filter__ = __webpack_require__(370);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ProductsPage = /** @class */ (function () {
    function ProductsPage(navCtrl, navParams, popoverCtrl, modalCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.popoverCtrl = popoverCtrl;
        this.modalCtrl = modalCtrl;
        var category = navParams.get('category');
        if (category) {
            this.pageTitle = category.name;
        }
        this.isMenSelected = navParams.get('isMenSelected');
        console.log(navParams);
    }
    ProductsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ProductsPage');
    };
    ProductsPage.prototype.goToSavedItems = function () {
        this.navCtrl.push('SavedItemsPage');
    };
    ProductsPage.prototype.showPopOver = function (myEvent) {
        var popover = this.popoverCtrl.create(__WEBPACK_IMPORTED_MODULE_2__recommended_pop_over_recommended_pop_over__["a" /* RecommendedPopOverPage */]);
        popover.present({
            ev: myEvent
        });
    };
    ProductsPage.prototype.showFilter = function () {
        var profileModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_3__filter_filter__["a" /* FilterPage */]);
        profileModal.present();
    };
    ProductsPage.prototype.goToProductDetails = function () {
        this.navCtrl.push('ProductDetailsPage', { isMenSelected: this.isMenSelected });
    };
    ProductsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-products',template:/*ion-inline-start:"C:\Users\domin\Documents\myacccim\myACCCIM App\asos-user\src\pages\products\products.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <ion-title>\n\n      <p>{{pageTitle}}</p>\n\n    </ion-title>\n\n\n\n    <ion-buttons right>\n\n      <button class="navbar-button" ion-button icon-only (click)="goToSavedItems()">\n\n        <ion-icon name="md-heart-outline"></ion-icon>\n\n      </button>\n\n\n\n      <button class="navbar-button" ion-button icon-only>\n\n        <ion-icon name="md-search"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n  <ion-item no-lines>\n\n    <button item-start class="recommended-button" ion-button clear (click)="showPopOver($event)">\n\n      <p>RECOMMENDED</p>\n\n      <ion-icon name="ios-arrow-down"></ion-icon>\n\n    </button>\n\n\n\n    <button item-end class="filter-button" ion-button clear (click)="showFilter()">\n\n      <p>FILTER</p>\n\n    </button>\n\n  </ion-item>\n\n\n\n  <ion-row>\n\n    <ion-col text-center>\n\n      <p class="items-found">200 items found</p>\n\n    </ion-col>\n\n  </ion-row>\n\n\n\n  <ion-grid *ngIf="isMenSelected">\n\n    <ion-row>\n\n      <ion-col>\n\n        <ion-row class="product-row">\n\n          <img class="product-image" (click)="goToProductDetails()" src="https://res.cloudinary.com/cediim8/image/upload/v1523344614/product-1.jpg">\n\n\n\n          <ion-item no-lines>\n\n            <p class="product-price" item-start>$40.00</p>\n\n\n\n            <button class="product-like" item-end ion-button clear icon-only>\n\n              <ion-icon name="md-heart-outline"></ion-icon>\n\n            </button>\n\n          </ion-item>\n\n\n\n          <p class="product-brand-description">Pull&Bear Denim Jacket In Black</p>\n\n        </ion-row>\n\n      </ion-col>\n\n\n\n      <ion-col>\n\n        <ion-row class="product-row">\n\n          <img class="product-image" (click)="goToProductDetails()" src="https://res.cloudinary.com/cediim8/image/upload/v1523344614/product-2.jpg">\n\n\n\n          <ion-item no-lines>\n\n            <p class="product-price" item-start>$89.00</p>\n\n\n\n            <button class="product-like" item-end ion-button clear icon-only>\n\n              <ion-icon name="md-heart-outline"></ion-icon>\n\n            </button>\n\n          </ion-item>\n\n\n\n          <p class="product-brand-description">Liquor N Poker Oversized Denim Jacket Stonewas</p>\n\n        </ion-row>\n\n      </ion-col>\n\n    </ion-row>\n\n\n\n    <ion-row>\n\n      <ion-col>\n\n        <ion-row class="product-row">\n\n          <img class="product-image" (click)="goToProductDetails()" src="https://res.cloudinary.com/cediim8/image/upload/v1523344614/product-9.jpg">\n\n\n\n          <ion-item no-lines>\n\n            <p class="product-price" item-start>$110.00</p>\n\n\n\n            <button class="product-like" item-end ion-button clear icon-only>\n\n              <ion-icon name="md-heart-outline"></ion-icon>\n\n            </button>\n\n          </ion-item>\n\n\n\n          <p class="product-brand-description">Puma T7 Vintage Track Jacket In White 57498506</p>\n\n        </ion-row>\n\n      </ion-col>\n\n\n\n      <ion-col>\n\n        <ion-row class="product-row">\n\n          <img class="product-image" (click)="goToProductDetails()" src="https://res.cloudinary.com/cediim8/image/upload/v1523344614/product-10.jpg">\n\n\n\n          <ion-item no-lines>\n\n            <p class="product-price" item-start>$40.00</p>\n\n\n\n            <button class="product-like" item-end ion-button clear icon-only>\n\n              <ion-icon name="md-heart-outline"></ion-icon>\n\n            </button>\n\n          </ion-item>\n\n\n\n          <p class="product-brand-description">Pull&Bear Bomber Jacket In Green</p>\n\n        </ion-row>\n\n      </ion-col>\n\n    </ion-row>\n\n\n\n    <ion-row>\n\n      <ion-col>\n\n        <ion-row class="product-row">\n\n          <img class="product-image" (click)="goToProductDetails()" src="https://res.cloudinary.com/cediim8/image/upload/v1523344614/product-3.jpg">\n\n\n\n          <ion-item no-lines>\n\n            <p class="product-price" item-start>$105.00</p>\n\n\n\n            <button class="product-like" item-end ion-button clear icon-only>\n\n              <ion-icon name="md-heart-outline"></ion-icon>\n\n            </button>\n\n          </ion-item>\n\n\n\n          <p class="product-brand-description">Cheap Monday Upszied Black Denim Jacket</p>\n\n        </ion-row>\n\n      </ion-col>\n\n\n\n      <ion-col>\n\n        <ion-row class="product-row">\n\n          <img class="product-image" (click)="goToProductDetails()" src="https://res.cloudinary.com/cediim8/image/upload/v1523344614/product-4.jpg">\n\n\n\n          <ion-item no-lines>\n\n            <p class="product-price" item-start>$218.00</p>\n\n\n\n            <button class="product-like" item-end ion-button clear icon-only>\n\n              <ion-icon name="md-heart-outline"></ion-icon>\n\n            </button>\n\n          </ion-item>\n\n\n\n          <p class="product-brand-description">Lee Sherpa Rider Denim Jacket Mid Wash</p>\n\n        </ion-row>\n\n      </ion-col>\n\n    </ion-row>\n\n\n\n    <ion-row>\n\n      <ion-col>\n\n        <ion-row class="product-row">\n\n          <img class="product-image" (click)="goToProductDetails()" src="https://res.cloudinary.com/cediim8/image/upload/v1523344614/product-5.jpg">\n\n\n\n          <ion-item no-lines>\n\n            <p class="product-price" item-start>$99.00</p>\n\n\n\n            <button class="product-like" item-end ion-button clear icon-only>\n\n              <ion-icon name="md-heart-outline"></ion-icon>\n\n            </button>\n\n          </ion-item>\n\n\n\n          <p class="product-brand-description">Kappa Poly Tricot Track Jacket With Banda Tapin</p>\n\n        </ion-row>\n\n      </ion-col>\n\n\n\n      <ion-col>\n\n        <ion-row class="product-row">\n\n          <img class="product-image" (click)="goToProductDetails()" src="https://res.cloudinary.com/cediim8/image/upload/v1523344614/product-6.jpg">\n\n\n\n          <ion-item no-lines>\n\n            <p class="product-price" item-start>$111.00</p>\n\n\n\n            <button class="product-like" item-end ion-button clear icon-only>\n\n              <ion-icon name="md-heart-outline"></ion-icon>\n\n            </button>\n\n          </ion-item>\n\n\n\n          <p class="product-brand-description">Champion Track Jacket With Taping In Navy</p>\n\n        </ion-row>\n\n      </ion-col>\n\n    </ion-row>\n\n\n\n    <ion-row>\n\n      <ion-col>\n\n        <ion-row class="product-row">\n\n          <img class="product-image" (click)="goToProductDetails()" src="https://res.cloudinary.com/cediim8/image/upload/v1523344614/product-7.jpg">\n\n\n\n          <ion-item no-lines>\n\n            <p class="product-price" item-start>$111.00</p>\n\n\n\n            <button class="product-like" item-end ion-button clear icon-only>\n\n              <ion-icon name="md-heart-outline"></ion-icon>\n\n            </button>\n\n          </ion-item>\n\n\n\n          <p class="product-brand-description">Champion Track Jacket With Taping In Purple</p>\n\n        </ion-row>\n\n      </ion-col>\n\n\n\n      <ion-col>\n\n        <ion-row class="product-row">\n\n          <img class="product-image" (click)="goToProductDetails()" src="https://res.cloudinary.com/cediim8/image/upload/v1523344614/product-8.jpg">\n\n\n\n          <ion-item no-lines>\n\n            <p class="product-price" item-start>$50.00</p>\n\n\n\n            <button class="product-like" item-end ion-button clear icon-only>\n\n              <ion-icon name="md-heart-outline"></ion-icon>\n\n            </button>\n\n          </ion-item>\n\n\n\n          <p class="product-brand-description">New Look Cotton Bomber Jacket In Burgundy</p>\n\n        </ion-row>\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-grid>\n\n\n\n  <ion-grid *ngIf="!isMenSelected">\n\n    <ion-row>\n\n      <ion-col>\n\n        <ion-row class="product-row">\n\n          <img class="product-image" (click)="goToProductDetails()" src="https://res.cloudinary.com/cediim8/image/upload/v1523415018/women-product-1.jpg">\n\n\n\n          <ion-item no-lines>\n\n            <p class="product-price" item-start>$40.00</p>\n\n\n\n            <button class="product-like" item-end ion-button clear icon-only>\n\n              <ion-icon name="md-heart-outline"></ion-icon>\n\n            </button>\n\n          </ion-item>\n\n\n\n          <p class="product-brand-description">ASOS Cotton Mini Shirt Dress</p>\n\n        </ion-row>\n\n      </ion-col>\n\n\n\n      <ion-col>\n\n        <ion-row class="product-row">\n\n          <img class="product-image" (click)="goToProductDetails()" src="https://res.cloudinary.com/cediim8/image/upload/v1523415018/women-product-2.jpg">\n\n\n\n          <ion-item no-lines>\n\n            <p class="product-price" item-start>$40.00</p>\n\n\n\n            <button class="product-like" item-end ion-button clear icon-only>\n\n              <ion-icon name="md-heart-outline"></ion-icon>\n\n            </button>\n\n          </ion-item>\n\n\n\n          <p class="product-brand-description">Stradivarius Polka Dot Shirt Dress</p>\n\n        </ion-row>\n\n      </ion-col>\n\n    </ion-row>\n\n\n\n    <ion-row>\n\n      <ion-col>\n\n        <ion-row class="product-row">\n\n          <img class="product-image" (click)="goToProductDetails()" src="https://res.cloudinary.com/cediim8/image/upload/v1523415018/women-product-3.jpg">\n\n\n\n          <ion-item no-lines>\n\n            <p class="product-price" item-start>$30.00</p>\n\n\n\n            <button class="product-like" item-end ion-button clear icon-only>\n\n              <ion-icon name="md-heart-outline"></ion-icon>\n\n            </button>\n\n          </ion-item>\n\n\n\n          <p class="product-brand-description">ASOS Ultimate Rolled Sleeve T-Shirt Dress With Tab</p>\n\n        </ion-row>\n\n      </ion-col>\n\n\n\n      <ion-col>\n\n        <ion-row class="product-row">\n\n          <img class="product-image" (click)="goToProductDetails()" src="https://res.cloudinary.com/cediim8/image/upload/v1523415018/women-product-4.jpg">\n\n\n\n          <ion-item no-lines>\n\n            <p class="product-price" item-start>$40.00</p>\n\n\n\n            <button class="product-like" item-end ion-button clear icon-only>\n\n              <ion-icon name="md-heart-outline"></ion-icon>\n\n            </button>\n\n          </ion-item>\n\n\n\n          <p class="product-brand-description">Boohoo One Shoulder Floral Midi Dress</p>\n\n        </ion-row>\n\n      </ion-col>\n\n    </ion-row>\n\n\n\n    <ion-row>\n\n      <ion-col>\n\n        <ion-row class="product-row">\n\n          <img class="product-image" (click)="goToProductDetails()" src="https://res.cloudinary.com/cediim8/image/upload/v1523415018/women-product-5.jpg">\n\n\n\n          <ion-item no-lines>\n\n            <p class="product-price" item-start>$44.00</p>\n\n\n\n            <button class="product-like" item-end ion-button clear icon-only>\n\n              <ion-icon name="md-heart-outline"></ion-icon>\n\n            </button>\n\n          </ion-item>\n\n\n\n          <p class="product-brand-description">Boohoo Off Shoulder Lemon Print Dress</p>\n\n        </ion-row>\n\n      </ion-col>\n\n\n\n      <ion-col>\n\n        <ion-row class="product-row">\n\n          <img class="product-image" (click)="goToProductDetails()" src="https://res.cloudinary.com/cediim8/image/upload/v1523415017/women-product-6.jpg">\n\n\n\n          <ion-item no-lines>\n\n            <p class="product-price" item-start>$168.00</p>\n\n\n\n            <button class="product-like" item-end ion-button clear icon-only>\n\n              <ion-icon name="md-heart-outline"></ion-icon>\n\n            </button>\n\n          </ion-item>\n\n\n\n          <p class="product-brand-description">ASOS Embellished Cluster Crop Top Tulle Midi Dress</p>\n\n        </ion-row>\n\n      </ion-col>\n\n    </ion-row>\n\n\n\n    <ion-row>\n\n      <ion-col>\n\n        <ion-row class="product-row">\n\n          <img class="product-image" (click)="goToProductDetails()" src="https://res.cloudinary.com/cediim8/image/upload/v1523415017/women-product-7.jpg">\n\n\n\n          <ion-item no-lines>\n\n            <p class="product-price" item-start>$135.00</p>\n\n\n\n            <button class="product-like" item-end ion-button clear icon-only>\n\n              <ion-icon name="md-heart-outline"></ion-icon>\n\n            </button>\n\n          </ion-item>\n\n\n\n          <p class="product-brand-description">ASOS DESIGN Sheer & Solid Printed Midi Dress In Floral Print</p>\n\n        </ion-row>\n\n      </ion-col>\n\n\n\n      <ion-col>\n\n        <ion-row class="product-row">\n\n          <img class="product-image" (click)="goToProductDetails()" src="https://res.cloudinary.com/cediim8/image/upload/v1523415017/women-product-8.jpg">\n\n\n\n          <ion-item no-lines>\n\n            <p class="product-price" item-start>$109.00</p>\n\n\n\n            <button class="product-like" item-end ion-button clear icon-only>\n\n              <ion-icon name="md-heart-outline"></ion-icon>\n\n            </button>\n\n          </ion-item>\n\n\n\n          <p class="product-brand-description">ASOS Embroidered Mini Drop Waist Dress</p>\n\n        </ion-row>\n\n      </ion-col>\n\n    </ion-row>\n\n\n\n    <ion-row>\n\n      <ion-col>\n\n        <ion-row class="product-row">\n\n          <img class="product-image" (click)="goToProductDetails()" src="https://res.cloudinary.com/cediim8/image/upload/v1523415289/women-product-9.jpg">\n\n\n\n          <ion-item no-lines>\n\n            <p class="product-price" item-start>$36.00</p>\n\n\n\n            <button class="product-like" item-end ion-button clear icon-only>\n\n              <ion-icon name="md-heart-outline"></ion-icon>\n\n            </button>\n\n          </ion-item>\n\n\n\n          <p class="product-brand-description">Stradivarius Short Sleeve Stripe Dress</p>\n\n        </ion-row>\n\n      </ion-col>\n\n\n\n      <ion-col>\n\n        <ion-row class="product-row">\n\n          <img class="product-image" (click)="goToProductDetails()" src="https://res.cloudinary.com/cediim8/image/upload/v1523415289/women-product-10.jpg">\n\n\n\n          <ion-item no-lines>\n\n            <p class="product-price" item-start>$91.00</p>\n\n\n\n            <button class="product-like" item-end ion-button clear icon-only>\n\n              <ion-icon name="md-heart-outline"></ion-icon>\n\n            </button>\n\n          </ion-item>\n\n\n\n          <p class="product-brand-description">River Island Tie Waist Wrap Front Ruffle Mini Dress</p>\n\n        </ion-row>\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-grid>\n\n</ion-content>'/*ion-inline-end:"C:\Users\domin\Documents\myacccim\myACCCIM App\asos-user\src\pages\products\products.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* PopoverController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ModalController */]])
    ], ProductsPage);
    return ProductsPage;
}());

//# sourceMappingURL=products.js.map

/***/ })

});
//# sourceMappingURL=0.js.map