webpackJsonp([3],{

/***/ 729:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CategoryPageModule", function() { return CategoryPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__category__ = __webpack_require__(733);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var CategoryPageModule = /** @class */ (function () {
    function CategoryPageModule() {
    }
    CategoryPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__category__["a" /* CategoryPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__category__["a" /* CategoryPage */]),
            ],
        })
    ], CategoryPageModule);
    return CategoryPageModule;
}());

//# sourceMappingURL=category.module.js.map

/***/ }),

/***/ 733:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CategoryPage; });
/* unused harmony export Category */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var CategoryPage = /** @class */ (function () {
    function CategoryPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.products = [];
        this.ranges = [];
        this.occasions = [];
        this.pageTitle = navParams.get('category');
        this.isMenSelected = navParams.get('isMenSelected');
        this.initialiseProducts();
        this.initialiseRanges();
        this.initialiseOccasions();
    }
    CategoryPage.prototype.initialiseProducts = function () {
        if (this.isMenSelected) {
            this.products.push(new Category('New In', 'https://res.cloudinary.com/cediim8/image/upload/v1523343800/new-in.jpg'));
            this.products.push(new Category('Hoodies & Sweatshirts', 'https://res.cloudinary.com/cediim8/image/upload/v1523343799/hoodies-sweatshirts.jpg'));
            this.products.push(new Category('Jackets and Coats', 'https://res.cloudinary.com/cediim8/image/upload/v1523343799/jackets-coats.jpg'));
            this.products.push(new Category('Jeans', 'https://res.cloudinary.com/cediim8/image/upload/v1523343799/jeans.jpg'));
            this.products.push(new Category('Jumpers & Cardigans', 'https://res.cloudinary.com/cediim8/image/upload/v1523343799/jumper-cardigans.jpg'));
            this.products.push(new Category('Loungewear', 'https://res.cloudinary.com/cediim8/image/upload/v1523343799/lounge-waer.jpg'));
            this.products.push(new Category('Multipacks', 'https://res.cloudinary.com/cediim8/image/upload/v1523343799/myltipacks.jpg'));
            this.products.push(new Category('Pants & Chinos', 'https://res.cloudinary.com/cediim8/image/upload/v1523343800/pants-chinos.jpg'));
            this.products.push(new Category('Shirts', 'https://res.cloudinary.com/cediim8/image/upload/v1523343800/shirts.jpg'));
            this.products.push(new Category('Shorts', 'https://res.cloudinary.com/cediim8/image/upload/v1523343800/shorts.jpg'));
            this.products.push(new Category('Socks', 'https://res.cloudinary.com/cediim8/image/upload/v1523343800/socks.jpg'));
            this.products.push(new Category('Suits', 'https://res.cloudinary.com/cediim8/image/upload/v1523343800/sutis.jpg'));
            this.products.push(new Category('Swimwear', 'https://res.cloudinary.com/cediim8/image/upload/v1523343800/swimwear.jpg'));
            this.products.push(new Category('T-Shirts & Singlets', 'https://res.cloudinary.com/cediim8/image/upload/v1523343801/t-shirts-singlets.jpg'));
            this.products.push(new Category('Tracksuits', 'https://res.cloudinary.com/cediim8/image/upload/v1523343800/tracksuits.jpg'));
            this.products.push(new Category('Underwear', 'https://res.cloudinary.com/cediim8/image/upload/v1523343800/underwear.jpg'));
        }
        else {
            this.products.push(new Category('New In', 'https://res.cloudinary.com/cediim8/image/upload/v1523414035/new-in-women-caetgory.jpg'));
            this.products.push(new Category('Coats and Jackets', 'https://res.cloudinary.com/cediim8/image/upload/v1523414035/coats-jackets-women.jpg'));
            this.products.push(new Category('Dresses', 'https://res.cloudinary.com/cediim8/image/upload/v1523414035/dresses-women.jpg'));
            this.products.push(new Category('Hoodies & Sweatshirts', 'https://res.cloudinary.com/cediim8/image/upload/v1523414035/hoodies-sweatshirt-women.jpg'));
            this.products.push(new Category('Jeans', 'https://res.cloudinary.com/cediim8/image/upload/v1523414035/jeans-women.jpg'));
            this.products.push(new Category('Jumpers & Cardigans', 'https://res.cloudinary.com/cediim8/image/upload/v1523414034/jumpers-cardigans-women.jpg'));
            this.products.push(new Category('Jumpsuits & Playsuits', 'https://res.cloudinary.com/cediim8/image/upload/v1523414034/jumpsuit-playsuit-women.jpg'));
            this.products.push(new Category('Lingerie & Nightwear', 'https://res.cloudinary.com/cediim8/image/upload/v1523414034/lingerie-nightwear.jpg'));
            this.products.push(new Category('Loungewear', 'https://res.cloudinary.com/cediim8/image/upload/v1523414034/loungewear-women.jpg'));
            this.products.push(new Category('Multipacks', 'https://res.cloudinary.com/cediim8/image/upload/v1523414034/multipacks-women.jpg'));
            this.products.push(new Category('Pants & Leggings', 'https://res.cloudinary.com/cediim8/image/upload/v1523414034/pants-leggins-women.jpg'));
            this.products.push(new Category('Shorts', 'https://res.cloudinary.com/cediim8/image/upload/v1523414034/shorts-women.jpg'));
            this.products.push(new Category('Skirts', 'https://res.cloudinary.com/cediim8/image/upload/v1523414034/skirts-women.jpg'));
            this.products.push(new Category('Socks & Tights', 'https://res.cloudinary.com/cediim8/image/upload/v1523414034/socks-tights-women.jpg'));
            this.products.push(new Category('Suits & Separates', 'https://res.cloudinary.com/cediim8/image/upload/v1523414034/suits-separates-women.jpg'));
            this.products.push(new Category('Swimwear & Beachwear', 'https://res.cloudinary.com/cediim8/image/upload/v1523414034/swimwear-women.jpg'));
            this.products.push(new Category('Tops', 'https://res.cloudinary.com/cediim8/image/upload/v1523414034/tops-women.jpg'));
        }
    };
    CategoryPage.prototype.initialiseRanges = function () {
        if (this.isMenSelected) {
            this.ranges.push(new Category('Plus Size', 'https://res.cloudinary.com/cediim8/image/upload/v1523344208/plus-size.jpg'));
            this.ranges.push(new Category('Tall', 'https://res.cloudinary.com/cediim8/image/upload/v1523344207/tall-size.jpg'));
        }
        else {
            this.ranges.push(new Category('Curve & Plus Size', 'https://res.cloudinary.com/cediim8/image/upload/v1523414573/plus-size-women.jpg'));
            this.ranges.push(new Category('Maternity', 'https://res.cloudinary.com/cediim8/image/upload/v1523414573/meternity.jpg'));
            this.ranges.push(new Category('Petite', 'https://res.cloudinary.com/cediim8/image/upload/v1523414573/petite.jpg'));
            this.ranges.push(new Category('Tall', 'https://res.cloudinary.com/cediim8/image/upload/v1523414573/tall-women.jpg'));
        }
    };
    CategoryPage.prototype.initialiseOccasions = function () {
        if (this.isMenSelected) {
            this.occasions.push(new Category('AUTUMN', 'https://res.cloudinary.com/cediim8/image/upload/v1523344207/autum.jpg'));
            this.occasions.push(new Category('WEDDINGS', 'https://res.cloudinary.com/cediim8/image/upload/v1523344207/wedding.jpg'));
            this.occasions.push(new Category('WORKWEAR', 'https://res.cloudinary.com/cediim8/image/upload/v1523344207/workwear.jpg'));
            this.occasions.push(new Category('GOING OUT-OUT', 'https://res.cloudinary.com/cediim8/image/upload/v1523344207/goingout.jpg'));
            this.occasions.push(new Category('HOLIDAY', 'https://res.cloudinary.com/cediim8/image/upload/v1523344207/holiday.jpg'));
            this.occasions.push(new Category('FESTIVAL', 'https://res.cloudinary.com/cediim8/image/upload/v1523344207/festival.jpg'));
        }
        else {
            this.occasions.push(new Category('AUTUMN', 'https://res.cloudinary.com/cediim8/image/upload/v1523414572/autumn-women.jpg'));
            this.occasions.push(new Category('WEDDINGS', 'https://res.cloudinary.com/cediim8/image/upload/v1523414572/wedding-women.jpg'));
            this.occasions.push(new Category('WORKWEAR', 'https://res.cloudinary.com/cediim8/image/upload/v1523414572/workwear-women.jpg'));
            this.occasions.push(new Category('GOING OUT-OUT', 'https://res.cloudinary.com/cediim8/image/upload/v1523414572/going-out-women.jpg'));
            this.occasions.push(new Category('HOLIDAY', 'https://res.cloudinary.com/cediim8/image/upload/v1523414572/holiday-women.jpg'));
            this.occasions.push(new Category('FESTIVAL', 'https://res.cloudinary.com/cediim8/image/upload/v1523414572/festival-women.jpg'));
        }
    };
    CategoryPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CategoryPage');
    };
    CategoryPage.prototype.goToSavedItems = function () {
        this.navCtrl.push('SavedItemsPage');
    };
    CategoryPage.prototype.goToProducts = function (category) {
        var navParams = {
            category: category,
            isMenSelected: this.isMenSelected
        };
        this.navCtrl.push('ProductsPage', navParams);
    };
    CategoryPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-category',template:/*ion-inline-start:"C:\Users\domin\Documents\myacccim\myACCCIM App\asos-user\src\pages\category\category.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <ion-title>\n\n      <p>{{ pageTitle }}</p>\n\n    </ion-title>\n\n\n\n    <ion-buttons right>\n\n      <button class="navbar-button" ion-button icon-only (click)="goToSavedItems()">\n\n        <ion-icon name="md-heart-outline"></ion-icon>\n\n      </button>\n\n\n\n      <button class="navbar-button" ion-button icon-only>\n\n        <ion-icon name="md-search"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n  <ion-row>\n\n    <p class="shop-title">SHOP BY PRODUCT</p>\n\n\n\n    <ion-card class="shopping-category-card">\n\n      <ion-list>\n\n        <ion-item *ngFor="let product of products" (click)="goToProducts(product)">\n\n          <ion-avatar item-start>\n\n            <img src="{{ product.picture }}">\n\n          </ion-avatar>\n\n\n\n          <p>{{ product.name }}</p>\n\n        </ion-item>\n\n      </ion-list>\n\n    </ion-card>\n\n  </ion-row>\n\n\n\n  <ion-row>\n\n    <p class="shop-title">SHOP BY RANGE</p>\n\n\n\n    <ion-card class="shopping-category-card">\n\n      <ion-list>\n\n        <ion-item *ngFor="let range of ranges" (click)="goToProducts(product)">\n\n          <ion-avatar item-start>\n\n            <img src="{{ range.picture }}">\n\n          </ion-avatar>\n\n\n\n          <p>{{ range.name }}</p>\n\n        </ion-item>\n\n      </ion-list>\n\n    </ion-card>\n\n  </ion-row>\n\n\n\n  <ion-row>\n\n    <p class="shop-title">SHOP BY OCCASION</p>\n\n\n\n    <ion-card class="occasions-card">\n\n      <ion-list>\n\n        <ion-card *ngFor="let occasion of occasions">\n\n          <ion-item>\n\n            <p item-start>{{ occasion.name }}</p>\n\n\n\n            <ion-avatar item-end>\n\n              <img src="{{ occasion.picture }}">\n\n            </ion-avatar>\n\n          </ion-item>\n\n        </ion-card>\n\n      </ion-list>\n\n    </ion-card>\n\n  </ion-row>\n\n</ion-content>'/*ion-inline-end:"C:\Users\domin\Documents\myacccim\myACCCIM App\asos-user\src\pages\category\category.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */]])
    ], CategoryPage);
    return CategoryPage;
}());

var Category = /** @class */ (function () {
    function Category(name, picture) {
        this.name = name;
        this.picture = picture;
    }
    return Category;
}());

//# sourceMappingURL=category.js.map

/***/ })

});
//# sourceMappingURL=3.js.map