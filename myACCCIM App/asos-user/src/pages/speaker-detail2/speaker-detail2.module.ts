import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SpeakerDetail2Page } from './speaker-detail2';

@NgModule({
  declarations: [
    SpeakerDetail2Page,
  ],
  imports: [
    IonicPageModule.forChild(SpeakerDetail2Page),
  ],
})
export class SpeakerDetail2PageModule {}
