import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SpeakerDetail5Page } from './speaker-detail5';

@NgModule({
  declarations: [
    SpeakerDetail5Page,
  ],
  imports: [
    IonicPageModule.forChild(SpeakerDetail5Page),
  ],
})
export class SpeakerDetail5PageModule {}
