import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SpeakerDetail9Page } from './speaker-detail9';

@NgModule({
  declarations: [
    SpeakerDetail9Page,
  ],
  imports: [
    IonicPageModule.forChild(SpeakerDetail9Page),
  ],
})
export class SpeakerDetail9PageModule {}
