import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { HttpClient } from '@angular/common/http';
import { Http,Response } from '@angular/http';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoginPage } from '../login/login';
import {Storage} from '@ionic/storage';
/**
 * Generated class for the ChangePasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-change-password',
  templateUrl: 'change-password.html',
})
export class ChangePasswordPage {

  oldpassword: any;
  newpassword: any;
  id: any;
  userCredential: any;
  ChangePasswordForm: FormGroup;
  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public formBuilder: FormBuilder,
              public ApiService: ApiServiceProvider,
              public alertCtrl: AlertController, 
              private loadingCtrl: LoadingController,
              public storage: Storage) {

                this.ChangePasswordForm = this.formBuilder.group({
                  'oldpassword': ['', Validators.compose([Validators.required])],
                  'newpassword': ['',Validators.compose([Validators.required])]
                });

                this.storage.get("userid").then((val) => {
                  this.id = val;});
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChangePasswordPage');
  }

  changepassword() 
  {
    const loading = this.loadingCtrl.create({
      spinner: null,
      duration: 5000,
      content: 'Please Wait...',
      //translucent: true,
      cssClass: 'custom-class custom-loading'
    });

    let alert = this.alertCtrl.create({
      title: 'Password Successfully Changed',
      subTitle: 'You will be signed out to sign in with your new password',
      buttons: [
        {
          text: "Ok",
          handler: () => {
            this.storage.clear(); 
            this.navCtrl.setRoot(LoginPage);
          }
        }
       
      ],
  
    });
    loading.present();
   

    this.ApiService.UpdatePassword(this.id,this.oldpassword,this.newpassword)
    .subscribe(result => {
      loading.dismiss(); 
      alert.present(); 
      
    });  
  }


  
}
