import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController,ModalController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { Http,Response } from '@angular/http';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

/**
 * Generated class for the PopupModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-popup-modal',
  templateUrl: 'popup-modal.html',
})
export class PopupModalPage {

  id : any;
  Diamond: any;

  
  constructor(public navCtrl: NavController, 
            public navParams: NavParams, public view: ViewController,
            public ApiService : ApiServiceProvider, private iab: InAppBrowser, public modal : ModalController) {

    this.id = navParams.get('data');
    this.DiamondSponsor();
  }

 

  DiamondSponsor()
  {
    this.ApiService.getSpecificAds(this.id).subscribe((res: Response) => {
      const getDiamondSponsor = res.json();
 
      this.Diamond = getDiamondSponsor;
      
    })
  }

  OpenLink(link)
  {
    this.iab.create(link, '_blank');
  }

  OpenLink2(link)
  {
    this.iab.create(link, '_blank');
  }

  OpenLink3(link)
  {
    this.iab.create(link, '_blank');
  }

 
  

}
