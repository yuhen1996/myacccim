import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SpeakerDetail10Page } from './speaker-detail10';

@NgModule({
  declarations: [
    SpeakerDetail10Page,
  ],
  imports: [
    IonicPageModule.forChild(SpeakerDetail10Page),
  ],
})
export class SpeakerDetail10PageModule {}
