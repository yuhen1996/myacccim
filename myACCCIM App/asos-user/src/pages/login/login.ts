import {Component} from "@angular/core";
import {NavController, AlertController, ToastController, MenuController, LoadingController, ViewController} from "ionic-angular";
import {HomePage} from "../home/home";
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Http, Response } from '@angular/http';
import { ApiServiceProvider } from "../../providers/api-service/api-service";
import {Storage} from "@ionic/storage";
@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {

  user:any;
  pass:any;
  result: any;
  result2: any;
  loginForm: FormGroup;
  private loading;

  constructor(public nav: NavController, 
    public forgotCtrl: AlertController, 
    public menu: MenuController, 
    public toastCtrl: ToastController,
    public formBuilder: FormBuilder,
    public ApiService: ApiServiceProvider,
    public alertCtrl: AlertController, 
    private loadingCtrl: LoadingController,
    public storage: Storage,
    public view : ViewController) {
    this.menu.swipeEnable(false);

    this.loginForm = this.formBuilder.group({
      'user': ['', Validators.compose([Validators.required])],
      'pass': ['',Validators.compose([Validators.required])]
    });
  }

  // closeLoginModal()
  // {
  //   this.view.dismiss();
  // }
  // login and go to home page
  login() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...',
      duration: 2000
    });
  
    loading.present();

   this.ApiService.userlogin(this.user, this.pass).subscribe(
      (res:Response) => {
        const checkLogin = res.json();
        this.result = checkLogin;
    
          let alert = this.alertCtrl.create({
            title: 'Successfully Signed In',
            subTitle: 'Welcome To MyACCCIM!',
            buttons: ['Ok']
          });
          alert.present();

          this.storage.set("userid", this.result);
         
          this.nav.setRoot(HomePage);

      }, 
      (err:Error) => {
          //console.log("Failed");
          let alert = this.alertCtrl.create({
            title: 'Sign In Failed',
            subTitle: 'ID or password is incorrect',
            buttons: ['Ok']
          });
          alert.present();
      }
   )
    
  }

  forgotPass() {
    let forgot = this.forgotCtrl.create({
      title: 'Forgot Password?',
      message: "Enter you email address to send a reset link password.",
      inputs: [
        {
          name: 'username',
          placeholder: 'username',
          type: 'username'
        },
        {
          name: 'email',
          placeholder: 'Email',
          type: 'email'
        },
        {
          name: 'new password',
          placeholder: 'new password',
          type: 'password'
        },
      ],

    
    
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Send',
          handler: data => {
            console.log('Send clicked');
            let toast = this.toastCtrl.create({
              message: 'Email was sended successfully',
              duration: 3000,
              position: 'top',
              cssClass: 'dark-trans',
              closeButtonText: 'OK',
              showCloseButton: true
            });
            toast.present();
          }
        }
      ]
    });
    forgot.present();
  }



}
