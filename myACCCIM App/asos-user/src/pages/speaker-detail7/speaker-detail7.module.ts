import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SpeakerDetail7Page } from './speaker-detail7';

@NgModule({
  declarations: [
    SpeakerDetail7Page,
  ],
  imports: [
    IonicPageModule.forChild(SpeakerDetail7Page),
  ],
})
export class SpeakerDetail7PageModule {}
