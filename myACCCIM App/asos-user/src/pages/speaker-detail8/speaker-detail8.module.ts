import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SpeakerDetail8Page } from './speaker-detail8';

@NgModule({
  declarations: [
    SpeakerDetail8Page,
  ],
  imports: [
    IonicPageModule.forChild(SpeakerDetail8Page),
  ],
})
export class SpeakerDetail8PageModule {}
