import { Component, OnInit } from '@angular/core';
import { NavController, PopoverController, Events, Form, AlertController, LoadingController,ModalController, MenuController, Loading } from 'ionic-angular';
import { MenWomenCategoryPopOverPage } from '../men-women-category-pop-over/men-women-category-pop-over';
import { CategoryPage, Category } from '../category/category';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { HttpClient } from '@angular/common/http';
import { Http,Response } from '@angular/http';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { ProductDetailsPage } from '../product-details/product-details';
import { _ParseAST } from '@angular/compiler';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import {Storage} from "@ionic/storage";
import { LoginPage } from '../login/login';
import { DomSanitizer } from '@angular/platform-browser';
import { AboutPage } from '../about/about';
import { ChangePasswordPage } from '../change-password/change-password';
import { Renderer } from '@angular/core';
import { SpeakerDetail1Page } from '../speaker-detail1/speaker-detail1';
import { SpeakerDetail2Page } from '../speaker-detail2/speaker-detail2';
import { SpeakerDetail3Page } from '../speaker-detail3/speaker-detail3';
import { SpeakerDetail5Page } from '../speaker-detail5/speaker-detail5';
import { SpeakerDetail4Page } from '../speaker-detail4/speaker-detail4';
import { SpeakerDetail6Page } from '../speaker-detail6/speaker-detail6';
import { SpeakerDetail10Page } from '../speaker-detail10/speaker-detail10';
import { SpeakerDetail9Page } from '../speaker-detail9/speaker-detail9';
import { SpeakerDetail8Page } from '../speaker-detail8/speaker-detail8';
import { SpeakerDetail7Page } from '../speaker-detail7/speaker-detail7';
import { PopupModalPage } from '../popup-modal/popup-modal';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  homeSegment: string = "home";
  isMenSelected: boolean = true;
  pageTitle: string = 'MEN';
  Diamond : any;
  Platinum: any;
  Gold: any;
  Silver: any;
  Supporting: any;
  Advertisement: any;
  gridresult:any;
  listviewresult: any;
  allviewresult:any;
  showEvent: any;
  gridlistview: any = "sponsors";
  eventinfo: any = "programme";
  
  showSpecificEvent: any;
  showPopularEvent: any;
  showSpecificAds: any;
  
  
  items: any; //initialize your items array empty

 
  griditems: any;
  listitems: any;
  allviewitems: any;

  id:any;
  userCredential: any;
  image:any;
  userislogin : boolean;
  usernotyetlogin: boolean;
  loginbutton: boolean;

  userisnotsignin:boolean;
  userissignin:boolean;
  username: any;

  platinumitems: any;
  silveritems : any;
  supportingitems : any;
  platinumresult:any;
  silverresult: any;
  supportingresult: any;
  golditems: any;
  goldresult:any;
  advertisementitems:any;
  advertisementresult: any;

  fakeadvertisementresult : Array<any> = new Array(10);
  fakeshowEvent: Array<any> = new Array(10);
  fakeplatinumresult: Array<any> = new Array(10);
  fakegoldresult: Array<any> = new Array(10);
  fakesilverresult: Array<any> = new Array(10);
  fakesupportingresult: Array<any> = new Array(20);
  fakeDiamond: Array<any> = new Array(1);
  fakePlatinum: Array<any> = new Array(1);
  fakeGold: Array<any> = new Array(1);
  fakeSilver: Array<any> = new Array(2);
  fakeAdvertisement: Array<any> = new Array(5);
  fakeshowPopularEvent: Array<any> = new Array(5);
  constructor(public navCtrl: NavController, 
    public popoverCtrl: PopoverController,
    public events: Events,
    public ApiService: ApiServiceProvider,
    public httpClient: HttpClient, 
    public https:Http,
    private iab: InAppBrowser,
    public storage: Storage,
    public alertCtrl: AlertController,
    public loadingController: LoadingController,
    private sanitizer: DomSanitizer,
    public renderer: Renderer,
    public modal: ModalController,
    public menu:MenuController) {
    
     
      this.menu.enable(false, 'myMenu');

      this.allRefresh(0);
      
   

    this.storage.get("userid").then((val) => {
      this.id = val;

      if(this.id === null)
      {
        this.userisnotsignin = true;
        this.userissignin = false;

      }

      else
      {
        this.ApiService.getUser(this.id)
        .subscribe((res: Response) =>
        {
          this.userCredential = res.json();
         
        });

        this.userisnotsignin = false;
        this.userissignin = true;
      }
    });
    
   
  }

  goToAbout() {
   this.navCtrl.push(AboutPage);
  }

  checkStorage() {
   
      this.storage.get("userid").then((val) => {
        this.id = val;

        if(this.id === null)
        {
          this.usernotyetlogin = true;
          this.loginbutton = true;
          this.userislogin = false;
         
        }

        else
        {
          this.ApiService.getUser(this.id)
          .subscribe((res: Response) =>
          {
            this.userCredential = res.json();
            
          });

          this.userislogin = true;
          this.loginbutton = false;
          this.usernotyetlogin = false;
        }
      });
  
  }
  signIn()
  {
    this.navCtrl.push(LoginPage);
    
  }

  goToChangePassword() {
    this.navCtrl.push(ChangePasswordPage);
  }
  

  allRefresh(refresher)
  {
    
    /*#region Home */
    setTimeout(() => {
    this.ApiService.getHomePageDiamond().subscribe((res: Response) => {
      const getDiamondSponsor = res.json();
 
      this.Diamond = getDiamondSponsor;
      
      if(refresher != 0)
         {
          setTimeout(() => {
            refresher.complete();
          }, 2000);
         }

        
         

         
    });
  },3000);

    setTimeout(() => {
    this.ApiService.getPlatinumSponsorSlider().subscribe((res: Response) => {
      const getPlatinumSponsor = res.json();
 
      this.Platinum = getPlatinumSponsor;
      if(refresher != 0)
         {
          setTimeout(() => {
            refresher.complete();
          }, 2000);
         }
      
    });
  },3000);

  setTimeout(() => {
    this.ApiService.getGoldSponsorSlider().subscribe((res: Response) => {
      const getGoldSponsor = res.json();
 
      this.Gold = getGoldSponsor;
      if(refresher != 0)
         {
          setTimeout(() => {
            refresher.complete();
          }, 2000);
         }
      
    });
  },3000);

  setTimeout(() => {
    this.ApiService.getSilverSponsorSlider().subscribe((res: Response) => {
      const getSilverSponsor = res.json();
 
      this.Silver = getSilverSponsor;
      if(refresher != 0)
         {
          setTimeout(() => {
            refresher.complete();
          }, 2000);
         }
      
    });
  },3000);

  setTimeout(() => {
    this.ApiService.getSupportingSponsorSlider().subscribe((res: Response) => {
      const getSupportingSponsor = res.json();
 
      this.Supporting = getSupportingSponsor;
      if(refresher != 0)
         {
          setTimeout(() => {
            refresher.complete();
          }, 2000);
         }
      
    });
  },3000);

  setTimeout(() => {
    this.ApiService.getAdvertisement().subscribe((res: Response) => {
      const getAdvertisementImg = res.json();
 
      this.Advertisement = getAdvertisementImg;
      if(refresher != 0)
         {
          setTimeout(() => {
            refresher.complete();
          }, 2000);
         }
      
    });
  },3000);

  setTimeout(() => {
    this.ApiService.popularevent()
      .subscribe((res:Response) => {
        const getPopularEvent = res.json();
        this.showPopularEvent = getPopularEvent;
        if(refresher != 0)
        {
          setTimeout(() => {
            refresher.complete();
          }, 2000);
        }
        
      });
    },3000);

      /*#endregion*/

      /*#region Sponsor */
      setTimeout(() => {
      this.ApiService.getPlatinumCategories()
      .subscribe((res: Response) =>{
        const platinum = res.json();
        this.platinumitems = platinum;
        this.initializePlatinumCategories();
        if(refresher != 0)
        {
         setTimeout(() => {
          refresher.complete();
        }, 2000);
        }
        
      });
    },3000);
    
    setTimeout(() => {
      this.ApiService.getGoldCategories()
      .subscribe((res: Response) =>{
        const gold = res.json();
        this.golditems = gold;
        this.initializeGoldCategories();
        if(refresher != 0)
        {
          setTimeout(() => {
            refresher.complete();
          }, 2000);
        }
        
      });
    },3000);
    
    setTimeout(() => {
      this.ApiService.getSilverCategories()
        .subscribe((res:Response) =>{
          const silver = res.json();
          this.silveritems = silver;
          this.initializeSilverCategories();
    
          if(refresher != 0)
        {
          setTimeout(() => {
            refresher.complete();
          }, 2000);
        }
        });
      },3000);
    
      setTimeout(() => {
        this.ApiService.getSupportingCategories()
        .subscribe((res:Response) => {
          const supporting = res.json();
          this.supportingitems = supporting;
          this.initializeSupportingCategories();
    
          if(refresher != 0)
        {
          setTimeout(() => {
            refresher.complete();
          }, 2000);
        }
        });
      },3000);
    
        setTimeout(() => {
        this.ApiService.getSponsorAdvertisement()
        .subscribe((res:Response) =>{
          const advertisement = res.json();
          this.advertisementitems = advertisement;
          this.initializeAdvertisementCategories();
    
          if(refresher != 0)
        {
          setTimeout(() => {
            refresher.complete();
          }, 2000);
        }
        });
      },3000);

        /*#endregion */

        /*#region Programme */
        setTimeout(() => {
          this.ApiService.getEvent().subscribe((res:Response) => {
            this.items = res.json();
            this.initializeEvent();
               if(refresher != 0)
               {
                setTimeout(() => {
                  refresher.complete();
                }, 2000);
               }
                  
                 
              
          });
  
        },5000);
        
        /*#endregion */

  }

  /*#region Programme Search */
  getEventTitle(evt:any)
  {
    this.initializeEvent();
    
    let serVal = evt.target.value;
    if(serVal && serVal.trim() != '')
    {
      
      this.showEvent = this.showEvent.filter((getevent) => {
        return(getevent.EventTitle.toLowerCase().indexOf(serVal.toLowerCase()) > -1);
       
      });
      this.renderer.invokeElementMethod(evt.target, 'blur');
      
    }
  }

  initializeEvent() {
    this.showEvent = this.items;
  }

  /*#endregion */

  /*#region Sponsor  */
  platinumCategories(plt:any)
  {
    this.initializePlatinumCategories();
    
    let serVal = plt.target.value;
    if(serVal && serVal.trim() != '')
    {
      
      this.platinumresult = this.platinumresult.filter((getplt) => {
        return(getplt.CompanyName.toLowerCase().indexOf(serVal.toLowerCase()) > -1);
      });

      this.renderer.invokeElementMethod(plt.target, 'blur');
      
    }
  }

  initializePlatinumCategories() {
    this.platinumresult = this.platinumitems;
  }

  
  goldCategories(gold:any)
  {
    this.initializeGoldCategories();
    
    let serVal = gold.target.value;
    if(serVal && serVal.trim() != '')
    {
      
      this.goldresult = this.goldresult.filter((getgold) => {
        return(getgold.CompanyName.toLowerCase().indexOf(serVal.toLowerCase()) > -1);
      });

      this.renderer.invokeElementMethod(gold.target, 'blur');
      
    }
  }

  initializeGoldCategories() {
    this.goldresult = this.golditems;
  }
  

  SilverCategories(sil:any)
  {
    this.initializeSilverCategories();
    
    let serVal = sil.target.value;
    if(serVal && serVal.trim() != '')
    {
      
      this.silverresult = this.silverresult.filter((getsil) => {
        return(getsil.CompanyName.toLowerCase().indexOf(serVal.toLowerCase()) > -1);
      });
      this.renderer.invokeElementMethod(sil.target, 'blur');
      
    }
  }

  initializeSilverCategories() {
    this.silverresult = this.silveritems;
  }

 

  SupportingCategories(sup:any)
  {
    this.initializeSupportingCategories();
    
    let serVal = sup.target.value;
    if(serVal && serVal.trim() != '')
    {
      
      this.supportingresult = this.supportingresult.filter((getsup) => {
        return(getsup.CompanyName.toLowerCase().indexOf(serVal.toLowerCase()) > -1);
      });
      this.renderer.invokeElementMethod(sup.target, 'blur');
      
    }
  }

  initializeSupportingCategories() {
    this.supportingresult = this.supportingitems;
  }

  advertisementCategories(ads:any)
  {
    this.initializeAdvertisementCategories();
    
    let serVal = ads.target.value;
    if(serVal && serVal.trim() != '')
    {
      
      this.advertisementresult = this.advertisementresult.filter((getads) => {
        return(getads.CompanyName.toLowerCase().indexOf(serVal.toLowerCase()) > -1);
      });

      this.renderer.invokeElementMethod(ads.target, 'blur');
      
    }
  }

  initializeAdvertisementCategories() {
    this.advertisementresult = this.advertisementitems;
  }



  
  OpenLink(link)
  {
    this.iab.create(link, '_blank');
  }

  OpenLink2(link)
  {
    this.iab.create(link, '_blank');
  }

  OpenLink3(link)
  {
    this.iab.create(link, '_blank');
  }


  

  gotoEvent(EventID){
    this.ApiService.getSpecificEvent(EventID).subscribe((res:Response) => {
      const getSpecificEvent = res.json();
      this.showSpecificEvent = getSpecificEvent;

      this.navCtrl.push(ProductDetailsPage,{
        data: EventID
        
      });
     
      
    })
    
  }

  PopUpAds(AdsID){
    this.ApiService.getSpecificAds(AdsID).subscribe((res:Response) => {
      const getSpecificAds = res.json();
      this.showSpecificAds = getSpecificAds;

      this.navCtrl.push(PopupModalPage, {
        data: AdsID
      });
      
      
      
    })
  }

  /*#region Speaker Detail*/
  gotoSpeakers1()
  {
    this.navCtrl.push(SpeakerDetail1Page);
  }

  gotoSpeakers2()
  {
    this.navCtrl.push(SpeakerDetail2Page);
  }

  gotoSpeakers3()
  {
    this.navCtrl.push(SpeakerDetail3Page);
  }

  gotoSpeakers4()
  {
    this.navCtrl.push(SpeakerDetail4Page);
  }

  gotoSpeakers5()
  {
    this.navCtrl.push(SpeakerDetail5Page);
  }

  gotoSpeakers6()
  {
    this.navCtrl.push(SpeakerDetail6Page);
  }

  gotoSpeakers7()
  {
    this.navCtrl.push(SpeakerDetail7Page);
  }

  gotoSpeakers8()
  {
    this.navCtrl.push(SpeakerDetail8Page);
  }

  gotoSpeakers9()
  {
    this.navCtrl.push(SpeakerDetail9Page);
  }

  gotoSpeakers10()
  {
    this.navCtrl.push(SpeakerDetail10Page);
  }

  /*#endregion*/

  
  signOut()
  {
    let alert = this.alertCtrl.create({
      title: 'Are You Sure?',
      message: 'You will be Signed out.',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel');
          }
        },
        {
          text: 'Ok',
          handler: () => {
            this.storage.clear();
            this.presentLoadingWithOptions();
            //this.DisplaySuccessSignOutMessage();
            this.navCtrl.setRoot(HomePage);
            
          }
        }
      ]
    });
    alert.present();

    
  }

   async presentLoadingWithOptions() {
    const loading = await this.loadingController.create({
      spinner: null,
      duration: 3000,
      content: 'Signing Out...',
      //translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    
    return await loading.present();
  }

  
  
 
 

 
 
     
  
  changeTab(value){
    this.gridlistview = value;
}

  showPopOver(myEvent) {
    let popover = this.popoverCtrl.create(MenWomenCategoryPopOverPage);

    popover.present({
      ev: myEvent
    });
  }

  goToSavedItems() {
    this.navCtrl.push('SavedItemsPage');
  }

  goToCategory(category: string) {
    var navParams = { 
      category: category, 
      isMenSelected: this.isMenSelected
    };

    this.navCtrl.push('CategoryPage', navParams);
  }

  goToProducts() {
    var navParams = { 
      category: 'CLOTHING', 
      isMenSelected: this.isMenSelected
    };

    this.navCtrl.push('ProductsPage', navParams);
  }


  //Move to Next slide
  slideNext(object, slideView) {
    slideView.slideNext(500).then(() => {
      this.checkIfNavDisabled(object, slideView);
    });
  }
 
  //Move to previous slide
  slidePrev(object, slideView) {
    slideView.slidePrev(500).then(() => {
      this.checkIfNavDisabled(object, slideView);
    });;
  }
 
  //Method called when slide is changed by drag or navigation
  SlideDidChange(object, slideView) {
    this.checkIfNavDisabled(object, slideView);
  }
 
  //Call methods to check if slide is first or last to enable disbale navigation  
  checkIfNavDisabled(object, slideView) {
    this.checkisBeginning(object, slideView);
    this.checkisEnd(object, slideView);
  }
 
  checkisBeginning(object, slideView) {
    slideView.isBeginning().then((istrue) => {
      object.isBeginningSlide = istrue;
    });
  }
  checkisEnd(object, slideView) {
    slideView.isEnd().then((istrue) => {
      object.isEndSlide = istrue;
    });
  }
 
}
