import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SpeakerDetail3Page } from './speaker-detail3';

@NgModule({
  declarations: [
    SpeakerDetail3Page,
  ],
  imports: [
    IonicPageModule.forChild(SpeakerDetail3Page),
  ],
})
export class SpeakerDetail3PageModule {}
