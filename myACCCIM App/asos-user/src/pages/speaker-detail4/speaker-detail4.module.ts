import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SpeakerDetail4Page } from './speaker-detail4';

@NgModule({
  declarations: [
    SpeakerDetail4Page,
  ],
  imports: [
    IonicPageModule.forChild(SpeakerDetail4Page),
  ],
})
export class SpeakerDetail4PageModule {}
