import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App, ViewController } from 'ionic-angular';
import { HomePage } from '../home/home';

@IonicPage()
@Component({
  selector: 'page-about',
  templateUrl: 'about.html',
})
export class AboutPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public app: App, public view: ViewController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AboutPage');
  }

  goToSavedItems() {
    this.navCtrl.push('SavedItemsPage');
  }

  goToHome() {
    this.app.getRootNav().setRoot(HomePage);
  }

  // closeAboutModal()
  // {
  //   this.view.dismiss();
  // }

}
