import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SpeakerDetail1Page } from './speaker-detail1';

@NgModule({
  declarations: [
    SpeakerDetail1Page,
  ],
  imports: [
    IonicPageModule.forChild(SpeakerDetail1Page),
  ],
})
export class SpeakerDetail1PageModule {}
