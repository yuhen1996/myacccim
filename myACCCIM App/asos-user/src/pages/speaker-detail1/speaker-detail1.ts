import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import { Http, Response } from "@angular/http";
import { Observable } from 'rxjs';
/**
 * Generated class for the SpeakerDetail1Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-speaker-detail1',
  templateUrl: 'speaker-detail1.html',
})
export class SpeakerDetail1Page {

  version: any = "chinese";
  speakerdetail1 : any;
  automaticClose = false;
  pages: any;
  pages2: any;
  showLevel1 = null;
  showLevel2 = null;

  cshowLevel1 = null;
  cshowLevel2 = null;
  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public http: HttpClient) {

    
    this.getCategory();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SpeakerDetail1Page');
  }

  
  getCategory(){
    this.http.get('assets/speakerdetail1.json')
    .subscribe(res =>
    {

      this.pages = res['english'];
      this.pages2 = res['chinese'];
    });
   
  }

  toggleLevel1(idx) {
    if (this.isLevel1Shown(idx)) {
      this.showLevel1 = null;
    } else {
      this.showLevel1 = idx;
    }
  };
  
  toggleLevel2(idx) {
    if (this.isLevel2Shown(idx)) {
      this.showLevel1 = null;
      this.showLevel2 = null;
    } else {
      this.showLevel1 = idx;
      this.showLevel2 = idx;
    }
  };

  isLevel1Shown(idx) {
    return this.showLevel1 === idx;
  };
  
  isLevel2Shown(idx) {
    return this.showLevel2 === idx;
  };



  /*Chinese*/
  ctoggleLevel1(idx) {
    if (this.cisLevel1Shown(idx)) {
      this.cshowLevel1 = null;
    } else {
      this.cshowLevel1 = idx;
    }
  };
  
  ctoggleLevel2(idx) {
    if (this.cisLevel2Shown(idx)) {
      this.cshowLevel1 = null;
      this.cshowLevel2 = null;
    } else {
      this.cshowLevel1 = idx;
      this.cshowLevel2 = idx;
    }
  };

  cisLevel1Shown(idx) {
    return this.cshowLevel1 === idx;
  };
  
  cisLevel2Shown(idx) {
    return this.cshowLevel2 === idx;
  };
}
