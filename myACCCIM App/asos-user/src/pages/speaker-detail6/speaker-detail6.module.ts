import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SpeakerDetail6Page } from './speaker-detail6';

@NgModule({
  declarations: [
    SpeakerDetail6Page,
  ],
  imports: [
    IonicPageModule.forChild(SpeakerDetail6Page),
  ],
})
export class SpeakerDetail6PageModule {}
