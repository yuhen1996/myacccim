import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError,tap } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';
import { Http, Response, RequestOptions } from '@angular/http';
/*
  Generated class for the ApiServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
//Declare a httpOptions and mention the header in json format so that HTTP Post method can use this option.
const httpOptions = {
  headers : new HttpHeaders({'Content-Type': 'application/json'})
};


@Injectable()
export class ApiServiceProvider {

  constructor(public https: Http) {
  
  }

  public UserLoginAPI = "https://api.acccim-registration.org.my/api/SignIn?id=";
  public DiamondSponsorAds = 'https://api.acccim-registration.org.my/api/DiamondSponsorAds';
  public PlatinumSlider = 'https://api.acccim-registration.org.my/api/HomePagePlatinum';
  public GoldSlider = 'https://api.acccim-registration.org.my/api/GoldSponsorSlider';
  public SilverSlider = 'https://api.acccim-registration.org.my/api/HomePageSilver';
  public SupportingSlider = 'https://api.acccim-registration.org.my/api/HomePageSupporting';
  public advertisement = 'https://api.acccim-registration.org.my/api/HomePageAdvertisement';
  public PlatinumCategories = 'https://api.acccim-registration.org.my/api/SponsorPageCategoriesPlatinum';
  public GoldCategories = 'https://api.acccim-registration.org.my/api/SponsorPageCategoriesGold';
  public SilverCategories = 'https://api.acccim-registration.org.my/api/SponsorPageCategoriesSilver';
  public SupportingCategories = 'https://api.acccim-registration.org.my/api/SponsorPageCategoriesSupporting';
  public ClickToPrompt = "https://api.acccim-registration.org.my/api/ClickToPromptAds?AdsId=";
  public SponsorPageAdvertisement = 'https://api.acccim-registration.org.my/api/SponsorPageAdvertisement';
  public HomePageDiamond = "https://api.acccim-registration.org.my/api/HomePageDiamond";
  public SponsorArrangementAPI = "https://api.acccim-registration.org.my/api/SponsorArrangementDisplay";
  public eventListing = "https://api.acccim-registration.org.my/api/EventListing";
  public specificevent = "https://api.acccim-registration.org.my/api/SpecificEvent?eventid=";
  public PopularEvent = "https://api.acccim-registration.org.my/api/PopularEvent";
  public changePassword = "https://api.acccim-registration.org.my/api/ChangePassword?id=";
  public getUserData = "https://api.acccim-registration.org.my/api/UserRegistrationData?id=";
  //User Login
  userlogin(user,pass)
  {
    var id = user;
    var pw = pass;

    if(id != null && pw != null)
    {
      return this.https.get(this.UserLoginAPI + id + '&&pw=' + pw);
    }
  }

  getDiamondSponsors()
  {
    //using https.get to retrieve the api url
    return this.https.get(this.DiamondSponsorAds);
  }

  getPlatinumSponsorSlider()
  {
    //using https.get to retrieve the api url
    return this.https.get(this.PlatinumSlider);
  }
  getGoldSponsorSlider()
  {
    //using https.get to retrieve the api url
    return this.https.get(this.GoldSlider);
  }

  getSilverSponsorSlider()
  {
    //using https.get to retrieve the api url
    return this.https.get(this.SilverSlider);
  }

  getSupportingSponsorSlider()
  {
    //using https.get to retrieve the api url
    return this.https.get(this.SupportingSlider);
  }

  getAdvertisement()
  {
    return this.https.get(this.advertisement);
  }

  getPlatinumCategories()
  {
    return this.https.get(this.PlatinumCategories);
  }

  getGoldCategories()
  {
    return this.https.get(this.GoldCategories);
  }

  getSilverCategories()
  {
    return this.https.get(this.SilverCategories);
  }

  getSupportingCategories()
  {
    return this.https.get(this.SupportingCategories);
  }

  getHomePageDiamond()
  {
    return this.https.get(this.HomePageDiamond);
  }
  getSponsorAdvertisement()
  {
    return this.https.get(this.SponsorPageAdvertisement);
  }

  getSponsorGridList()
  {
    return this.https.get(this.SponsorArrangementAPI);
  }
  getEvent()
  {
    return this.https.get(this.eventListing);
  }

  getSpecificEvent(evtid)
  {
    var eventid = evtid;
    
    return this.https.get(this.specificevent + eventid);
  }

  getSpecificAds(adsid)
  {
    var ads = adsid;
    
    return this.https.get(this.ClickToPrompt + ads);
  }

  popularevent()
  {
    return this.https.get(this.PopularEvent);
  }

  UpdatePassword(id,oldpassword,newpassword)
  {
    return this.https.put(this.changePassword + id + "&oldpassword=" + oldpassword + "&newpassword=" + newpassword, httpOptions)
    .pipe();
  }

  getUser(id)
  {
    return this.https.get(this.getUserData + id );
  }
 
 
}
