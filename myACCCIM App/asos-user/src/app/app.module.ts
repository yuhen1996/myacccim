import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { MenWomenCategoryPopOverPage } from '../pages/men-women-category-pop-over/men-women-category-pop-over';
import { AboutPage } from '../pages/about/about';
import { SavedItemsPage } from '../pages/saved-items/saved-items';
import { AccountPage } from '../pages/account/account';
import { AppSettingsPage } from '../pages/app-settings/app-settings';
import { HelpFaqPage } from '../pages/help-faq/help-faq';
import { SavedItemsPageModule } from '../pages/saved-items/saved-items.module';
import { RecommendedPopOverPage } from '../pages/recommended-pop-over/recommended-pop-over';
import { FilterPage } from '../pages/filter/filter';
import { ProductSizePopOverPage } from '../pages/product-size-pop-over/product-size-pop-over';
import { ProductColourPopOverPage } from '../pages/product-colour-pop-over/product-colour-pop-over';
import { ApiServiceProvider } from '../providers/api-service/api-service';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { ProductDetailsPage } from '../pages/product-details/product-details';
import { LoginPage } from '../pages/login/login';
import { ChangePasswordPage } from '../pages/change-password/change-password';
import {IonicStorageModule} from '@ionic/storage';
import {AboutPageModule} from '../pages/about/about.module';
import { LoginPageModule } from '../pages/login/login.module';
import { SpeakerDetail1Page } from '../pages/speaker-detail1/speaker-detail1';
import { SpeakerDetail2Page } from '../pages/speaker-detail2/speaker-detail2';
import { SpeakerDetail3Page } from '../pages/speaker-detail3/speaker-detail3';
import { SpeakerDetail4Page } from '../pages/speaker-detail4/speaker-detail4';
import { SpeakerDetail5Page } from '../pages/speaker-detail5/speaker-detail5';
import { SpeakerDetail6Page } from '../pages/speaker-detail6/speaker-detail6';
import { SpeakerDetail7Page } from '../pages/speaker-detail7/speaker-detail7';
import { SpeakerDetail8Page } from '../pages/speaker-detail8/speaker-detail8';
import { SpeakerDetail9Page } from '../pages/speaker-detail9/speaker-detail9';
import { SpeakerDetail10Page } from '../pages/speaker-detail10/speaker-detail10';
import { PopupModalPage } from '../pages/popup-modal/popup-modal';
import {IonicImageViewerModule} from 'ionic-img-viewer';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    MenWomenCategoryPopOverPage,
    RecommendedPopOverPage,
    ProductSizePopOverPage,
    ProductColourPopOverPage,
    AccountPage,
    AppSettingsPage,
    HelpFaqPage,
    FilterPage,
    AboutPage,
    LoginPage,
    ProductDetailsPage,
    ChangePasswordPage,
    SpeakerDetail1Page,
    SpeakerDetail2Page,
    SpeakerDetail3Page,
    SpeakerDetail4Page,
    SpeakerDetail5Page,
    SpeakerDetail6Page,
    SpeakerDetail7Page,
    SpeakerDetail8Page,
    SpeakerDetail9Page,
    SpeakerDetail10Page,
    PopupModalPage
  ],

  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot({ name: '__mydb',
    driverOrder: ['indexeddb', 'sqlite', 'websql']}),
    HttpModule,
    HttpClientModule,
    SavedItemsPageModule,
    IonicImageViewerModule
  
    
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    AboutPage,
    MenWomenCategoryPopOverPage,
    RecommendedPopOverPage,
    ProductSizePopOverPage,
    ProductColourPopOverPage,   
    SavedItemsPage,
    AccountPage,
    AppSettingsPage,
    HelpFaqPage,
    FilterPage,
    ProductDetailsPage,
    LoginPage,
    ChangePasswordPage,
    SpeakerDetail1Page,
    SpeakerDetail2Page,
    SpeakerDetail3Page,
    SpeakerDetail4Page,
    SpeakerDetail5Page,
    SpeakerDetail6Page,
    SpeakerDetail7Page,
    SpeakerDetail8Page,
    SpeakerDetail9Page,
    SpeakerDetail10Page,
    PopupModalPage
    

  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ApiServiceProvider,
    InAppBrowser
  ]
})
export class AppModule {}
