import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, AlertController, LoadingController, MenuController} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { AboutPage } from '../pages/about/about';
import { SavedItemsPage } from '../pages/saved-items/saved-items';
import { AccountPage } from '../pages/account/account';
import { AppSettingsPage } from '../pages/app-settings/app-settings';
import { HelpFaqPage } from '../pages/help-faq/help-faq';
import { ChangePasswordPage } from '../pages/change-password/change-password';
import { LoginPage } from '../pages/login/login';
import {Storage} from '@ionic/storage';
import { ApiServiceProvider } from '../providers/api-service/api-service';
import { Http,Response } from '@angular/http';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = HomePage;
  id:any;
 
  userCredential:any;

  nouser: boolean;
  hasuser: boolean;
  welcome: any;
  username: any;
  constructor(public platform: Platform, public statusBar: StatusBar, 
              public splashScreen: SplashScreen, public storage: Storage, 
              public alertCtrl : AlertController, 
              public loadingController :LoadingController,
              public ApiService :ApiServiceProvider,
              public menu:MenuController) {
                this.menu.enable(false, 'myMenu');

    this.initializeApp();
    
    

    this.storage.get("userid").then((val) => {
      this.id = val;

      if(this.id === null)
      {
       
        
        this.nouser = true;
        
       
      }

      else
      {
        this.ApiService.getUser(this.id)
          .subscribe((res: Response) =>
          {
            this.userCredential = res.json();
            
          });

         
        this.nouser = false;
      
        
      }

    });
   
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  goToHome() {
    this.nav.setRoot(HomePage);
  }

  goToBag() {
    this.nav.push(AboutPage);
  }

  goToSavedItems() {
    this.nav.setRoot(SavedItemsPage);
  }

  goToChangePassword() {
    this.nav.push(ChangePasswordPage);
  }


  goToMyAccount() {
    this.nav.setRoot(AccountPage);
  }

  goToAppSettings() {
    this.nav.setRoot(AppSettingsPage);
  }

  goToHelpAndFaq() {
    this.nav.setRoot(HelpFaqPage);
  }

  signOut()
  {
    let alert = this.alertCtrl.create({
      title: 'Are You Sure?',
      message: 'You will be Signed out.',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel');
          }
        },
        {
          text: 'Ok',
          handler: () => {
            this.storage.clear();
            this.presentLoadingWithOptions();
            //this.DisplaySuccessSignOutMessage();
            this.nav.setRoot(HomePage);
            
          }
        }
      ]
    });
    alert.present();

    
  }

    presentLoadingWithOptions() {
    const loading =  this.loadingController.create({
      spinner: null,
      duration: 5000,
      content: 'Signing Out...',
      //translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    
    loading.present();
  }

  
//  DisplaySuccessSignOutMessage()
//   {
//     let alert =  this.alertCtrl.create({
//       title: 'Sign Out',
//       subTitle: 'You Had Successfully Sign Out!',
//       buttons: ['Ok']
//     });
//     alert.present();
//   }
}
